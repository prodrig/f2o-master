
"""
F2O: First Order Optimization
=============================

Documentation is available in the docstrings and
online at https://.

Provides
  1. First Order Optimization methods such ISTA, FISTA, ADMM, etc.
  2. 
  3. Examples


Available subpackages
---------------------



Utilities
---------


"""
from __future__ import division, absolute_import, print_function
