
import scipy as sc
import matplotlib.pylab as PLT
import numpy as np

from scipy import signal

import F2O.constants as CTE

import multiprocessing
import pyfftw

pyfftw.config.NUM_THREADS = multiprocessing.cpu_count()
pyfftw.config.PLANNER_EFFORT = 'FFTW_MEASURE'



__author__ = """Paul Rodriguez <prodrig@pucp.edu.pe>"""


class dataShpMixin:
    r"""Geometry (data, solution, observe, etc.)
    
    """
    
    def set_dataShp(self, bShape=None):

        self.xShape   = None           # Original shape of the solution
        self.x_sptl   = None           # when problems are solve in Fourier,
                                       # this variable keeps track of the spatial /
                                       # temporal version

        
        self.bShape   = bShape         # input data shape
        self.bfShape  = None           # input data shape in freq.
        self.Bf       = None           # variable in the freq. domain
        
        self.maxShape = None           # m
        
        self.padShape  = None          # to be used if sol. is computed in freq
        self.padFlag   = True
        self.padOffset = None
        #self.convFreq = CTE.convLin   # if convolution is computed in frequency, force the result
                                        ## to be the same as for
                                        ## * linear convolution with symmetric boundary condition
                                        ## * circular convolution
                                        ## * etc.


class fwOpShpMixin:
    r"""Geometry (fw operator)
    
    """

    def set_fwOpShp(self, hShape=None, lShape=None):

        self.hShape   = hShape          # Shape for forward operator
        self.lShape   = lShape          # if any, shape for the operator in the regularization term

        # variables in the freq. domain
        self.Hf      = None
        self.Lf      = None



class argsF2O(dataShpMixin, fwOpShpMixin):

    def __init__(self,hShape=None, bShape=None, lShape=None):
      
        self.set_dataShp(bShape)
        self.set_fwOpShp(hShape, lShape)
        self.lmbda    = 0.
      
