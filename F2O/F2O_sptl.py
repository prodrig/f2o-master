
#%env JAX_ENABLE_X64=1
#%env JAX_PLATFORM_NAME=gpu

import scipy as sc
import matplotlib.pylab as PLT
import numpy as np


from scipy import signal

from F2O.F2O_utils import f2oJaxUtils, argsF2O, stepSize, stopCondF2O, costGradF2O,  proxOp, ISeq
import F2O.constants as f2oDef

from timeit import default_timer as timer


__author__ = """Paul Rodriguez <prodrig@pucp.edu.pe>"""


# ======================================================
# ======================================================

def gd(Op, b, nIter=100, args=None):
  r"""
  Solve the gradient descent (GD) problem:

    F(x) = f( Ax(x) - b )
    
  where f is the cost functional, Ax is the forward operator and
  b is the input (observed) data, via the iteration
  
    x_{k+1} = x_k - \alpha \cdot \nabla F(x_k)
    
  where \nabla F can be computed via the f (costGradF2O class in F2O_utils.py)
  and Ax (fwOp class in fwOperator.py) classes
    
  """
  
 
  
  if args is None:
    args = argsF2O()
    
  args.f2oJax = f2oJaxUtils()

  
  gd = commomPGD(False, False)          # (True: proximal. True: accelerated GG. False --> just GD)
    
    
  # input data is transfered to 'device' (if needed)
  bDev = args.f2oJax.getDataIn(b)  
    
    
  # Initialization
  x, Ax, f, stopFunc, gdStats, itemStats = gd.Init(Op, bDev, nIter, args)
    
    
  
  # Get subroutine (e.g. step-size computation, stoping criteria, etc)
  computeCost, computeGrad, computeSS = gd.SetFunctions(args, f, Ax, nIter)
  
  
  # Elapse time per iteration (after initializations)
  start = timer()

  
  for k in range(nIter):
    
    
    #z     = fAx(x, 0, None) - b.ravel()         # A*x - b        
    #grad  = fAx(z, 1, None)                     # A^T * z


    grad = computeGrad(x, bDev, None)
                        
    alpha, grad = computeSS(k, grad, x)         # compute step-size            
    
    x = x - alpha*grad                          # Gradient descent step
    

        
    # --- statistics --
    gd.RecordStats(k, x, bDev, grad, gdStats, itemStats, None, computeCost)
    
    # --- record step-Size ---
    if 'ssX' in itemStats.keys():
       gdStats[k,itemStats['ssX']] = np.mean(alpha)
    #gdStats[k,1] = alpha
    # --- 
    
    
    if 'time' in itemStats.keys():
       gdStats[k,itemStats['time']] = timer()-start
    #gdStats[k,2] = timer()-start          # Measure time
    
    if stopFunc(k, x, gdStats):           # check for exit condition / print stats
      print('Stop criteria -- iteration {0}'.format(k))
      gdStats = gdStats[0:k,:]
      break
    
  
  xOut = args.f2oJax.getDataOut(x)
  
  if args.xShape is not None:
     xOut.shape = args.xShape
  
  
  return(xOut, gdStats)



# ======================================================
# ======================================================


def agd(Op, b, nIter=100, args=None):

  pass

  
# ======================================================
# ======================================================



def ls_viaCG(Ax, b, nIter=100, args=None):
  r"""
  Solve the least squares problem:

    0.5|| Ax(x) - b ||_2^2
    
  where Ax (fwOp class in fwOperator.py) is the forward operator and 
  b is the input (observed) data, via the Conjugate Gradient (CG) algorithm,
  thus the lineal system

    Ax^T( Ax(x) ) = Ax^T( b )
    
  is the one actually solved
  
  """
  #=================================================================



  imgDims = args.xShape
  if len(imgDims) == 2:
    Nr = imgDims[0]*imgDims[1]
    Nc = imgDims[0]*imgDims[1]
  else:
    Nr = imgDims[0]*imgDims[1]*imgDims[2]
    Nc = imgDims[0]*imgDims[1]*imgDims[2]
    
  
  ATAx = lambda x : Ax( Ax(x, 0, None).ravel(), 1, None)
  ATb  = Ax( b, 1, None).ravel()
  
  opA =  sc.sparse.linalg.LinearOperator((Nr,Nc), matvec=ATAx)


  x, info = sc.sparse.linalg.cg(opA, ATb, tol=args.stop_tol, maxiter=nIter )
  
  x.shape = imgDims 


  return x, info


# ======================================================
# ======================================================


def Tikhonov_viaCG(Ax, b, lmbda, nIter=100, args=None, L=None):
  r"""
  Solve the regularized least squares (Tikhonov) problem:

    0.5|| Ax(x) - b ||_2^2 + 0.5*\lmbda|| x ||
    
  where Ax (fwOp class in fwOperator.py) is the forward operator and 
  b is the input (observed) data, via the Conjugate Gradient (CG) algorithm,
  thus the lineal system

    Ax^T( Ax(x) ) + lmbda*x = Ax^T( b )
    
  is the one actually solved
  
  """
  #=================================================================



  imgDims = args.xShape
  if len(imgDims) == 2:
    Nr = imgDims[0]*imgDims[1]
    Nc = imgDims[0]*imgDims[1]
  else:
    Nr = imgDims[0]*imgDims[1]*imgDims[2]
    Nc = imgDims[0]*imgDims[1]*imgDims[2]
    
  
  if Ax is not None:
     if L is None:
        ATAx = lambda x : Ax( Ax(x, 0, None).ravel(), 1, None) + lmbda*x
        ATb  = Ax( b, 1, None).ravel()
  
  else:
     ATAx = lambda x : x.ravel() + lmbda*signal.convolve2d(np.reshape(x, imgDims), L, boundary=args.boundary, mode=args.mode).ravel()
     ATb = b.ravel()
  
  
  opA =  sc.sparse.linalg.LinearOperator((Nr,Nc), matvec=ATAx)


  x, info = sc.sparse.linalg.cg(opA, ATb, tol=args.stop_tol, maxiter=nIter )
  
  x.shape = imgDims 


  return x, info



# ======================================================
# ======================================================
  
class commomPGD(object):
    r"""Commom routines for algorithms related to the (proximal) 
        gradient descent and accelerated variants 
    """
    def __init__(self, accelerated, proximal=True):
        self.proximal    = proximal
        self.accelerated = accelerated
        self.Tstart      = None
        
  
    def Init(self, Op, b, nIter, args):
      r"""
      PG: Simple initializations:
      """
      
      # Sanity check
      if Op.A is None:
         if Op.linOp is f2oDef.fAx.Identity:
           args.hShape = (1,1)
           Op.A = np.array([[1.]])
         else:
           print('Forward operator was no set ... ')
           return None
      else:
         if args.hShape is None:
            args.hShape = Op.A.shape
         
      if args.bShape is None:
         args.bShape = b.shape
      
      if args.freqSol is not False:
         args.freqSol = False
         
      args.dwFactor = Op.dwFactor   
         
      # ---
      
      # args.fCostClass has forceDual()
      
      Op.bShape = b.shape
      
      Ax = Op.sel_Ax(fCostClass=args.fCostClass)

      Op.xShape  = Ax(b,0,1)
      
      Op.vecFlag = True

  
      x = args.f2oJax.getDataIn( np.zeros((np.prod(Op.xShape),),dtype='float') )
  
      args.xShape = Op.xShape
    
      fCost = costGradF2O(Ax, args)
  
      # Select stoping criteria
      stopVar = stopCondF2O(args, nIter)
      stopFun = stopVar.sel_SC()

      # Create variable for statistics (if needed)
      if stopVar.dict['last'] >= 1:
        gdStats = np.zeros((nIter, stopVar.dict['last']),dtype='float')
      else:
        gdStats = None
      
      itemStats = stopVar.dict.copy()

      #tmp = args.fCost + args.alpha + args.gradL2 + args.gradLi + 1
      #if tmp >= 1:
        #gdStats = np.zeros((nIter,tmp),dtype='float')
      #else:
        #gdStats = None
      
        
      return x, Ax, fCost, stopFun, gdStats, itemStats
  

    #def setShapeDual(self, xShape, costClass):      
      
      #if costClass.forceDual():
        
        #if costClass is f2oDef.cost.L2TV_lin:
           #zShape = (*xShape,2)
                  
        #return zShape

      #else:
        
        #return xShape
      

    def SetFunctions(self, args, f, Ax, nIter):
  
      # select cost and gradient functions  
      computeCost, computeGrad = f.sel_cost(dual=args.fCostClass.forceDual())
  
    
      # Select SS (step-size) function
      ssVar = stepSize(args,  Ax)
      #computeSS = stepSize.sel_SS(ssVar)
      computeSS = ssVar.sel_SS()

      if self.proximal:

        # Select prox / proj function    
        ppVar = proxOp(args)
        #computeProx = proxOp.sel_ppOp(ppVar)
        computeProx = ppVar.sel_ppOp(dual=args.fCostClass.forceDual())
    
        if self.accelerated is False:
          return computeCost, computeGrad, computeSS, computeProx
       
        # Select inertial sequence    
        iseqVar = ISeq(args)
        #computeISeq = ISeq.sel_iseqOp(iseqVar)
        computeISeq = iseqVar.sel_iseqOp()
      
        return computeCost, computeGrad, computeSS, computeProx, computeISeq
      
      else:

        if self.accelerated is False:
          return computeCost, computeGrad, computeSS
       
        # Select inertial sequence    
        iseqVar = ISeq(args)
        computeISeq = ISeq.sel_iseqOp(iseqVar)
      
        return computeCost, computeGrad, computeSS, computeISeq
      
      return
    
    
    def SetDualRec(self, args, Op, Ax):
    
      # select cost and gradient functions  
      computePrimalVar = Op.sel_primalVar(Ax, args.fCostClass)
  
      return computePrimalVar


    def RecordStats(self, k, x, b, grad, gdStats, itemStats, extraVar, computeCost):
      r"""
      PG: Record statistics associated with the evolution of PG
      """

      if gdStats is not None:
      
        if 'cost' in itemStats.keys():
           gdStats[k,itemStats['cost']] = computeCost(x, b.ravel(), extraVar)
      
        if 'gL2' in itemStats.keys():
           gdStats[k,itemStats['gL2']] =  sum(grad.ravel()*grad.ravel())        # \| grad \|_2^2

        if 'gLi' in itemStats.keys():
           gdStats[k,itemStats['gLi']] =  abs(grad.ravel()).max()               # \| grad \|_\infty
    
      #if gdStats is not None:
      
        #if gdStats.shape[1] >= 1:
           #gdStats[k,0] = computeCost(x, b.ravel(), x_sptl)
      
        
        #switcher = {
          #4:    self.compute_gL2,
          #5:    self.compute_gL2gLi
          #}
        
        #extraStats = switcher.get(gdStats.shape[1], None)
        
        #if extraStats is not None:
           #extraStatsFun = lambda grad, gdStats: extraStats(k, grad, gdStats)
           #extraStatsFun(grad, gdStats)
           
        return None
           
    def compute_gL2(self, k, grad, gdStats):
      gdStats[k,3] =  np.dot(grad.ravel(), grad.ravel())           # \| grad \|_2^2
      
    def compute_gL2gLi(self, k, grad, gdStats):
      gdStats[k,3] =  np.dot(grad.ravel(), grad.ravel())           # \| grad \|_2^2
      gdStats[k,4] =  np.abs(grad.ravel()).max()                   # \| grad \|_2^2
      

           
    def iterTimer(self, flagTimer, flagInit, flagElapse):
      r"""
      Simple routine to elapse time
      """
        
      if flagTimer:
        
        if flagInit:
           self.Tstart = timer()
           
        
        if flagElapse:
           return timer()-self.Tstart
        
      else:
        return None

