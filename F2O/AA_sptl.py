
import scipy as sc
import matplotlib.pylab as PLT
import numpy as np

from scipy import signal

from F2O.F2O_utils import argsF2O, stepSize, stopCondF2O, costGradF2O,  proxOp, ISeq
from F2O.F2O_sptl import commomPGD


from timeit import default_timer as timer


__author__ = """Paul Rodriguez <prodrig@pucp.edu.pe>"""


# ======================================================
# ======================================================


def AA_gd(Op, b, nIter=100, args=None):
  r"""
  Solve the gradient descent (GD) problem:

    F(x) = f( Ax(x) - b )
    
  where f is the cost functional, Ax is the forward operator and
  b is the input (observed) data, via the iteration
  
    x_{k+1} = x_k - \alpha \cdot \nabla F(x_k)
    
  where \nabla F can be computed via the f (costGradF2O class in F2O_utils.py)
  and Ax (fwOp class in fwOperator.py) classes
    
  """
  
 
  
  if args is None:
    args = argsF2O()
    

  AAgd = commomPGD(False, False)          # (True: proximal. True: accelerated GG. False --> just GD)
    
    
  # Initialization
  x, Ax, f, gdStats = AAgd.Init(Op, b, nIter, args)
    
    
  
  # Get subroutine (e.g. step-size computation, stoping criteria, etc)
  computeCost, computeGrad, computeSS, stopFunc = AAgd.SetFunctions(args, f, Ax, nIter)
  
  
  # Elapse time per iteration (after initializations)
  start = timer()


  Gx = np.zeros( (x.ravel().shape[0], args.AAlength), dtype=float)
  R  = np.zeros( (x.ravel().shape[0], args.AAlength), dtype=float)
  
  for k in range(nIter):
    
    
    #z     = fAx(x, 0, None) - b.ravel()         # A*x - b        
    #grad  = fAx(z, 1, None)                     # A^T * z

    grad = computeGrad(x, b, None)
    alpha = computeSS(k, grad, x)       # compute step-size -- For AA, default is cte.
    
    g = x - alpha*grad                  # fixed-point step

    if k < args.AAlength:
      Gx[:,k] = g.ravel()
      R[:,k]  = g.ravel() - x.ravel()
    else:
      Gx = np.concatenate( ( Gx[:,1:args.AAlength], g[...,np.newaxis]), axis=1)
      R  = np.concatenate( ( R[:,1:args.AAlength], g[...,np.newaxis]-x[...,np.newaxis]), axis=1)
    
    
    if k == 0:
       beta = 1./np.dot(R.ravel(),R.ravel())
    
    else:
      if k < args.AAlength:
        rMat = R[:,0:k+1].transpose().dot(R[:,0:k+1])
      else:
        rMat = R.transpose().dot(R)
      
      
      rMat /= np.linalg.norm(rMat, 2)
    
      rMatSol = np.linalg.lstsq(rMat + 1e-8 * np.eye(rMat.shape[1]), np.ones(rMat.shape[1]), rcond=None)
      
      beta = rMatSol[0] / rMatSol[0].sum()

    if k < args.AAlength:
       x = Gx[:,0:k+1].dot( beta ).ravel()
    else:
       x = Gx.dot( beta ).ravel()
        
    # --- statistics --
    AAgd.RecordStats(k, x, b, grad, gdStats, None, computeCost)
    # --- 
          
          
    
    
    # --- record step-Size ---
    gdStats[k,1] = beta.mean()
    # --- 
    
    
    gdStats[k,2] = timer()-start          # Measure time
    if stopFunc(k, x, gdStats):           # check for exit condition / print stats
      print('Stop criteria -- iteration {0}'.format(k))
      gdStats = gdStats[0:k,:]
      break
    
  
  if args.xShape is not None:
    x.shape = args.xShape
  
  
  return(x, gdStats)



# ======================================================
# ======================================================


def AA_pg(Op, b, lmbda, nIter=100, args=None):  
  r"""Proximal gradient (includes ISTA)
  
  Solve the l1 regularized problem problem:

    F(x) = f( Ax(x) - b ) + \lambda || x ||_1
    
  where f is the fidelity functional, Ax is the forward operator and
  b is the input (observed) data, via the iteration
  
    x_{k+1} = prox_{l1}( x_k - \alpha \cdot \nabla f(x_k), lmbda )
    
  where \nabla f can be computed via the f (costGradF2O class in F2O_utils.py)
  and Ax (fwOp class in fwOperator.py) classes
    
  """

  # -----------------------
  
  if args is None:
    args = argsF2O()

    
  AApg = commomPGD(True, False)          # True: accelerated PG. False --> just PG
    
    
  # Initialization
  args.lmbda = lmbda
  x, Ax, f, pgStats = AApg.Init(Op, b, nIter, args)
    
  
  # Get subroutines (e.g. step-size computation, stoping criteria, etc.)
  computeCost, computeGrad, computeSS, stopFunc, prox = AApg.SetFunctions(args, f, Ax, nIter)
  
  
  # Elapse time per iteration (after initializations)
  start = timer()
  
  Gx = np.zeros( (x.ravel().shape[0], args.AAlength), dtype=float)
  R  = np.zeros( (x.ravel().shape[0], args.AAlength), dtype=float)
  
  y = x.copy()
  
  for k in range(nIter):
    
    
    grad = computeGrad(x, b, None)      # compute gradien
    alpha = computeSS(k, grad, x)       # compute step-size
        
    g = x - alpha*grad                  # fixed-point step

    # --- statistics --
    AApg.RecordStats(k, x, b, grad, pgStats, None, computeCost)
    # --- 

    if k < args.AAlength:
      Gx[:,k] = g.ravel()
      R[:,k]  = g.ravel() - y.ravel()
    else:
      Gx = np.concatenate( ( Gx[:,1:args.AAlength], g[...,np.newaxis]), axis=1)
      R  = np.concatenate( ( R[:,1:args.AAlength], g[...,np.newaxis]-x[...,np.newaxis]), axis=1)
    
    
    if k == 0:
       beta = 1./np.dot(R.ravel(),R.ravel())
    
    else:
      if k < args.AAlength:
        rMat = R[:,0:k+1].transpose().dot(R[:,0:k+1])
      else:
        rMat = R.transpose().dot(R)
      
      
      rMat /= np.linalg.norm(rMat, 2)
    
      rMatSol = np.linalg.lstsq(rMat + 1e-8 * np.eye(rMat.shape[1]), np.ones(rMat.shape[1]), rcond=None)
      
      beta = rMatSol[0] / rMatSol[0].sum()
    
    
    if k < args.AAlength:
       y = Gx[:,0:k+1].dot( beta ).ravel()
    else:
       y = Gx.dot( beta ).ravel()
    
    
    x = prox(y, alpha*lmbda)     # proximal gradient descent step
    
    
    # --- record step-Size --
    pgStats[k,1] = beta.mean()
    # --- 
                                    
    
    pgStats[k,2] = timer()-start        # Measure time
    
    if stopFunc(k, x, pgStats):         # check for exit condition / print stats
      print('Stop criteria -- iteration {0}'.format(k))
      break
    
  
  if args.xShape is not None:
    x.shape = args.xShape
  
  
  return(x, pgStats)
  

