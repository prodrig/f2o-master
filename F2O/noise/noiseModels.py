


import numpy as np
import scipy as sc
from scipy import signal


import F2O.constants as f2oDef
from F2O.F2O_utils import f2oJaxUtils


__author__ = """Paul Rodriguez <prodrig@pucp.edu.pe>"""


class noiseModels(object):
    r"""Noise model  
    """
    def __init__(self, model=f2oDef.noise.Null, mean=0., sigma=0.,enableJAX=None):
      
      if model is not None:
         self.model = model
      
      self.dtype = 'float'
      self.noiseName = None
      self.mean  = mean           # Default mean value
      self.sigma = sigma          # Default variance for Gaussian
      self.p     = 0.1            # Default probability for SnP / RVIN

      self.f2oJax = f2oJaxUtils(flagForceJAX=enableJAX)
      

    def sel_NoiseModel(self):
      
      if self.model.val == f2oDef.noise.Gaussian.val and self.sigma == 0.:
         self.model =  f2oDef.noise.Null
         
      switcher = {
          f2oDef.noise.Gaussian.val:  self.gaussNoise,
          f2oDef.noise.SnP.val:       self.SnPnoise,
          f2oDef.noise.Null.val:      self.Identity
          }
      
      noiseFun = switcher.get(self.model.val, "nothing")
      
      func = lambda u: noiseFun(u)
      
      self.noiseName = self.model.name
      
      return func


    def Identity(self, U):
      return U


    def SnPnoise(self, U):
    
      uMax = U.max()
      uMin = U.min()
    
      V = np.zeros(U.shape,dtype=float)
      mask = np.random.binomial(1,1-self.p,U.shape)
      noise = np.random.binomial(1,0.5,U.shape)
    
      V = U*mask + (1-mask)*noise*uMax +  (1-mask)*(1-noise)*uMin
    
      return V


    def gaussNoise(self, U):
    
      if isinstance(U, list):
    
        V = []
        
        if self.f2oJax.have_jax:

          for k in range(len(U)):
            V.append( U[k] + np.random.normal(self.mean, self.sigma, U[k].shape)  ) 
          
          ##key = self.f2oJax.jnp.random.PRNGKey(np.random.randint(1000))          
          ##for k in range(len(U)):
            ###key, subkey = self.f2oJax.jnp.random.split(key)
            ##key = self.f2oJax.jnp.random.PRNGKey(np.random.randint(1000))          
            ##V.append( U[k] + self.sigma*self.f2oJax.jnp.random.normal(key, U[k].shape) + self.mean )  
          
        else:
          
          for k in range(len(U)):
            V.append( U[k] + np.random.normal(self.mean, self.sigma, U[k].shape)  ) 
                               
        
        return V
      
      else:

        return (U + np.random.normal(self.mean, self.sigma, U.shape) )
    
        ##if self.f2oJax.have_jax:
          ##key = self.f2oJax.jnp.random.PRNGKey(np.random.randint(1000))  
          ##return (U + self.mean + self.sigma*self.sigma*self.f2oJax.jnp.random.normal(key, U.shape) )
        ##else:
          ##return (U + np.random.normal(self.mean, self.sigma, U.shape) )


