import scipy as sc
import matplotlib.pylab as PLT
import numpy as np


from scipy import signal

import F2O.constants as f2oDef

from F2O.F2O_utils import f2oJaxUtils, opDxDy


import multiprocessing
import pyfftw

pyfftw.config.NUM_THREADS = multiprocessing.cpu_count()
pyfftw.config.PLANNER_EFFORT = 'FFTW_MEASURE'
pyfftw.interfaces.cache.enable()
pyfftw.interfaces.cache.set_keepalive_time(300)




__author__ = """Paul Rodriguez <prodrig@pucp.edu.pe>"""


class fwOpVarsMixin:

    def set_fwOpvars(self, linOp=None, A=None, enableJAX=None):

        if linOp is not None:
           self.linOp     = linOp
           
        self.A         = A                      #matrix (if None, a randomized option will be generated)
           
        self.Af        = None                   #matrix
        self.cardfwOp  = None                   # cardinality of operator (e.g. usefull for filterbanks)
        
        self.bShape    = None           # Original shape of the observed data
        self.xShape    = None           # Original shape of the solution
        
        self.boundary  = 'symm'
        self.mode      = 'same'
        self.vecFlag   = False
        self.label     = None
        
        self.freqSol   = False          # The Convolution theorem applies, thus the operator
                                        # will operate in the frequency domain
        self.dwFactor  = None
        self.interp     = None
        self.interpFlag = 'F4'          # follows notation by 10.1109/TASSP.1977.1162921
        self.thresh     = None

        self.f2oJax    = f2oJaxUtils(enableJAX)

class fwOp(fwOpVarsMixin):
    r"""Functions for forward operator (spatial).

    """
    def __init__(self, linOp=None, A=None, enableJAX=None):

        self.set_fwOpvars(linOp, A, enableJAX)
        self.freqSol = False

    # -------------------------------------------------------
    # -------------------------------------------------------
    
    def fun_Identity(self, A, x, trans=0, shape_Ax=None):
      r"""Matrix times vector.

      """
    
      if shape_Ax is None:
  
         z = x
  
      else:
        
        z = x.shape
        
      return z

    # -------------------------------------------------------
    # -------------------------------------------------------
    
    def fun_rec_dualId(self, Ax, x, extraVar=None):
    # given the dual var, compute the current primal var (identity)
    
      if extraVar[0].xShape is not None:
         x.shape = extraVar[0].xShape

    
      return x
    
    
    def fun_mv(self, A, x, trans=0, shape_Ax=None):
      r"""Matrix times vector.

      """
    
      if shape_Ax is None:
  
        if trans==0:
          z = A.dot(x)
        else:
          z = A.transpose().dot(x)
  
      else:
        z = (A.shape[1],)
        
      return z


    # -------------------------------------------------------
    # -------------------------------------------------------
    
    
    def colorConv2d(self, U, H):
      nDims = U.shape
    
      V = 0*U
      for k in range( nDims[2] ):
          V[:,:,k] = self.f2oJax.convolve2d(U[:,:,k], H, boundary=self.boundary, mode=self.mode)
    
      return(V)


    def conv2D(self, U, H):

      if len(U.shape)==3:
         z = colorConv2d(U, H, boundary=self.boundary, mode=self.mode)
      else:
         z = self.f2oJax.convolve2d(U, H, boundary=self.boundary, mode=self.mode)

      return(z)


    def fun_conv2D(self, H, b, trans=0, shape_Ax=None):
        
        
      if shape_Ax is None:

        if self.vecFlag:                  # if input data x is vectorized
           origShape = b.shape
           b = self.f2oJax.setShape(b, self.bShape)  # then re-shape it as a matrix (image).


        if trans==0:
          
                
          if len(b.shape)==3:
            z = self.colorConv2d(b, H)
          else:
            z = self.f2oJax.convolve2d(b, H, boundary=self.boundary, mode=self.mode)   # NOTE: JAX only support boundary='fill', fillvalue=0
    
        else:
          if len(b.shape)==3:
            z = self.colorConv2d(b, np.flip(np.flip(H,1),0) )
          else:
            
            z = self.f2oJax.convolve2d(b, np.flip(np.flip(H,1),0), boundary=self.boundary, mode=self.mode)
                
        
        if self.vecFlag:        # if input data x is vectorized
           z = z.ravel()        # then force output to be vectorized.
           b = self.f2oJax.setShape(b, origShape)
           
      else:
        z = self.bShape     # FIXME: add the case where output (convolution) has not the same size


    
      return z

    # -------------------------------------------------------
    # -------------------------------------------------------

    def colorFB2D(self, U, H, trans):
      r"""
      H.shape = nFRows x nFcols x nFilters
      """
      
      uDims = U.shape
      hDims = H.shape
    
      if trans == 0:    # U.shape = nURows x nUCols x nUCh x nFilters
        
        V = np.zeros( (uDims[0], uDims[1], uDims[2]), dtype=float)
        
        for l in range( hDims[2] ):        
          for k in range( nDims[2] ):
            V[:,:,k] += signal.convolve2d(U[:,:,k,l], H[:,:,l], boundary=self.boundary, mode=self.mode)
          
      else:             # U.shape = nURows x nUCols x nUCh x 1
        
        V = np.empty( (uDims[0], uDims[1], uDims[2], hDims[2]), dtype=float)
        for l in range( hDims[2] ):        
          for k in range( nDims[2] ):
            V[:,:,k,l] = signal.convolve2d(U[:,:,k], np.flip(np.flip(H[:,:,l],1),0), boundary=self.boundary, mode=self.mode)
        
        
      return(V)


    def grayFB2D(self, U, H, trans):
      r"""
      H.shape = nFRows x nFcols x nFilters
      """
      
      uDims = U.shape
      hDims = H.shape
    
      
      
      if trans == 0:    # U.shape = nURows x nUCols x 1 x nFilters
        
        V = np.zeros( (uDims[0], uDims[1] ), dtype=float)
        
        for l in range( hDims[2] ):        
            V += signal.convolve2d( U[:,:,0,l], H[:,:,l], boundary=self.boundary, mode=self.mode)
            
      else:             # U.shape = nURows x nUCols x 1 x 1
        
        V = np.empty( (uDims[0], uDims[1], 1, hDims[2]), dtype=float)
                
        L = np.floor( ( np.asarray(H[:,:,0].shape) - 0.5)/2. )
        L = L.astype(int)+1
                
        for l in range( hDims[2] ):        # Filter (2D matrix) must be rotated by 180 degrees

            ##v_pad = signal.convolve2d(np.pad(U[:,:,0], ((L[0],L[0]), (L[1],L[1]) ), mode='symmetric'), 
                                   ##np.flip(np.flip(H[:,:,l],1),0), boundary='fill', mode='full')
            ##V[:,:,0,l] = v_pad[-U.shape[0]:,-U.shape[1]:]

            #V[:,:,0,l] = signal.convolve2d(U[:,:,0], np.flip(np.flip(H[:,:,l],1),0), boundary=self.boundary, mode=self.mode)
        
            v_pad = signal.convolve2d(U[:,:,0], np.flip(np.flip(H[:,:,l],1),0), boundary=self.boundary, mode='full')
        
            V[:,:,0,l] = v_pad[L[0]:L[0]+U.shape[0],L[1]:L[1]+U.shape[1]] 
            
      return(V)



    def fun_FB2D(self, H, x, transpose=0, shape_Ax=None):

      #if self.vecFlag:                  # if input data x is vectorized
         #origShape = x.shape
         #x.shape = self.xShape          # then re-shape it as a matrix (image).
        
      if shape_Ax is None:

          if self.vecFlag:                  # if input data x is vectorized
            origShape = x.shape
            if transpose==0:               
               x.shape = self.xShape          # then re-shape it as coeffient map.
            else:
               x.shape = self.bShape          # then re-shape it as a matrix (image).
            
            
          if x.shape[2] > 1:            # For CSC, it may be of purely academic interest to compare
                                        # the spatial implementation versus the frequency one. If len(x.shape) == 5
                                        # the the problem at hand is CDL, which becomes computationally
                                        # prohibitive.
            z = self.colorFB2D(x, H, transpose)
          else:
            z = self.grayFB2D(x, H, transpose)
        
        
          if self.vecFlag:        # if input data x is vectorized
             z = z.ravel()        # then force output to be vectorized.
             x.shape = origShape
           
      else:
        
        if len(x.shape) == 3: # x --> xRows, xCols, xCh          xCh is either 1 (artificially added by the class ImgRead)
                              #                                  or the number of channels (i.e. colors)
           z = ( x.shape[0], x.shape[1], x.shape[2], H.shape[2])

        if len(x.shape) == 4: # x --> xRows, xCols, xCh, nInputs
           z = ( x.shape[0], x.shape[1], x.shape[2], H.shape[2], x.shape[3])

        #z = x.shape
      
      return z
      
    # -------------------------------------------------------
    # -------------------------------------------------------

    #def Dx(U):
      #v = np.diff(U,axis=1)
    
      #return np.concatenate( (v, np.zeros((U.shape[0],1),dtype=float)),axis=1 )

    #def Dy(U):
      #v = np.diff(U,axis=0)
    
      #return np.concatenate( (v, np.zeros((1,U.shape[1]),dtype=float)),axis=0 )

    # -------------------------------------------------------
    # -------------------------------------------------------


    def color_dwFB2D(self, U, H, trans, dwFactor):
      r"""
      H.shape = nFRows x nFcols x nFilters
      """
      
      pass


    def gray_dwFB2D(self, U, H, trans, dwFactor):
      r"""
      H.shape = nFRows x nFcols x nFilters
      """

      uDims = U.shape
      hDims = H.shape
    
      
      
      if trans == 0:    # U.shape = nURows x nUCols x 1 x nFilters
        
        tmp = np.zeros( (uDims[0], uDims[1] ), dtype=float)
        V   = tmp[::dwFactor,::dwFactor]
        
        for l in range( hDims[2] ):  # NOTE: fix this so it uses a 'Wavelet'-like approach
            tmp = signal.convolve2d( U[:,:,0,l], H[:,:,l], boundary=self.boundary, mode=self.mode)
            V  += tmp[::dwFactor,::dwFactor]
            
      else:             # U.shape = nURows x nUCols x 1 x 1
        
        
        V = np.empty( (uDims[0]*dwFactor, uDims[1]*dwFactor, 1, hDims[2]), dtype=float)
                
        L = np.floor( ( np.asarray(H[:,:,0].shape) - 0.5)/2. )
        L = L.astype(int)+1
                
        tmp = np.zeros( (uDims[0]*dwFactor, uDims[1]*dwFactor ), dtype=float)
        tmp[::dwFactor,::dwFactor] = U[:,:,0]
        
        for l in range( hDims[2] ):        # Filter (2D matrix) must be rotated by 180 degrees

        
            v_pad = signal.convolve2d(tmp, np.flip(np.flip(H[:,:,l],1),0), boundary=self.boundary, mode='full')
        
            V[:,:,0,l] = v_pad[L[0]:L[0]+U.shape[0]*dwFactor,L[1]:L[1]+U.shape[1]*dwFactor] 
            
      return(V)


    def fun_dwFB2D(self, H, x, transpose=0, shape_Ax=None):

      #if self.vecFlag:                  # if input data x is vectorized
         #origShape = x.shape
         #x.shape = self.xShape          # then re-shape it as a matrix (image).
        
      if shape_Ax is None:

          if self.vecFlag:                  # if input data x is vectorized
            origShape = x.shape
            if transpose==0:          
               x = self.f2oJax.setShape(x, self.xShape)         # then re-shape it as coeffient map.
            else:
               x = self.f2oJax.setShape(x, self.bShape)         # then re-shape it as a matrix (image).
            
            
          if x.shape[2] > 1:            # For CSC, it may be of purely academic interest to compare
                                        # the spatial implementation versus the frequency one. If len(x.shape) == 5
                                        # the the problem at hand is CDL, which becomes computationally
                                        # prohibitive.
            z = self.color_dwFB2D(x, H, transpose, self.dwFactor)
          else:
            z = self.gray_dwFB2D(x, H, transpose, self.dwFactor)
        
        
          if self.vecFlag:        # if input data x is vectorized
             z = z.ravel()        # then force output to be vectorized.             
             x = self.f2oJax.setShape(x, origShape)
           
      else:
        
        if self.dwFactor is None:
           self.dwFactor = 2
           print('Using downsampling FB2D without setting dwFactor... using x2')
        
        if len(x.shape) == 3: # x --> xRows, xCols, xCh          xCh is either 1 (artificially added by the class ImgRead)
                              #                                  or the number of channels (i.e. colors)
           z = ( x.shape[0]*self.dwFactor, x.shape[1]*self.dwFactor, x.shape[2], H.shape[2])

        if len(x.shape) == 4: # x --> xRows, xCols, xCh, nInputs
           z = ( x.shape[0], x.shape[1], x.shape[2], H.shape[2], x.shape[3])

        #z = x.shape
      
      return z
      
    # -------------------------------------------------------
    # -------------------------------------------------------


    
    def fun_TV2D(self, H, x, transpose=0, shape_Ax=None):
    #
    # This function will be use with a 'direct' TV solver like ADMM
    #
    # 0.5\| H(x) - b \|_2^2 --> same as matrix-times-vector or
    #                           conv2D
    
      r"""Matrix times vector.

      """
    
      if shape_Ax is None:
  
        if transpose==0:
          z = A.dot(x)
        else:
          z = A.transpose().dot(x)
  
      else:
        z = (A.shape[1],)
        
      return z
      pass

      
    # -------------------------------------------------------
    # -------------------------------------------------------

    def fun_Id_dualTV2D(self, H, x, transpose=0, shape_Ax=None):
    #
    # This func0.5\|b-D'*z\|_2^2 tion will be use with a 'dual' TV solver like FISTA
    #
    # Denoising 0.5\| x - b \|_2^2 + l\| \nabla x \|_1 --> 0.5\|u - (b-D'*z) \|_2^2 - 0.5\|b-D'*z\|_2^2  D = [Dx' Dy']', z = [z1' z2']'
    #
    # then : 0.5\|b - D'*z\|_2^2 would be the equivalent linOp
    #                           
      
      if not hasattr(self, 'opD'): 
         self.opD = opDxDy()
    
      if shape_Ax is None:
  
  
        if self.vecFlag:                  			# if input data x is vectorized
           origShape = x.shape
           if transpose==0:          
               x = self.f2oJax.setShape(x, self.xShape)         # then re-shape it as Dx and Dy components.
           else:
               x = self.f2oJax.setShape(x, self.bShape)         # then re-shape it as a matrix (image).
  
  
        if transpose==0:
    
  
           z = self.opD.DxT(x[...,0]) + self.opD.DyT(x[...,1])
          
          
          
        else:

          DxVar = self.opD.Dx(x)
          DyVar = self.opD.Dy(x)
        
          z = self.f2oJax.jnp.concatenate( (DxVar[...,self.f2oJax.jnp.newaxis],DyVar[...,self.f2oJax.jnp.newaxis]), axis=-1)



        if self.vecFlag:        # if input data x is vectorized
           z = z.ravel()        # then force output to be vectorized.             
           x = self.f2oJax.setShape(x, origShape)

  
      else:
        z = (*x.shape,2)
        
      return z


    def fun_rec_dualTV2D(self, Ax, x, extraVar):
    # given the dual var, compute the current primal var
      
      # FIXME: in case of TV-SuperRes, the shape should include SR-factor
      # extraVar : [ args, b, lambda]
      return extraVar[1]-extraVar[2]*self.f2oJax.setShape(Ax(x,0,None), extraVar[1].shape)

    # -------------------------------------------------------
    # -------------------------------------------------------

    def fun_conv_dualTV2D(self, H, x, transpose=0, shape_Ax=None):
    #
        pass
      
    # -------------------------------------------------------
    # -------------------------------------------------------
    
    def sel_Ax(self, fCostClass=None):
      
      if fCostClass is None:
         switcher = {
          f2oDef.fAx.Identity.val:   self.fun_Identity,
          f2oDef.fAx.matrixvec.val:  self.fun_mv,
          f2oDef.fAx.conv2D.val:     self.fun_conv2D,
          f2oDef.fAx.FB2D.val:       self.fun_FB2D,
          f2oDef.fAx.TV2D.val:       self.fun_TV2D,
          f2oDef.fAx.dwFB2D.val:     self.fun_dwFB2D
          }
      
      else:

        if fCostClass is f2oDef.cost.L2TV_lin: # FIXME: this selection should only occur with dual
	
            switcher = {
              f2oDef.fAx.Identity.val:   self.fun_Id_dualTV2D,
              f2oDef.fAx.conv2D.val:     self.fun_conv_dualTV2D  
              }
        else:	# FIXME:same as fCostClass is None
            switcher = {
              f2oDef.fAx.Identity.val:   self.fun_Identity,
              f2oDef.fAx.matrixvec.val:  self.fun_mv,
              f2oDef.fAx.conv2D.val:     self.fun_conv2D,
              f2oDef.fAx.FB2D.val:       self.fun_FB2D,
              f2oDef.fAx.TV2D.val:       self.fun_TV2D,
              f2oDef.fAx.dwFB2D.val:     self.fun_dwFB2D
              }
      
      
      linOp = switcher.get(self.linOp.val, "nothing")
      
      # NOTE: if HAVE_JAX, then getDataIn transfers data to 'device', otherwise returns the same array
      func = lambda x,trans,shape_Ax: linOp(self.f2oJax.getDataIn(self.A), x, trans, shape_Ax)
      
      return func
      
      
    # -------------------------------------------------------
    # -------------------------------------------------------

    def sel_primalVar(self, Ax, fCostClass=None):
      
      if fCostClass is None:
         switcher = {
          f2oDef.fAx.Identity.val:   self.fun_rec_dualId,
          f2oDef.fAx.matrixvec.val:  self.fun_rec_dualId,
          f2oDef.fAx.conv2D.val:     self.fun_rec_dualId,
          f2oDef.fAx.FB2D.val:       self.fun_rec_dualId,
          f2oDef.fAx.TV2D.val:       self.fun_rec_dualId,
          f2oDef.fAx.dwFB2D.val:     self.fun_rec_dualId    
          }
      
      else:

        if fCostClass is f2oDef.cost.L2TV_lin: # FIXME: this selection should only occur with dual
        
            switcher = {
              f2oDef.fAx.Identity.val:   self.fun_rec_dualTV2D,
              f2oDef.fAx.conv2D.val:     self.fun_rec_dualTV2D  
              }
        else:   # FIXME:same as fCostClass is None. Use a general def (also for sel_Ax)
            switcher = {
              f2oDef.fAx.Identity.val:   self.fun_rec_dualId,
              f2oDef.fAx.matrixvec.val:  self.fun_rec_dualId,
              f2oDef.fAx.conv2D.val:     self.fun_rec_dualId,
              f2oDef.fAx.FB2D.val:       self.fun_rec_dualId,
              f2oDef.fAx.TV2D.val:       self.fun_rec_dualId,
              f2oDef.fAx.dwFB2D.val:     self.fun_rec_dualId    
              }

      fRec = switcher.get(self.linOp.val, "nothing")
      
      # NOTE: if HAVE_JAX, then getDataIn transfers data to 'device', otherwise returns the same array
      func = lambda x,extraPar: fRec(Ax, x, extraPar)

      return func

    # -------------------------------------------------------
    # -------------------------------------------------------
    
    def fRec(self, x, aux=None):

      fw = self.sel_Ax()

    
      tmpVal = self.dwFactor         
      self.dwFactor = 1
  
      uRec = fw(x, 0, None)
  
      if tmpVal is not None:
         self.dwFactor = tmpVal
    
          
      if len(self.bShape) == 3:
         if self.bShape[2] == 1:
            uRec.shape = (self.bShape[0]*self.dwFactor, self.bShape[1]*self.dwFactor)
         else:
            uRec.shape = (self.bShape[0]*self.dwFactor, self.bShape[1]*self.dwFactor, self.bShape[2])
      
    
      if aux is not None:
        uRec += np.reshape(aux, uRec.shape)
        
      return uRec
      
#=========================================================================
#=========================================================================



class fwOp_f(fwOpVarsMixin):
    r"""Functions for forward operator (frequency).

    """
    def __init__(self, linOp=None, A=None, enableJAX=None):
      
        self.set_fwOpvars(linOp, A, enableJAX)      
        self.freqSol = True
        self.thresh  = None

        
    # -------------------------------------------------------
    # -------------------------------------------------------

    def fun_Identity(self, H, x, trans=0, shape_Ax=None):
                
        
      
      if shape_Ax is None:

         z = x
         
      else:
      
         z = x.shape
         
      return(z)
    

    def fun_conv2D(self, H, x, trans=0, shape_Ax=None):
                
        
      # x.shape = xRows, xCols, xCh
      # H.shape = xRows, xCols
      
      if shape_Ax is None:

        if trans==0:

          if self.vecFlag:                                   # if input data x is vectorized
            origShape = x.shape
            x = self.f2oJax.setShape(x, self.xShape)          # then re-shape it as a matrix (image).


          if len(x.shape)==3:
            z = x*H[...,np.newaxis]     
          else:
            z = x*H
    
        else:
          
          if self.vecFlag:                                   # if input data x is vectorized
            origShape = x.shape
            x = self.f2oJax.setShape(x,self.bShape)          # then re-shape it as a matrix (image).
          
          if len(x.shape)==3:
            z = x*( self.f2oJax.jnp.conj(H)[..., np.newaxis] )
          else:
            z = x*self.f2oJax.jnp.conj(H)
    
        
        if self.vecFlag:                                     # if input data x is vectorized
           z = z.ravel()                                     # then force output to be vectorized.
           x = self.f2oJax.setShape(x, origShape)
           
      else:
        
        z = x.shape


    
      return z

    # -------------------------------------------------------
    # -------------------------------------------------------


    def fun_FB2D(self, H, x, trans=0, shape_Ax=None):
               
      
      if shape_Ax is None:



        if trans==0:

          if self.vecFlag:                                  # if input data x is vectorized
            
            origShape = x.shape
            x = self.f2oJax.setShape(x, self.xShape)        # then re-shape it as a matrix (image).


        # x.shape =    (1) xRows, xCols, 1, hFil                  (grayscale, CSC like)
        #              (2) xRows, xCols, xCh, xFil                (color, CSC like)
        #              (3) xRows, xCols, 1, xFil, nInputs         (grayscale w. n training images, CDL like)
        #              (4) xRows, xCols, xCh, xFil, nInputs       (color w. n training images, CDL like)
        #
        # H.shape = xRows, xCols, hFil -->   (i)  xRows, xCols, 1, hFil
        #                                    (ii) xRows, xCols, 1, hFil, 1
        
          if len(x.shape) == 5:    
            
            
#            FB = np.swapaxes( H[...,np.newaxis,np.newaxis], 2, 3 )
#            
#            z = np.sum( x*FB, axis=3 )          # for l in range( hDims[2] ):
#                                                #     z += x[:,:,:,l,:]*FB[:,:,:,l,:]                          
 
            z = self.f2oJax.jnp.einsum('ijl,ijmln->ijmn',H,x) 
            
          else:
            
#            FB = np.swapaxes( H[...,np.newaxis], 2, 3 )
#            
#            z = np.sum( x*FB, axis=3 )          # for l in range( hDims[2] ):
#                                                #     z += x[:,:,:,l]*FB[:,:,:,l]
 
            z = self.f2oJax.jnp.einsum('ijl,ijml->ijm',H,x)
 

        else:   #trans is 1

          if self.vecFlag:                              # if input data x is vectorized
            
            origShape = x.shape
            x = self.f2oJax.setShape(x, self.bShape)    # then re-shape it as a matrix (image).
            


        # x.shape =    (1) xRows, xCols, 1                  (grayscale, CSC like)
        #              (2) xRows, xCols, xCh                (color, CSC like)
        #              (3) xRows, xCols, 1, nInputs         (grayscale w. n training images, CDL like)
        #              (4) xRows, xCols, xCh, nInputs       (color w. n training images, CDL like)
        #
        # H.shape = xRows, xCols, hFil

          

          if len(x.shape) == 4:    
          
#           FB = np.swapaxes( H[...,np.newaxis,np.newaxis], 2, 3 )
#           z = x[...,np.newaxis]*np.conj(FB)                   # for l in range( hDims[2] ):
#                                                               #     z[:,:,:,l,:] = x*np.conj(FB[:,:,:,l])

            z = self.f2oJax.jnp.einsum('ijmn,ijk->ijmkn',x, self.f2oJax.jnp.conj(H))
                          
          else:

#            FB = np.swapaxes( H[...,np.newaxis], 2, 3 )
#            
#            z = x[...,np.newaxis]*np.conj(FB)
 
            z = self.f2oJax.jnp.einsum('ijm,ijk->ijmk',x, self.f2oJax.jnp.conj(H))
        
        
        if self.vecFlag:        # if input data x is vectorized
           z = z.ravel()        # then force output to be vectorized.
           
           x = self.f2oJax.setShape(x, origShape)
           
           
      else: #  
        
        if len(x.shape) == 3: # x --> xRows, xCols, xCh          xCh is either 1 (artificially added by the class ImgRead)
                              #                                  or the number of channels (i.e. colors)
           z = ( x.shape[0], x.shape[1], x.shape[2], H.shape[2])

        if len(x.shape) == 4: # x --> xRows, xCols, xCh, nInputs
           z = ( x.shape[0], x.shape[1], x.shape[2], H.shape[2], x.shape[3])
      
      return(z)
    
    # -------------------------------------------------------
    # -------------------------------------------------------


    # -------------------------------------------------------
    # -------------------------------------------------------


    def sel_interpFilter(self):
      
      switcher = {
          'F2':  np.array([1., 2., 1.],ndmin=2).transpose()/2.,
          'F3':  np.array([-1., 0., 9., 16., 9., 0., -1.],ndmin=2).transpose()/16.,
          'F4':  np.array([-3., 0., 19., 32., 19., 0., -3.],ndmin=2).transpose()/32.,
          'F5':  np.array([3., 0., -25., 0., 150., 256., 150., 0., -25., 0., -3.],ndmin=2).transpose()/256.
          }       

      return(switcher.get(self.interpFlag, "nothing"))

    def fun_dwFB2D(self, H, x, trans=0, shape_Ax=None):
               
      
      if self.interp is None:
         h = self.sel_interpFilter()
         self.interp = np.outer(h,h)
         
      if shape_Ax is None:



        if trans==0:

          if self.vecFlag:                  # if input data x is vectorized
            origShape = x.shape
            x = self.f2oJax.setShape(x, self.xShape)          # then re-shape it as a matrix (image).


        
          if len(x.shape) == 5:    
                                         
            v = self.f2oJax.jnp.einsum('ijl,ijmln->ijmn',H,x) 
            
          else:            
               
            v = self.f2oJax.jnp.einsum('ijl,ijml->ijm',H,x)
 
          #print('Shapes x, H, v', x.shape, H.shape, v.shape)
          # downsampling op

          #vT = self.f2oJax.irfft2(v, s=(H.shape[0],2*(H.shape[1]-1)), axes=(0,1) ).copy()
          vT = self.f2oJax.irfft2(v, s=(H.shape[0],2*(H.shape[1]-1)), axes=(0,1) )
          z  = self.f2oJax.rfft2( vT[::self.dwFactor,::self.dwFactor], axes=(0,1) )

          

        else:

          if self.vecFlag:                 # if input data x is vectorized
            origShape = x.shape
            x = self.f2oJax.setShape(x, self.bShape)          # then re-shape it as a matrix (image).

          
          vT = self.f2oJax.irfft2( x, s=(self.bShape[0],2*(self.bShape[1]-1)), axes=(0,1) )

          #xT = ImgRead.scale(self, vT, 1./float(self.dwFactor), interpKind='cubic')

          if (len(vT.shape) == 3):
              #xT = self.f2oJax.jimg(vT, (vT.shape[0]*self.dwFactor,vT.shape[1]*self.dwFactor,vT.shape[2]), 'bilinear' )
              xT = np.zeros( (vT.shape[0]*self.dwFactor,vT.shape[1]*self.dwFactor,vT.shape[2]), dtype='float');
          if (len(vT.shape) == 4):
              xT = np.zeros( (vT.shape[0]*self.dwFactor,vT.shape[1]*self.dwFactor,vT.shape[2],vT.shape[3]), dtype='float');
              #xT = self.f2oJax.jimg(vT, (vT.shape[0]*self.dwFactor,vT.shape[1]*self.dwFactor,vT.shape[2],vT.shape[3]), 'bilinear')
              

          #xT[::self.dwFactor,::self.dwFactor] = vT
          #xT = self.f2oJax.jsp.signal.convolve2d(tmp[:,:,0], (1.0/4.0)*self.f2oJax.jnp.ones((2,2),dtype=float), boundary='symm', mode='same')
          
          
          xT[::self.dwFactor,::self.dwFactor] = self.f2oJax.getDataOut(vT)
          # jax convolve2d has open issues (12/2021)
          
          ##h0 = np.array([-1., 0., 9., 16., 9., 0., -1.])/16.
          ##h0.shape = (7,1)
          ###hh = np.array([[0.25, 0.5 , 0.25], [0.5 , 1.  , 0.5 ], [0.25, 0.5 , 0.25]])
          ##hh = np.outer(h0,h0)
          
          xT[:,:,0] = signal.convolve2d(xT[:,:,0], self.interp, boundary='symm', mode='same')
          #xT[:,:,0] = signal.convolve2d(xT[:,:,0], (1.0/4.5)*np.ones((2,2),dtype=float), boundary='symm', mode='same')
          #xT[0:vT.shape[0],0:vT.shape[1]] = vT          # equivalent to Frequency interpolation
          
          
          #print('fun_dwFB2D', self.thresh)
          if self.thresh is not None: 
             xT = xT * ( np.abs(xT) >= 0.1*self.thresh )
          
          if len(x.shape) == 4:    
          
            z = self.f2oJax.jnp.einsum('ijmn,ijk->ijmkn', self.f2oJax.rfft2( self.f2oJax.getDataIn(xT), axes=(0,1) ), 
                                                          self.f2oJax.jnp.conj(H))
                          
          else:

            z = self.f2oJax.jnp.einsum('ijm,ijk->ijmk', self.f2oJax.rfft2( self.f2oJax.getDataIn(xT), axes=(0,1) ), 
                                                        self.f2oJax.jnp.conj(H))
        
        
        del vT
        
        if self.vecFlag:        # if input data x is vectorized
           z = z.ravel()        # then force output to be vectorized.
           x = self.f2oJax.setShape(x, origShape)
           
      else: #  
        
        if len(x.shape) == 3: # x --> xRows, xCols, xCh          xCh is either 1 (artificially added by the class ImgRead)
                              #                                  or the number of channels (i.e. colors)
           z = ( x.shape[0]*self.dwFactor, x.shape[1]*self.dwFactor-(self.dwFactor-1), x.shape[2], H.shape[2])

        if len(x.shape) == 4: # x --> xRows, xCols, xCh, nInputs
           z = ( x.shape[0]*self.dwFactor, x.shape[1]*self.dwFactor-(self.dwFactor-1), x.shape[2], H.shape[2], x.shape[3])
      
      return(z)
    
    # -------------------------------------------------------
    # -------------------------------------------------------



    def sel_Ax(self):
      switcher = {
          f2oDef.fAx.Identity.val:   self.fun_Identity,
          f2oDef.fAx.conv2D.val:     self.fun_conv2D,
          f2oDef.fAx.FB2D.val:       self.fun_FB2D,
          f2oDef.fAx.dwFB2D.val:     self.fun_dwFB2D
          }
      
      linOp = switcher.get(self.linOp.val, "nothing")
      
      func = lambda x,trans,shape_Ax: linOp(self.Af, x, trans, shape_Ax)
      
      return func

    # -------------------------------------------------------
    # -------------------------------------------------------


    def sel_AxExplicit(self):
      switcher = {
          f2oDef.fAx.Identity.val:   self.fun_Identity,
          f2oDef.fAx.conv2D.val:     self.fun_conv2D,
          f2oDef.fAx.FB2D.val:       self.fun_FB2D,
          f2oDef.fAx.dwFB2D.val:     self.fun_dwFB2D
          }
      
      #linOp = self.f2oJax.getJIT( switcher.get(self.linOp, "nothing") )
      linOp = switcher.get(self.linOp.val, "nothing")
      
      func = lambda Hf, x,trans,shape_Ax: linOp(Hf, x, trans, shape_Ax)
      
      return func
    
    # -------------------------------------------------------
    # -------------------------------------------------------
    
    
    def fRec(self, x, aux=None, getROI=None, HF=None):


      tmpF = self.dwFactor         
      tmpV = self.vecFlag

      self.dwFactor = 1         # force value to avoid problems with z = fw(x, 0, None)
      self.vecFlag = False
        
        
  
      if HF is None:         
        fw = self.sel_Ax()
        z = fw(x, 0, None)
      else:
        
        if not self.f2oJax.have_jax:
           pyfftw.interfaces.cache.disable()       # case in which several
                                                   # reconstruction are done with different FB
        fw = self.sel_AxExplicit()
        z = fw(HF, x, 0, None)

      if tmpF is not None:
         self.dwFactor = tmpF
    
      self.vecFlag = tmpV
          
          
      #if len(self.bShape) == 3:
         #if self.bShape[2] == 1:
            #z.shape = (self.bShape[0]*self.dwFactor, self.bShape[1]*self.dwFactor)
         #else:
            #z.shape = (self.bShape[0]*self.dwFactor, self.bShape[1]*self.dwFactor, self.bShape[2])


      #fw = self.sel_Ax()
      
      #z = fw(x, 0, None)
  
      #if len(self.bShape) == 3:
         #if self.bShape[2] == 1:
            #z.shape = (self.bShape[0], self.bShape[1])
         #else:
            #z.shape = self.bShape
      
      #print(x.shape, z.shape)
      #try:
        #print(uRec.shape)
      #except:
        #print('no uRec')
      
      if self.dwFactor == 1 or self.dwFactor is None:
      
        if getROI is not None:
           uRec = getROI( self.f2oJax.irfft2( z, s=self.padShape[:2], axes=(0,1)) )
        else:         
           if self.padFlag: # NOTE: self.getROI is set by the calling routine... in the context of an
                         #       optimization problem, it would be set by 'f'APG, 'f'ADMM, etc. ('f': freq)
              uRec = self.getROI( self.f2oJax.irfft2( z, s=self.padShape[:2], axes=(0,1)) )
           else:
              uRec = self.f2oJax.irfft2( z, s=self.padShape[:2], axes=(0,1))
    
      else: # FIXME: consider other cases than just wrap BC (circular conv)
      
        if getROI is not None:
           #print('getROI none')
           uRec = getROI( self.f2oJax.irfft2( z, s=tuple( self.dwFactor*np.array(self.padShape[:2])), axes=(0,1)) )
        else:         
           if self.padFlag: # NOTE: self.getROI is set by the calling routine... in the context of an
                         #       optimization problem, it would be set by 'f'APG, 'f'ADMM, etc. ('f': freq)
              uRec = self.getROI( self.f2oJax.irfft2( z, s=tuple( self.dwFactor*np.array(self.padShape[:2])), axes=(0,1)) )
           else:
              uRec = self.f2oJax.irfft2( z, s=tuple( self.dwFactor*np.array(self.padShape[:2])), axes=(0,1))
      
      
      if aux is not None:
        uRec += self.f2oJax.setShape(aux, uRec.shape)
        
      return self.f2oJax.getDataOut(uRec)
    
#=========================================================================
#=========================================================================

class conv2DOp(object):
    r"""Predefined convolution operators  
    """
    def __init__(self):
      self.list = None
      self.dtype = 'float'
      self.filName = None



    def gauss2D(self, shape=(3,3), sigma=0.5):
      
      m,n = [(ss-1.)/2. for ss in shape]
      
      y,x = np.ogrid[-m:m+1,-n:n+1]
      
      h = np.exp( -(x*x + y*y) / (2.*sigma*sigma) )
      h[ h < np.finfo(h.dtype).eps*h.max() ] = 0
      sumh = h.sum()
      
      if sumh != 0:
          h /= sumh
                    
      return h


    def average(self, shape=(5,5)):
      h = np.ones(shape, dtype = self.dtype)
      h /= np.float(h.size)

      return h


    def laplace(self, shape=(3,3)):
      
      h = np.zeros(shape, dtype = self.dtype)
      h[0,1] = 1.
      h[2,1] = 1.
      h[1,0] = 1.
      h[1,2] = 1.
      h[1,1] = -4.
      
      return h


    def DxDy(self, shape=(2,2)):
      Dx = np.zeros(shape, dtype = self.dtype)
      Dy = np.zeros(shape, dtype = self.dtype)
      
      Dx[0,:] = np.array([1., -1.])
      Dy[:,0] = np.array([1., -1.])

      return Dx, Dy


class HPFiltered(object):
    r"""High-pass filter given image. To be used along CSC/CSR  
    """
    def __init__(self):
      self.list = None
      self.dtype = 'float'
      self.filName = None

