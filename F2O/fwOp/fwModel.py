import scipy as sc
import matplotlib.pylab as PLT
import numpy as np

from scipy import signal
import pyfftw

import F2O.constants as CTE



__author__ = """Paul Rodriguez <prodrig@pucp.edu.pe>"""



class fwOp(object):
    r"""Functions for forward operator (spatial).

    """
        self.linOp     = CTE.fAx_matrixvec
        self.A         = None           #matrix

        self.bShape    = None           # Original shape of the observed data
        self.xShape    = None           # Original shape of the solution
        
        self.boundary  = 'symm'
        self.mode      = 'same'
        self.vecFlag   = False
        self.label     = None
        self.freqSol   = False          # The Convolution theorem applies, thus the operator
                                        # will operate in the frequency domain


    # -------------------------------------------------------
    # -------------------------------------------------------
    
    def fun_mv(self, A, x, trans=0, shape_Ax=None):
      r"""Matrix times vector.

      """
  
      if shape_Ax is None:
  
        if trans==0:
          z = A.dot(x)
        else:
          z = A.transpose().dot(x)
  
      else:
        z = (A.shape[1],)
        #tmp = A.dot(x)
        #z   = len(tmp)
        
      return z


    # -------------------------------------------------------
    # -------------------------------------------------------
    
    
    def colorConv2d(self, U, H):
      nDims = U.shape
    
      V = 0*U
      for k in range( nDims[2] ):
          V[:,:,k] = signal.convolve2d(U[:,:,k], H, boundary=self.boundary, mode=self.mode)
    
      return(V)


    def conv2D(self, U, H):

      if len(U.shape)==3:
         z = colorConv2d(U, H, boundary=self.boundary, mode=self.mode)
      else:
         z = signal.convolve2d(U, H, boundary=self.boundary, mode=self.mode)

      return(z)


    def fun_conv2D(self, H, x, trans=0, shape_Ax=None):
        
      if self.vecFlag:                  # if input data x is vectorized
         origShape = x.shape
         x.shape = self.xShape          # then re-shape it as a matrix (image).
        
      if shape_Ax is None:

        if trans==0:
                
          if len(x.shape)==3:
            z = self.colorConv2d(x, H)
          else:
            z = signal.convolve2d(x, H, boundary=self.boundary, mode=self.mode)
    
        else:
          if len(x.shape)==3:
            z = self.colorConv2d(x, np.flip(np.flip(H,1),0) )
          else:
            L = np.floor( ( np.asarray(H.shape) + 0.5)/2. )
            L = L.astype(int)
            
            z_pad = signal.convolve2d(np.pad(x, ((L[0],L[0]), (L[1],L[1]) ), mode='symmetric'), 
                                   np.flip(np.flip(H,1),0), boundary='fill', mode='full')
            z = z_pad[-x.shape[0]:,-x.shape[1]:]
            #z = signal.convolve2d(x,  np.flip(np.flip(H,1),0), boundary=self.boundary, mode=self.mode)
    
        
        if self.vecFlag:        # if input data x is vectorized
           z = z.ravel()        # then force output to be vectorized.
           x.shape = origShape
           
      else:
        z = x.shape


    
      return z

    # -------------------------------------------------------
    # -------------------------------------------------------

    def colorFB2D(self, U, H, trans):
      r"""
      H.shape = nFRows x nFcols x nFilters
      """
      
      uDims = U.shape
      hDims = H.shape
    
      if trans == 0:    # U.shape = nURows x nUCols x nUCh x nFilters
        
        V = np.zeros( (uDims[0], uDims[1], uDims[2]), dtype=float)
        
        for l in range( hDims[2] ):        
          for k in range( nDims[2] ):
            V[:,:,k] += signal.convolve2d(U[:,:,k,l], H[:,:,l], boundary=self.boundary, mode=self.mode)
          
      else:             # U.shape = nURows x nUCols x nUCh x 1
        
        V = np.empty( (uDims[0], uDims[1], uDims[2], hDims[2]), dtype=float)
        for l in range( hDims[2] ):        
          for k in range( nDims[2] ):
            V[:,:,k,l] = signal.convolve2d(U[:,:,k], np.flip(np.flip(H[:,:,l],1),0), boundary=self.boundary, mode=self.mode)
        
        
      return(V)


    def grayFB2D(self, U, H, trans):
      r"""
      H.shape = nFRows x nFcols x nFilters
      """
      
      uDims = U.shape
      hDims = H.shape
    
      
      
      if trans == 0:    # U.shape = nURows x nUCols x 1 x nFilters
        
        V = np.zeros( (uDims[0], uDims[1] ), dtype=float)
        
        for l in range( hDims[2] ):        
            V += signal.convolve2d( U[:,:,0,l], H[:,:,l], boundary=self.boundary, mode=self.mode)
            
      else:             # U.shape = nURows x nUCols x 1 x 1
        
        V = np.empty( (uDims[0], uDims[1], 1, hDims[2]), dtype=float)
                
        L = np.floor( ( np.asarray(H[:,:,0].shape) - 0.5)/2. )
        L = L.astype(int)+1
                
        for l in range( hDims[2] ):        # Filter (2D matrix) must be rotated by 180 degrees

            ##v_pad = signal.convolve2d(np.pad(U[:,:,0], ((L[0],L[0]), (L[1],L[1]) ), mode='symmetric'), 
                                   ##np.flip(np.flip(H[:,:,l],1),0), boundary='fill', mode='full')
            ##V[:,:,0,l] = v_pad[-U.shape[0]:,-U.shape[1]:]

            #V[:,:,0,l] = signal.convolve2d(U[:,:,0], np.flip(np.flip(H[:,:,l],1),0), boundary=self.boundary, mode=self.mode)
        
            v_pad = signal.convolve2d(U[:,:,0], np.flip(np.flip(H[:,:,l],1),0), boundary=self.boundary, mode='full')
        
            V[:,:,0,l] = v_pad[L[0]:L[0]+U.shape[0],L[1]:L[1]+U.shape[1]] 
            
      return(V)



    def fun_FB2D(self, H, x, transpose=0, shape_Ax=None):

      #if self.vecFlag:                  # if input data x is vectorized
         #origShape = x.shape
         #x.shape = self.xShape          # then re-shape it as a matrix (image).
        
      if shape_Ax is None:

          if self.vecFlag:                  # if input data x is vectorized
            origShape = x.shape
            if transpose==0:               
               x.shape = self.xShape          # then re-shape it as coeffient map.
            else:
               x.shape = self.bShape          # then re-shape it as a matrix (image).
            
            
          if x.shape[2] > 1:            # For CSC, it may be of purely academic interest to compare
                                        # the spatial implementation versus the frequency one. If len(x.shape) == 5
                                        # the the problem at hand is CDL, which becomes computationally
                                        # prohibitive.
            z = self.colorFB2D(x, H, transpose)
          else:
            z = self.grayFB2D(x, H, transpose)
        
        
          if self.vecFlag:        # if input data x is vectorized
             z = z.ravel()        # then force output to be vectorized.
             x.shape = origShape
           
      else:
        
        if len(x.shape) == 3: # x --> xRows, xCols, xCh          xCh is either 1 (artificially added by the class ImgRead)
                              #                                  or the number of channels (i.e. colors)
           z = ( x.shape[0], x.shape[1], x.shape[2], H.shape[2])

        if len(x.shape) == 4: # x --> xRows, xCols, xCh, nInputs
           z = ( x.shape[0], x.shape[1], x.shape[2], H.shape[2], x.shape[3])

        #z = x.shape
      
      return z
      
    # -------------------------------------------------------
    # -------------------------------------------------------

    #def Dx(U):
      #v = np.diff(U,axis=1)
    
      #return np.concatenate( (v, np.zeros((U.shape[0],1),dtype=float)),axis=1 )

    #def Dy(U):
      #v = np.diff(U,axis=0)
    
      #return np.concatenate( (v, np.zeros((1,U.shape[1]),dtype=float)),axis=0 )


    def fun_TV2D(self, H, x, transpose=0, shape_Ax=None):
      
      pass

      #if self.vecFlag:                  # if input data x is vectorized
         #origShape = x.shape
         #x.shape = self.xShape          # then re-shape it as a matrix (image).
        
      #if shape_Ax is None:
        
          #udx = self.Dx(x)
          #udy = self.Dy(x)
        
          #if self.vecFlag:        # if input data x is vectorized
             #z = z.ravel()        # then force output to be vectorized.
             #x.shape = origShape
           
      #else:
        #z = x.size
      
    # -------------------------------------------------------
    # -------------------------------------------------------

    
    def sel_Ax(self):
      switcher = {
          CTE.fAx_matrixvec:  self.fun_mv,
          CTE.fAx_conv2D:     self.fun_conv2D,
          CTE.fAx_FB2D:       self.fun_FB2D,
          CTE.fAx_TV2D:       self.fun_TV2D
          }
      
      linOp = switcher.get(self.linOp, "nothing")
      
      func = lambda x,trans,shape_Ax: linOp(self.A, x, trans, shape_Ax)
      
      return func
      
      
    # -------------------------------------------------------
    # -------------------------------------------------------

    
    def fRec(self, x, aux=None):

      fw = self.sel_Ax()
      
      uRec = fw(x, 0, None)
  
      if len(self.bShape) == 3:
         if self.bShape[2] == 1:
            uRec.shape = (self.bShape[0], self.bShape[1])
         else:
            uRec.shape = self.bShape
      
    
      if aux is not None:
        uRec += np.reshape(aux, uRec.shape)
        
      return uRec
      
#=========================================================================
#=========================================================================


class fwOp_f(object):
    r"""Functions for forward operator (frequency).

    """
    def __init__(self):
      
        self.linOp     = CTE.fAx_matrixvec
        self.A         = None                   #
        self.Af        = None                   #

        self.bShape    = None           # Original shape of the observed data
        self.xShape    = None           # Original shape of the solution
        
        self.boundary  = 'symm'
        self.mode      = 'same'
        self.vecFlag   = False
        self.label     = None


        
    # -------------------------------------------------------
    # -------------------------------------------------------



    def fun_conv2D(self, H, x, trans=0, shape_Ax=None):
                
        
      # x.shape = xRows, xCols, xCh
      # H.shape = xRows, xCols
      
      if shape_Ax is None:

        if trans==0:

          if self.vecFlag:                  # if input data x is vectorized
            origShape = x.shape
            x.shape = self.xShape          # then re-shape it as a matrix (image).

          if len(x.shape)==3:
            z = x*H[...,np.newaxis]     
          else:
            z = x*H
    
        else:
          
          if self.vecFlag:                  # if input data x is vectorized
            origShape = x.shape
            x.shape = self.bShape          # then re-shape it as a matrix (image).
          
          if len(x.shape)==3:
            z = x*( np.conj(H)[..., np.newaxis] )
          else:
            z = x*np.conj(H)
    
        
        if self.vecFlag:        # if input data x is vectorized
           z = z.ravel()        # then force output to be vectorized.
           x.shape = origShape
           
      else:
        z = x.size


    
      return z

    # -------------------------------------------------------
    # -------------------------------------------------------


    def fun_FB2D(self, H, x, trans=0, shape_Ax=None):
               
      
      if shape_Ax is None:



        if trans==0:

          if self.vecFlag:                  # if input data x is vectorized
            origShape = x.shape
            x.shape = self.xShape          # then re-shape it as a matrix (image).


        # x.shape =    (1) xRows, xCols, 1, hFil                  (grayscale, CSC like)
        #              (2) xRows, xCols, xCh, xFil                (color, CSC like)
        #              (3) xRows, xCols, 1, xFil, nInputs         (grayscale w. n training images, CDL like)
        #              (4) xRows, xCols, xCh, xFil, nInputs       (color w. n training images, CDL like)
        #
        # H.shape = xRows, xCols, hFil -->   (i)  xRows, xCols, 1, hFil
        #                                    (ii) xRows, xCols, 1, hFil, 1
        
          if len(x.shape) == 5:    
            
            
#            FB = np.swapaxes( H[...,np.newaxis,np.newaxis], 2, 3 )
#            
#            z = np.sum( x*FB, axis=3 )          # for l in range( hDims[2] ):
#                                                #     z += x[:,:,:,l,:]*FB[:,:,:,l,:]                          
 
            z = np.einsum('ijl,ijmln->ijmn',H,z) 
            
          else:
            
#            FB = np.swapaxes( H[...,np.newaxis], 2, 3 )
#            
#            z = np.sum( x*FB, axis=3 )          # for l in range( hDims[2] ):
#                                                #     z += x[:,:,:,l]*FB[:,:,:,l]
 
            z = np.einsum('ijl,ijml->ijm',H,x)
 

        else:

          if self.vecFlag:                 # if input data x is vectorized
            origShape = x.shape
            x.shape = self.bShape          # then re-shape it as a matrix (image).


        # x.shape =    (1) xRows, xCols, 1                  (grayscale, CSC like)
        #              (2) xRows, xCols, xCh                (color, CSC like)
        #              (3) xRows, xCols, 1, nInputs         (grayscale w. n training images, CDL like)
        #              (4) xRows, xCols, xCh, nInputs       (color w. n training images, CDL like)
        #
        # H.shape = xRows, xCols, hFil

          

          if len(x.shape) == 4:    
          
#           FB = np.swapaxes( H[...,np.newaxis,np.newaxis], 2, 3 )
#           z = x[...,np.newaxis]*np.conj(FB)                   # for l in range( hDims[2] ):
#                                                               #     z[:,:,:,l,:] = x*np.conj(FB[:,:,:,l])

            z = np.einsum('ijmn,ijk->ijmkn',x, np.conj(H))
                          
          else:

#            FB = np.swapaxes( H[...,np.newaxis], 2, 3 )
#            
#            z = x[...,np.newaxis]*np.conj(FB)
 
            z = np.einsum('ijm,ijk->ijmk',x, np.conj(H))
        
        
        if self.vecFlag:        # if input data x is vectorized
           z = z.ravel()        # then force output to be vectorized.
           x.shape = origShape
           
      else: #  
        
        if len(x.shape) == 3: # x --> xRows, xCols, xCh          xCh is either 1 (artificially added by the class ImgRead)
                              #                                  or the number of channels (i.e. colors)
           z = ( x.shape[0], x.shape[1], x.shape[2], H.shape[2])

        if len(x.shape) == 4: # x --> xRows, xCols, xCh, nInputs
           z = ( x.shape[0], x.shape[1], x.shape[2], H.shape[2], x.shape[3])
      
      return(z)
    
    # -------------------------------------------------------
    # -------------------------------------------------------


    def sel_Ax(self):
      switcher = {
          CTE.fAx_conv2D:     self.fun_conv2D,
          CTE.fAx_FB2D:       self.fun_FB2D
          }
      
      linOp = switcher.get(self.linOp, "nothing")
      
      func = lambda x,trans,shape_Ax: linOp(self.Af, x, trans, shape_Ax)
      
      return func

    # -------------------------------------------------------
    # -------------------------------------------------------

    
    def fRec(self, x, aux=None):

      fw = self.sel_Ax()
      
      z = fw(x, 0, None)
  
      if len(self.bShape) == 3:
         if self.bShape[2] == 1:
            z.shape = (self.bShape[0], self.bShape[1])
         else:
            z.shape = self.bShape
      
      uRec = pyfftw.interfaces.numpy_fft.irfft2( z, axes=(0,1))
    
      if aux is not None:
        uRec += np.reshape(aux, uRec.shape)
        
      return uRec
    
#=========================================================================
#=========================================================================

class conv2DOp(object):
    r"""Predefined convolution operators  
    """
    def __init__(self):
      self.list = None
      self.dtype = 'float'
      self.filName = None



    def gauss2D(self, shape=(3,3), sigma=0.5):
      
      m,n = [(ss-1.)/2. for ss in shape]
      
      y,x = np.ogrid[-m:m+1,-n:n+1]
      
      h = np.exp( -(x*x + y*y) / (2.*sigma*sigma) )
      h[ h < np.finfo(h.dtype).eps*h.max() ] = 0
      sumh = h.sum()
      
      if sumh != 0:
          h /= sumh
                    
      return h


    def average(self, shape=(5,5)):
      h = np.ones(shape, dtype = self.dtype)
      h /= np.float(h.size)

      return h


    def laplace(self, shape=(3,3)):
      
      h = np.zeros(shape, dtype = self.dtype)
      h[0,1] = 1.
      h[2,1] = 1.
      h[1,0] = 1.
      h[1,2] = 1.
      h[1,1] = -4.
      
      return h


    def DxDy(self, shape=(2,2)):
      Dx = np.zeros(shape, dtype = self.dtype)
      Dy = np.zeros(shape, dtype = self.dtype)
      
      Dx[0,:] = np.array([1., -1.])
      Dy[:,0] = np.array([1., -1.])

      return Dx, Dy

