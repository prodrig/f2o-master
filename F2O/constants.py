
from enum import Enum, IntEnum, unique

@unique
class ss(Enum):
  Cte            = (0, 'Cte step-size')
  Cauchy         = (1, 'Cauchy')
  CauchyLagged   = (2, 'Cauchy-BB (lagged)')
  CauchyRand     = (3, 'Cauchy rand')
  CauchySupp     = (4, 'Cauchy supp')
  CauchyLSupp    = (5, 'Cauchy-BB supp')
  BBv1           = (6, 'BBv1')
  BBv2           = (7, 'BBv2')
  BBv3           = (8, 'BBv3')
  AA             = (9, 'Anderson Acc')
  dualTV         = (10,'dual TV (Fista)')
  CteCauchy      = (11,'Cte. auto')

  def __init__(self, val, txt): 
      self.val  = val 
      self.txt  = txt 
        
  @classmethod
  def printSS(cls): 
      for k in cls: 
        print('ss.{}:'.format(k.name), k.val, k.txt)
  

@unique
class roGrad(Enum):

  Null       = (0, ' ')
  lI         = (1, '$\ell_\infty$ rule')
  Rel        = (2, 'Relaxed')
  Supp       = (3, 'Support')


  def __init__(self, val, txt): 
      self.val  = val 
      self.txt  = txt 
        
  @classmethod
  def printSS(cls): 
      for k in cls: 
        print('roGrad.{}:'.format(k.name), k.val, k.txt)




stop_nIter        = 0   # exits when nIter is reach
stop_l2xk         = 1   # exits when || x_k - x_{k-1} ||_2 <= thresh
stop_dGap         = 2   # Duality gap T.B.I.

f2o_stop_l2Tol    = 1e-5


@unique
class noise(Enum):

  Null       = (0, 'No noise (equivalent to identity) ')
  Gaussian   = (1, 'Gaussian')
  SnP        = (2, 'Salt and Pepper')
  RVIN       = (3, 'Random value impulse noise')
  Poisson    = (4, 'Poisson')
  Gamma      = (5, 'Gamma (multiplicative)')
  Rayleigh   = (6, 'Rayleigh (multiplicative)')


  def __init__(self, val, txt): 
      self.val  = val 
      self.txt  = txt 
        
  @classmethod
  def printSS(cls): 
      for k in cls: 
        print('noise.{}:'.format(k.name), k.val, k.txt)

#noise_None        = 0   # no noise; equivalent to identity function
#noise_Gaussian    = 1
#noise_SnP         = 2   # impulse -- salt and pepper
#noise_RVIN        = 3   # random value impulse noise
#noise_Poisson     = 4
#noise_Gamma       = 5
#noise_Rayleigh    = 6

@unique
class cost(IntEnum):
  L2_lin      = 0   # L2 fidelity term, with lineal f(x):               0.5||  f(x) - b ||_2^2
  L2L2_lin    = 1   # L2/L2 fidelity/regularization terms (Tikhonov), 
                       # with lineal f(x)                                  0.5||  f(x) - b ||_2^2  +  \lambda/2||x||_2^2

  L2L1_lin    = 2   # L2/L1 fidelity/regularization terms (Basis Pursuit), 
                       # with lineal f(x)                                  0.5||  f(x) - b ||_2^2  +  \lambda||x||_1

  L2TV_lin    = 3   # L2/TV fidelity/regularization terms (i.e. Total Variation), 
                       # with lineal f(x)                                  0.5||  f(x) - b ||_2^2  +  \lambda|| \nabla x ||_1
                       
  def forceDual(self):
      return self.value == self.getEnum().L2TV_lin.value
  
  @classmethod
  def getEnum(cls):
      # cls here is the enumeration
      return cls


grad_L2_Lin         = 0   # Gradient corresponds to cost_L2_lin
grad_L2_LinF        = 1   # Gradient corresponds to cost_L2_lin, but computed in the frequency domain

grad_L2L2_lin       = 2   # Gradient corresponds to cost_L2L2_lin

grad_L2L2_LinF      = 3   # Gradient corresponds to cost_L2L2_lin, but computed in the frequency domain




@unique
class fAx(Enum):

  matrixvec     = (0, 'Matrix times vector')
  conv1D        = (1, '1D convolution')
  conv2D        = (2, '1D convolution')
  FB2D          = (3, '2D filterbank')   # Filter bank H.shape = nRows x nCols x nElements
  dwFB2D        = (4, '2D filterbank + downsampling')
  Identity      = (6, 'Identity')
  TV2D          = (-1, 'TV deprecated')
  
  def __init__(self, val, txt): 
      self.val  = val 
      self.txt  = txt 
        
  @classmethod
  def printfAx(cls): 
      for k in cls: 
        print('fwOperator.{}:'.format(k.name), k.val, k.txt)



prox_None       = 0
prox_l1         = 1
prox_l0         = 2
proj_l1         = 3
proj_l0         = 4
prox_TV         = 5
prox_l0TV       = 6


@unique
class iseq(Enum):

  ntrv     = (0, "Nesterov's inertial seq.") 
  lin      = (1, 'Linear inertial seq.')
  glin     = (2, 'Generalized inertial seq.')

  def __init__(self, val, txt): 
      self.val  = val 
      self.txt  = txt 
        
  @classmethod
  def printSS(cls): 
      for k in cls: 
        print('iseq.{}:'.format(k.name), k.val, k.txt)


