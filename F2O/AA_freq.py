import scipy as sc
import matplotlib.pylab as PLT
import numpy as np


from F2O.F2O_utils import argsF2O, stepSize, stopCondF2O, costGradF2O, proxOp, ISeq

from F2O.fwOp.fwOperator import conv2DOp

from F2O.F2O_freq import commomPGD



from timeit import default_timer as timer


import multiprocessing

import pyfftw

pyfftw.config.NUM_THREADS = multiprocessing.cpu_count()
pyfftw.config.PLANNER_EFFORT = 'FFTW_MEASURE'
pyfftw.interfaces.cache.enable()
pyfftw.interfaces.cache.set_keepalive_time(300)


__author__ = """Paul Rodriguez <prodrig@pucp.edu.pe>"""



def AA_pg(Op, b, lmbda, nIter=100, args=None):  
  r"""Anderson accelaration
    
  where 
  
  * Ax represent the function (operations) to be carried out in the frequency domain.
    See fwOp_f class in fwOperator.py
  * The gradient computation is carried out by the costGradF2O class (see F2O_utils.py), which
    uses the Ax operator
  * b, the observed data, is already in the frequency domain
    
  """
  #=================================================================
  
  
  
  if args is None:
    return(None, None)
    
    
  # Initialization

  pg = commomPGD(False)          # True: accelerated PG. False --> just PG

  args.lmbda = lmbda
  x, Ax, f, stopFunc, pgStats, itemStats = pg.Init(Op, b, nIter, args)
    
  
  # Get subroutine (e.g. step-size computation, stoping criteria, etc)
  computeCost, computeGrad, computeSS, prox = pg.SetFunctions(args, f, Ax, nIter)
  
  
  # NOTE: args.Bf <--> FFT(b), Op.Af <--> FFT(fwOp), x is in freq (grad as well)
  
  x_sptl = np.zeros(1)
  y_sptl = x_sptl.copy()
  
  R = None
  Gx = None
  
  # Elapse time per iteration (after initializations)
  pg.iterTimer(args.Timer, True, False)

  
  for k in range(nIter):
    
    
    grad = computeGrad(x, args.Bf, x_sptl)      # compute gradient
                                                # NOTE: x_sptl is used to compute the cost functional along
                                                # the gradient. See costGradF2O class (in F2O_utils.py)
    alpha = computeSS(k, grad, x, None)         # compute step-size
      
    # --- statistics, record step-Size --
    pg.RecordStats(k, x, args.Bf, grad, pgStats, itemStats, x_sptl, computeCost)
    # --- 

    
    x.shape = args.xShape                       # needed to apply FFT / IFFT
    grad.shape = args.xShape
    
    
    g_sptl = pyfftw.interfaces.numpy_fft.irfft2( x - alpha*grad, axes=(0,1) )                  # fixed-point step
    
    
    y_sptl, beta, Gx, R = AA_extragradStep(args, k, g_sptl, y_sptl, Gx, R)
              
    
    # proximal gradient descent step
    x_sptl = prox(y_sptl, alpha*lmbda)    
    
    x_sptl.shape = g_sptl.shape 
    x      = pyfftw.interfaces.numpy_fft.rfft2( x_sptl, axes=(0,1) ).ravel()
    
    
    if 'ssX' in itemStats.keys():
       pgStats[k,itemStats['ssX']] = np.abs(beta).mean()
    #pgStats[k,1] = np.abs(beta).mean()
    
    if 'time' in itemStats.keys():
       pgStats[k,itemStats['time']] = pg.iterTimer(args.Timer, False, True)
    #pgStats[k,2] = pg.iterTimer(args.Timer, False, True)        # Elapse time
    
    if stopFunc(k, x_sptl.ravel(), pgStats):                    # check for exit condition / print stats
      print('Stop criteria -- iteration {0}'.format(k))
      break
    
  
  if args.xShape is not None:
    x.shape = args.xShape
  
  
  return(x, pgStats)
  
  
  
  
  
def AA_extragradStep(args, k, g_sptl, y_sptl, Gx=None, R=None):  
  r"""Anderson accelaration extra-gradient step
    
  """
  #=================================================================

  if k < args.AAlength:
      
     if k == 0 or Gx is None :
      
        Gx = np.zeros( (g_sptl.ravel().shape[0], args.AAlength), dtype=float)
        R  = np.zeros( (g_sptl.ravel().shape[0], args.AAlength), dtype=float)
      
      
     Gx[:,k] = g_sptl.ravel()
     R[:,k]  = g_sptl.ravel() - y_sptl.ravel()
     
  else:
      
     Gx = np.concatenate( ( Gx[:,1:args.AAlength], g_sptl.ravel()[...,np.newaxis]), axis=1)
     R  = np.concatenate( ( R[:,1:args.AAlength], g_sptl.ravel()[...,np.newaxis]-y_sptl.ravel()[...,np.newaxis]), axis=1)
    
    
  if k == 0:
     beta = 1./np.dot(R.ravel(),R.ravel())
    
  else:
      
     if k < args.AAlength:
        rMat = R[:,0:k+1].transpose().dot(R[:,0:k+1])
     else:
        rMat = R.transpose().dot(R)
      
   
   
     rMat /= np.linalg.norm(rMat, 2)
    
     rMatSol = np.linalg.lstsq(rMat + 1e-8 * np.eye(rMat.shape[1]), np.ones(rMat.shape[1]), rcond=None)
      
     beta = rMatSol[0] / rMatSol[0].sum()
    
    
    
  if k < args.AAlength:
     y_sptl = Gx[:,0:k+1].dot( beta ).ravel()
  else:
     y_sptl = Gx.dot( beta ).ravel()


  return y_sptl, beta, Gx, R

