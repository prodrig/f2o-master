
import scipy as sc
import matplotlib.pylab as PLT
import numpy as np


from F2O.F2O_utils import argsF2O, stepSize, stopCondF2O, proxOp, ISeq, costGradF2O, robustGrad
from F2O.F2O_utils import f2oJaxUtils
import F2O.constants as f2oDef

from timeit import default_timer as timer

import multiprocessing
import pyfftw

pyfftw.config.NUM_THREADS = multiprocessing.cpu_count()
pyfftw.config.PLANNER_EFFORT = 'FFTW_MEASURE'
pyfftw.interfaces.cache.enable()
pyfftw.interfaces.cache.set_keepalive_time(300)


__author__ = """Paul Rodriguez <prodrig@pucp.edu.pe>"""


    #r"""simple routine associated with solution in the frequency domain.
    #For this class of problem, the forward operator *is* convolution and 
    #the cost function is of the form
    
             #f( H*x - b) + \lambda\cdot g(x)
             
    #where f is quadratic and g is any function whose proximal operator
    #is computational afordable.
    
    #Examples:
    
        #Tikhonov      :  0.5|| H*x - b ||_2^2 + \lambda || L*x ||_2           
        #Basis Pursuit :  0.5|| H*x - b ||_2^2 + \lambda || x ||_1
        #CSC           :  0.5|| \sum_k H_k * x_k - b ||_2^2 + \lambda \sum_k || x_k ||_1
        #CDL           :
        
    #"""


# ======================================================
# ======================================================


def gd(Op, b, nIter=100, args=None):
  r"""


  """
  #=================================================================

  if args is None:
    return(None, None)
    
    
    
  # Initialization

  gd = commomPGD(False)          # True: accelerated PG. False --> just PG


  # input data is transfered to 'device' (if needed)
  bDev = args.f2oJax.getDataIn(b)  

  x, Ax, f, stopFunc, gdStats, itemStats = gd.Init(Op, bDev, nIter, args)
    
  
  # Get subroutine (e.g. step-size computation, stoping criteria, etc)
  computeCost, computeGrad, computeSS = gd.SetFunctions(args, f, Ax, nIter)
  
  # NOTE: args.Bf <--> FFT(b), Op.Af <--> FFT(fwOp), x is in freq (grad as well)
  
  
  # Elapse time per iteration (after initializations)
  gd.iterTimer(args.Timer, True, False)

  
  for k in range(nIter):
    
    
    grad = computeGrad(x, args.Bf)              # compute gradient
                                                # NOTE: x_sptl is used to compute the cost functional along
                                                # the gradient. See costGradF2O class (in F2O_utils.py)
    alpha, grad = computeSS(k, grad, x)               # compute step-size
    
    
    # --- statistics, record step-Size --
    gd.RecordStats(k, x, args.Bf, grad, gdStats, itemStats, None, computeCost)

    if 'ssX' in itemStats.keys():
       gdStats[k,itemStats['ssX']] = args.f2oJax.jnp.mean(alpha)
    # ---                     
    
    
    # gradient descent step
    x = x - alpha*grad 

    
    #pgStats[k,2] = pg.iterTimer(args.Timer, False, True)        # Elapse time
    if 'time' in itemStats.keys():
       gdStats[k,itemStats['time']] = gd.iterTimer(args.Timer, False, True)
    
    if stopFunc(k, x.ravel(), gdStats):                    # check for exit condition / print stats
      print('Stop criteria -- iteration {0}'.format(k))
      break
    
  
  
  x = args.f2oJax.setShape(x, args.xShape)
  # if needed, change xOut shape
  #xOut.shape = args.bShape
  
  xOut = args.getROI( args.f2oJax.getDataOut( args.f2oJax.irfft2(x, s=args.padShape[:2], axes=(0,1)) ) )
    
  
  
  return( xOut, gdStats)
  

# ======================================================
# ======================================================


def agd(Op, b, lmbda, nIter=100, args=None):

  pass



# ======================================================
# ======================================================

def Tikhonov_f(H, b, lmbda, args=None, L=None):
  r"""
  Solve the Tikhonov regularization problem:

    0.5|| H*x - b ||_2^2 + \lambda/2 || L*x ||_2^2
    
  where H is a covolutional kernel and b is the input (observed) data, 
  in the frequency domain via

    Hf = FFT(H)
    bf = FFT(b)
    
    x = IFFT( ( conj(Hf).*bf ) ./ (conf(Hf).*Hf + \lambda I ) ) 
    
  Three specialized cases are considered:
  
    (1) 0.5|| H*x - b ||_2^2 + \lambda/2 || x ||_2^2    (L is None)
    (2) 0.5|| x - b ||_2^2 + \lambda/2 || L*x ||_2^2    (H is None)
    (3) 0.5|| H*x - b ||_2^2 + \lambda/2 || L*x ||_2^2  (General case)
  """
  #=================================================================

  


  if args.bShape is None:
     args.bShape = b.shape


  # Sanity checks
  if H is not None:
    if args.hShape is None:
       args.hShape = H.shape
  else:
    args.hShape = np.asarray((0,0))  # None
    args.Hf = 1.

  if L is not None:
    if args.lShape is None:
       args.lShape = L.shape
  else:    
    args.lShape = np.asarray((0,0)) # None
    args.Lf = args.f2oJax.getDataIn(1.)
  
  
  # pad FFTs
  if args.Bf is None:   
     args.Bf, args.padShape = args.compute_padFFT2(b, padFlag=args.padFlag)
  
  if args.Hf is None:
     args.Hf = args.compute_KernelFFT(H)

  if args.Lf is None:
     args.Lf = args.compute_KernelFFT(L)


  den = args.f2oJax.jnp.conj(args.Hf)*args.Hf + lmbda*args.f2oJax.jnp.conj(args.Lf)*args.Lf

  if len(b.shape) == 2:
     z = args.f2oJax.irfft2( ( args.Bf*np.conj(args.Hf) ) / ( den ), axes=(0,1))


  if len(b.shape) == 3:
     if np.isscalar(args.Hf):
        z = args.f2oJax.irfft2( args.Bf / ( den[...,np.newaxis] ), axes=(0,1))
     else:
        z = args.f2oJax.irfft2( ( args.f2oJax.jnp.einsum('ijc,ij->ijc', args.Bf, args.f2oJax.jnp.conj(args.Hf)) ) / 
                                                                                 ( den[...,np.newaxis] ), axes=(0,1))

  x = args.f2oJax.jnp.clip( args.getROI( z ), args.clipVmin, args.clipVmax)

    
    
    
  return x




class commomPGD(object):
    r"""Commom routines for proximal gradient and accelerated variants 
        when solved in the frequency domain
    """
    def __init__(self, accelerated):
        self.accelerated = accelerated
        self.Tstart      = None
        
        
    def Init(self, Op, b, nIter, args):
      r"""
      GD: Simple initializations:
      """

      # Sanity check
      if Op.A is None:
         if args.fCostClass >= f2oDef.cost.L2TV_lin:
           args.hShape = (1,1)
         else:
           print('Forward operator was no set ... If FreqOp = fwOp_f(), then set FreqOp.A')
           #return None
           print('A randomized op. will be used')
      else:
         if args.hShape is None:
            args.hShape = Op.A.shape
         
      if Op.freqSol is False:
         print('Trying to use a spatial-based operator alog with frequency based routines?')
         return None
         
      if args.bShape is None:
         args.bShape = b.shape
      
      if args.freqSol is not True:
         args.freqSol = True
         
      args.dwFactor = Op.dwFactor   
      # ---

      # set this falg before calling compute_padFFT2
      if Op.linOp == f2oDef.fAx_FB2D:
         args.lphpFlag = True
      else:
         args.lphpFlag = False


      args.Bf, args.padShape = args.compute_padFFT2(b, padFlag=args.padFlag )

      #if args.fCostClass == f2oDef.cost.L2TV_lin: # FIXME: this is a test
        #args.Bf, args.padShape = args.compute_padFFT2(b, padFlag=args.padFlag, forceShp=(2,2))
      #else:
        #args.Bf, args.padShape = args.compute_padFFT2(b, padFlag=args.padFlag)        
      #print( 'padFFt', args.padShape )
      
      
      
      Op.bShape = args.Bf.shape                   # Shape of the input / observed data
      Op.padShape = args.padShape    
      
      
      
      Op.Af = args.compute_KernelFFT(Op.A, Op.dwFactor, Op.cardfwOp)
      Op.getROI    = args.getROI
      Op.padFlag   = args.padFlag                 # in case a padding policy is used (symmetric, etc.)
                                                  # this value is needed for reconstruction
                                                  
      if args.fCostClass >= f2oDef.cost.L2TV_lin:  # Operator "includes" DxDy
         Op.Df = args.compute_DxDyFFT(Op.dwFactor, Op.cardfwOp)
      
      
      Op.vecFlag = True                           # Data is assumed to be vectorized
    
      Ax = Op.sel_Ax()
    
      Op.xShape  = Ax(args.Bf, 0, 1)              # Shape of the solution in the freq. domain
    
      args.xShape = Op.xShape                     # 
      args.bfShape = Op.bShape
    
      if args.dwFactor is None:
        args.xSptlShape = args.padShape        
      else:
        args.xSptlShape = (args.padShape[0]*args.dwFactor, args.padShape[1]*args.dwFactor)  # FIXME: check other than
                                                                                            #        circular conv (or wrap BC)
        
      fCost = costGradF2O(Ax, args)
      fCost.padShape  = args.padShape
      fCost.padOffset = args.padOffset
      fCost.getROI    = args.getROI
      fCost.padMode   = args.padMode
      fCost.padFlag   = args.padFlag
    
      if args.f2oJax.have_jax:
        
        x = args.f2oJax.getDataIn( np.zeros((np.prod(Op.xShape),),dtype='complex64') )
        
      else:

        x = pyfftw.empty_aligned(Op.xShape, dtype='complex64').ravel()
        x[:] = np.zeros((np.prod(Op.xShape),),dtype='complex64')

      # Select stoping criteria
      stopVar = stopCondF2O(args, nIter)
      stopFun = stopVar.sel_SC()

      # Create variable for statistics (if needed)
      if stopVar.dict['last'] >= 1:
        gdStats = np.zeros((nIter, stopVar.dict['last']),dtype='float')
      else:
        gdStats = None
      
      itemStats = stopVar.dict.copy()
  
      ## FIXME: fix this
      #tmp = args.fCost + args.alpha + args.gradL2 + args.gradLi+1
      #if tmp >= 1:
        #gdStats = np.zeros((nIter,tmp),dtype='float')
      #else:
        #gdStats = None
      
    
      return x, Ax, fCost, stopFun, gdStats, itemStats
        
        
    def SetFunctions(self, args, f, Ax, nIter):
  
      # select cost and gradient functions  
      computeCost, computeGrad = f.sel_cost()
  
    
      # Select SS (step-size) function
      ssVar = stepSize(args,  Ax)
      
      #computeSS = stepSize.sel_SS(ssVar)
      computeSS = ssVar.sel_SS()


      roGradVar = robustGrad(args)
      ssVar.roGradFun = roGradVar.sel_roGrad()


      if args.fCostClass == f2oDef.cost.L2_lin:
         return computeCost, computeGrad, computeSS


      # Select prox / proj function    
      ppVar = proxOp(args)
      computeProx = proxOp.sel_ppOp(ppVar)
    
      if self.accelerated is False:
         return computeCost, computeGrad, computeSS, computeProx
       
       
      # Select inertial sequence    
      iseqVar = ISeq(args)
      computeISeq = ISeq.sel_iseqOp(iseqVar)
    
    
      return computeCost, computeGrad, computeSS, computeProx, computeISeq
        
        
    def RecordStats(self, k, x, b, grad, gdStats, itemStats, x_sptl, computeCost):
      r"""
      PG: Record statistics associated with the evolution of PG
      """
    
      if gdStats is not None:
      
        if 'cost' in itemStats.keys():
           gdStats[k,itemStats['cost']] = computeCost(x, b.ravel(), x_sptl)
      
        if 'gL2' in itemStats.keys():
           gdStats[k,itemStats['gL2']] =  sum(grad.ravel()*grad.ravel())        # \| grad \|_2^2

        if 'gLi' in itemStats.keys():
           gdStats[k,itemStats['gLi']] =  abs(grad.ravel()).max()               # \| grad \|_\infty
      
        #if gdStats.shape[1] >= 1:
           #gdStats[k,0] = computeCost(x, b.ravel(), x_sptl)
      
        #if gdStats.shape[1] >= 4:
           #gdStats[k,3] =  sum(grad*grad)          # \| grad \|_2^2
           #gdStats[k,4] =  abs(grad).max()         # \| grad \|_\infty
        
        
    def iterTimer(self, flagTimer, flagInit, flagElapse):
      r"""
      Simple routine to elapse time
      """
        
      if flagTimer:
        
        if flagInit:
           self.Tstart = timer()
           
        
        if flagElapse:
           return timer()-self.Tstart
        
      else:
        return None
