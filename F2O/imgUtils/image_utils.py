
import numpy as np
import scipy as sc
from scipy import signal
from scipy.interpolate import interp2d

from SSIM_PIL import compare_ssim

import matplotlib.pyplot as PLT
from PIL import Image, ImageOps


import F2O.F2O_utils as F2O
from F2O.F2O_freq import Tikhonov_f
from F2O.F2O_sptl import Tikhonov_viaCG as Tikhonov_s

from F2O.fwOp.fwOperator import fwOp, conv2DOp

from F2O.F2O_utils import f2oJaxUtils


__author__ = """Paul Rodriguez <prodrig@pucp.edu.pe>"""


# ======================================================
# ======================================================


class ImgMetrics(object):
    r"""Quality metrics for images 
    """
    def __init__(self,enableJAX=None):
      self.list    = None
      self.valSnr  = []
      self.valPsnr = []
      self.valSsim = []
      self.valMse  = []
      self.f2oJax  = f2oJaxUtils(flagForceJAX=enableJAX)


    def snr(self, Ref, U):
    
      num = np.var( Ref.flatten() )
      den = np.mean( np.abs( Ref.flatten() - U.flatten() )**2 )
    
      return( 10*np.log10(num/den))
  

    def psnr(self, Ref, U):
    
      #num = ( Ref.max() - Ref.min() )**2
      #den = np.mean( np.abs( Ref.flatten() - U.flatten() )**2 )
      num = np.float(Ref.size)
      den = np.sum( (Ref.flatten() - U.flatten() )**2 )
      
      return( 10*np.log10(num/den))


    def mse(self, Ref, U):
    
      return( np.mean( np.abs( Ref.flatten() - U.flatten() )**2 ) )


    def ssim(self, Ref, U):

      vMin = Ref.min()
      vMax = Ref.max()
      
      rShape = Ref.shape
      if len(rShape) == 3:
         if rShape[2] == 1:
            #Ref.shape = (rShape[0],rShape[1])
            Ref = self.f2oJax.setShape(Ref, (rShape[0],rShape[1]))
        
      uShape = U.shape
      if len(uShape) == 3:
         if uShape[2] == 1:
            #U.shape = (uShape[0],uShape[1])
            U = self.f2oJax.setShape(U, (uShape[0],uShape[1]))
      
      charRef = Image.fromarray( np.uint8( np.floor( 255*(Ref-vMin)/(vMax-vMin) ) ) )
    
      vMin = U.min()
      vMax = U.max()
      charU = Image.fromarray( np.uint8( np.floor( 255*(U-vMin)/(vMax-vMin) ) ) )
    
      # retsore original shape
      Ref = self.f2oJax.setShape(Ref, rShape)
      U   = self.f2oJax.setShape(U, uShape)
    
      return( compare_ssim(charRef, charU) )
    
    
    def appendEmpty(self):
    
      self.valSnr.append([])
      self.valPsnr.append([])
      self.valSsim.append([])
      self.valMse.append([])
    
      return

    def computeAll(self, Ref, U, k):

      self.valSnr[k].append( self.snr(Ref, U) )
      self.valPsnr[k].append( self.psnr(Ref, U) )
      self.valMse[k].append( self.mse(Ref, U) )
      self.valSsim[k].append( self.ssim(Ref, U) )
      
      return 
    


class ImgPlot(object):
    r"""Display images  
    """
    def __init__(self,enableJAX=None):
      self.list = None
      self.txtN    = []
      self.imgShow = []
      self.f2oJax  = f2oJaxUtils(flagForceJAX=enableJAX)

    def plotNImgs(self, U, N=None, txtN=None, winSize=4):
      

      if isinstance(U, list):

        if N is None:
           N = len(U)

        figure, f = PLT.subplots(ncols=N, figsize=(winSize*N, winSize))
           
        for k in np.arange(N):
            vMin = U[k].min()
            vMax = U[k].max()
            
            newAxisFlag = False
            uShape = U[k].shape
            if len(uShape) == 3:
              if uShape[2] == 1:
                 discardAxis = True
              else:
                 discardAxis = False
            else:
              discardAxis = False
                 
            if N > 1:
              g = f[k]
            else:
              g = f
              
            if discardAxis:
              g.imshow(Image.fromarray( np.uint8( np.floor( 255*(U[k][:,:,0]-vMin)/(vMax-vMin) ) ) ), cmap='gray')
            else:
              g.imshow(Image.fromarray( np.uint8( np.floor( 255*(U[k]-vMin)/(vMax-vMin) ) ) ), cmap='gray')
            
            if txtN is not None:
              g.set_title(txtN[k],fontsize='16')
              g.set_axis_off()
              
    
      else:

        figure, f = PLT.subplots(ncols=1, figsize=(winSize, winSize))

        vMin = U.min()
        vMax = U.max()

        uShape = U.shape
        if len(uShape) == 3:
           if uShape[2] == 1:
                U = self.f2oJax.setShape( U, (uShape[0],uShape[1]) )
                 
        f.imshow(Image.fromarray( np.uint8( np.floor( 255*(U-vMin)/(vMax-vMin) ) ) ), cmap='gray')
            
        if txtN is not None:
           f.set_title(txtN,fontsize='16')
           f.set_axis_off()


    
      return          



class ImgRead(object):
    r"""Display images  
    """
    #def __init__(self, cscFlag=False, hSize=8, blurLmbda=10):
    def __init__(self,enableJAX=None):
      self.list = []
      self.normalize = 255.
      self.cscInitFlag   = False
      self.reshape = None
      #self.hSize     = hSize
      #self.blurLmbda = blurLmbda
      #self.addNoise  = None
      self.f2oJax    = f2oJaxUtils(flagForceJAX=enableJAX)
      
      
    def readListImgs(self):
      
      L = len(self.list)
      
      #sanity check
      if L > 0:
        u = []
      else:
        u = None
      
      #if self.cscFlag:
         #kernel = conv2DOp()  
         #H = kernel.laplace( (self.hSize,self.hSize) )         # Laplacian
         #uLP = []
         #uHP = []
      
         #args = F2O.argsF2O()
      
      if self.reshape is None:
        rShp = lambda im: im
      else:
        rShp = lambda im: ImageOps.fit(im, (self.reshape[0], self.reshape[1]), method=3)

      
      for k in np.arange(L):
        
        if self.list[k][1] == 'g':          
          imgs = rShp( ImageOps.grayscale( Image.open(self.list[k][0]) ) )
          
        if self.list[k][1] == 'c':          
          imgs = rShp( Image.open(self.list[k][0]) )                    
           
        u.append( self.f2oJax.getDataIn( np.asarray(imgs).astype(float)/self.normalize ) )

           
      for k in np.arange(L):
        
        if len(u[k].shape) == 2:           
           u[k] = u[k][..., self.f2oJax.jnp.newaxis]
        #if self.cscFlag:

           #if self.list[k][1] == 'g':    
             
              #uShape = u[k].shape
              #u[k] = u[k][...,np.newaxis]
              
           #args.bShape = u[k].shape
           #args.lShape = H.shape
           #args.Bf     = None
           #args.Lf     = None


           #uLP.append( Tikhonov_f(None, u[k], self.blurLmbda, args, H) )
           #uHP.append( u[k] - uLP[k] )
             
           #if self.list[k][1] == 'g':    
              #u[k].shape = uShape       # For CSC, it is convinient that high-pass image is 
                                        ## nRows, nCols, 1 even if the image is grayscale
                                        ## However, the actual input image does not need to change
                                        ## its shape.
             
    
      return(u)
    
    
    def computeLPHP(self, u, blurLmbda=60, hSize=8, padFlag=False, padMode='symmetric'):

      if self.cscInitFlag == False:
         
         self.cscInitFlag = True
         kernel           = conv2DOp()  
         self.H           = kernel.laplace( (hSize,hSize) )         # Laplacian
      
         self.argsCSC     = F2O.argsF2O()
         self.blurLmbda   = blurLmbda
          
      
    
      if isinstance(u, list):

        #if True: print('computeLPHP', 'input is a list of images')

        for k in range(len(u)):
        
          uShape = u[k].shape
          if len(uShape) == 2:    
             
             u[k] = u[k][...,np.newaxis]
              
          self.argsCSC.bShape = u[k].shape
          self.argsCSC.lShape = self.H.shape
          self.argsCSC.Bf     = None
          self.argsCSC.Lf     = None
        
          self.argsCSC.padFlag  = padFlag
          self.argsCSC.padMode  = padMode
          self.argsCSC.lphpFlag = False         # NOTE: here it should be false (true for reconstruction
                                                # not for computing the lowpass / highpass version of input)
      
          tmp = Tikhonov_f(None, u[k], self.blurLmbda, self.argsCSC, self.H)

          if k == 0:
             uLP = self.f2oJax.copy( tmp )
             uHP = u[k] - uLP
             
             uLP = uLP[...,np.newaxis]
             uHP = uHP[...,np.newaxis]

             
          else:
             uLP = np.concatenate((uLP, tmp[...,np.newaxis]), axis=3)
            
             uHP = np.concatenate((uHP, u[k][...,np.newaxis] - tmp[...,np.newaxis]), axis=3)
           
          u[k] = self.f2oJax.setShape(u[k], uShape)
          
        
      else:

        uShape = u.shape

        if len(uShape) == 2:    
             
           u = u[...,np.newaxis]
              
        self.argsCSC.bShape = u.shape
        self.argsCSC.lShape = self.H.shape
        self.argsCSC.Bf     = None
        self.argsCSC.Lf     = None
      
        self.argsCSC.padFlag  = padFlag
        self.argsCSC.padMode  = padMode
        self.argsCSC.lphpFlag = False
      
        uLP = Tikhonov_f(None, u, self.blurLmbda, self.argsCSC, self.H)


        uHP = u - uLP

        u = self.f2oJax.setShape(u, uShape) # For CSC, it is convinient that high-pass image is 
                                            # nRows, nCols, 1 even if the image is grayscale
                                            # However, the actual input image does not need to change
                                            # its shape.

  
                                            

      #if flagFreq:
        #uLP = Tikhonov_f(None, u, self.blurLmbda, self.argsCSC, self.H)
      #else:
        
        #if len(u.shape) == 3:
           #if u.shape[2] > 1:
              #self.argsCSC.xShape = u.shape
           #else:
              #self.argsCSC.xShape = (u.shape[0],u.shape[1])
        #else:
          #self.argsCSC.xShape = u.shape
          
        #print(u.shape, self.H.shape)
        
        #self.argsCSC.boundary = 'wrap'        
        #self.argsCSC.mode = 'same'
        #uLP = Tikhonov_s(None, u, self.blurLmbda, 20, self.argsCSC, self.H)
        
             

      return uLP, uHP
    
    
    def scale(self, u, dwfactor, interpKind='quintic'):   #cubic
      
        uShp = u.shape
        
        zShp = (int(uShp[0]/dwfactor), int(uShp[1]/dwfactor))
    
        drange = lambda d: np.linspace(0, 1, d)

        
        if len(uShp) == 2:    
          f = interp2d(drange(uShp[0]), drange(uShp[1]), u.transpose(), kind=interpKind)
          return(f(drange(zShp[0]), drange(zShp[1])).transpose())
        
        if len(uShp) == 3:    
           z = np.zeros( (zShp[0],zShp[1],uShp[2]), dtype=u.dtype)
           for k in range(uShp[2]):
               f = interp2d(drange(uShp[0]), drange(uShp[1]), u[:,:,k].transpose(), kind=interpKind)
               z[:,:,k] = f(drange(zShp[0]), drange(zShp[1])).transpose()

           return(z)
    
    
class ImgApplyFwNoise(object):
    r"""Apply forward model, plus noise  
    """
    def __init__(self, origImgList, fwOpList, noise):
      
      self.origImgList      = origImgList
      self.fwOpList         = fwOpList
      self.noise            = noise
      self.computeMetrics   = True
      self.displayImgs      = False
      
    # FIXME: add the case in which fwOpList is None --> i.e. identity fw model
    
    def obsImg(self):
      
      nImgs = len(self.origImgList)
      nFw   = len(self.fwOpList)
      
      addNoise = self.noise.sel_NoiseModel()
      
      
      B       = []
      metrics = ImgMetrics()
      pltImg  = ImgPlot()
            
      
      for k in range(nImgs):
        
        B.append([])
        
        
        if self.computeMetrics:
          metrics.valSnr.append([])
          metrics.valPsnr.append([])
          metrics.valSsim.append([])
          metrics.valMse.append([])
        
        
        if self.displayImgs:
          pltImg.txtN.append( [] )
          pltImg.imgShow.append( [] )
          
          pltImg.txtN[k].append('Original')
          pltImg.imgShow[k].append(self.origImgList[k])


        for n in range( nFw ):

          self.fwOpList[n].bShape = self.origImgList[k].shape
          fAx = self.fwOpList[n].sel_Ax()
          self.fwOpList[n].xShape = fAx(self.origImgList[k],0,1)
          

          B[k].append( addNoise( fAx(self.origImgList[k], 0, None ) ) )
        
          if self.fwOpList[n].vecFlag:
             B[k][n].shape = self.fwOpList[n].xShape
        
          if self.computeMetrics:
             metrics.valPsnr[k].append( metrics.psnr(self.origImgList[k], B[k][n]) )
             metrics.valSnr[k].append( metrics.snr(self.origImgList[k],  B[k][n]) )
             metrics.valMse[k].append( metrics.mse(self.origImgList[k],  B[k][n]) )
             metrics.valSsim[k].append( metrics.ssim(self.origImgList[k], B[k][n]) )
        
        
          if self.displayImgs:
             if self.computeMetrics:
                pltImg.txtN[k].append('Observed (H {}) \n PSNR: {:1.2f} \n SNR: {:1.2f} \n MSE: {:.2e} \n SSIM: {:1.2f}'.format
                                   (self.fwOpList[n].label, metrics.valPsnr[k][n], metrics.valSnr[k][n], 
                                   metrics.valMse[k][n], metrics.valSsim[k][n]) )
             else:
                pltImg.txtN[k].append('Observed (H {})'.format(self.fwOpList[n].label) )
               
             pltImg.imgShow[k].append(B[k][n])
             
        
      return B, metrics, pltImg  
        
        
class ImgApplyFwNoise_f(object):
    r"""Apply forward model, plus noise. This function is intended to be used
        when setting up a simulation
    """
    def __init__(self, origImgList, fwOpList, noise, enableJAX=None):
      
      self.origImgList      = origImgList       # list of images, assumed spatial domain
      self.fwOpList         = fwOpList          # list of fwOp (defined in freq. domain)
      self.noise            = noise
      self.computeMetrics   = True
      self.displayImgs      = False
      self.f2oJax           = f2oJaxUtils(enableJAX)
      
      
    def obsImg(self):
      
      nImgs = len(self.origImgList)
      nFw   = len(self.fwOpList)
      
      addNoise = self.noise.sel_NoiseModel()
      
      
      B       = []
      metrics = ImgMetrics()
      pltImg  = ImgPlot()
            
      args    = F2O.argsF2O()
      
      for k in range(nImgs):
        
        B.append([])
        
        
        if self.computeMetrics:
          metrics.valSnr.append([])
          metrics.valPsnr.append([])
          metrics.valSsim.append([])
          metrics.valMse.append([])
        
        
        if self.displayImgs:
          pltImg.txtN.append( [] )
          pltImg.imgShow.append( [] )
          
          pltImg.txtN[k].append('Original')
          pltImg.imgShow[k].append(self.origImgList[k])


        for n in range( nFw ):


          # set this flag before calling compute_padFFT2
          if self.fwOpList[n].linOp == args.f2oDef.fAx_FB2D:
             args.lphpFlag = True
          else:
             args.lphpFlag = False

          args.padFlag         = self.fwOpList[n].padFlag
          args.padMode         = 'symmetric'               # FIXME: use self.fwOpList[n].boundary ('symm' --> 'symmetric')
      
          # input in the freq. domain
          args.bShape = self.origImgList[k].shape
          args.hShape = self.fwOpList[n].A.shape
          Bf, args.padShape = args.compute_padFFT2(self.origImgList[k], padFlag=args.padFlag)
      
          self.fwOpList[n].bShape = Bf.shape                   # Shape of the input / observed data
          self.fwOpList[n].padShape = args.padShape    


          # fwOp in the freq
          self.fwOpList[n].Af = args.compute_KernelFFT(self.fwOpList[n].A)

          fAx = self.fwOpList[n].sel_Ax()
          self.fwOpList[n].xShape = fAx(Bf,0,1)
           
          # Apply fwOp + noise      
          B[k].append( addNoise( args.getROI( self.f2oJax.irfft2( self.f2oJax.setShape( fAx(Bf, 0, None ), Bf.shape ), s=args.padShape, axes=(0,1) )) ) )
        
          if self.fwOpList[n].vecFlag:
             B[k][n] = self.f2oJax.setShape( B[k][n], self.origImgList[k].shape )
        
          if self.computeMetrics:
             metrics.valPsnr[k].append( metrics.psnr(self.origImgList[k], B[k][n]) )
             metrics.valSnr[k].append( metrics.snr(self.origImgList[k],  B[k][n]) )
             metrics.valMse[k].append( metrics.mse(self.origImgList[k],  B[k][n]) )
             metrics.valSsim[k].append( metrics.ssim(self.origImgList[k], B[k][n]) )
        
        
          if self.displayImgs:
             if self.computeMetrics:
                pltImg.txtN[k].append('Observed (H {}) \n PSNR: {:1.2f} \n SNR: {:1.2f} \n MSE: {:.2e} \n SSIM: {:1.2f}'.format
                                   (self.fwOpList[n].label, metrics.valPsnr[k][n], metrics.valSnr[k][n], 
                                   metrics.valMse[k][n], metrics.valSsim[k][n]) )
             else:
                pltImg.txtN[k].append('Observed (H {})'.format(self.fwOpList[n].label) )
               
             pltImg.imgShow[k].append(B[k][n])
             
        
          
      return B, metrics, pltImg  
        
        
        
        
        