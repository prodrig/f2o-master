
import scipy as sc
import matplotlib.pylab as PLT
import numpy as np


from F2O.F2O_utils import f2oJaxUtils, argsF2O, stepSize, stopCondF2O, costGradF2O, proxOp, ISeq, opDxDy
from F2O.F2O_sptl import commomPGD

from timeit import default_timer as timer


__author__ = """Paul Rodriguez <prodrig@pucp.edu.pe>"""



def pg(Op, b, lmbda, nIter=100, args=None):  
  r"""Proximal gradient (includes ISTA)
  
  Solve the l1 regularized problem problem:

    F(x) = f( Ax(x) - b ) + \lambda || x ||_1
    
  where f is the fidelity functional, Ax is the forward operator and
  b is the input (observed) data, via the iteration
  
    x_{k+1} = prox_{l1}( x_k - \alpha \cdot \nabla f(x_k), lmbda )
    
  where \nabla f can be computed via the f (costGradF2O class in F2O_utils.py)
  and Ax (fwOp class in fwOperator.py) classes
    
  """

  # -----------------------
  
  if args is None:
    args = argsF2O()

    
  args.f2oJax = f2oJaxUtils()

  pg = commomPGD(True, False)          # True: accelerated PG. False --> just PG
    
  # input data is transfered to 'device' (if needed)
  bDev = args.f2oJax.getDataIn(b)  
    
    
  # Initialization
  args.lmbda = lmbda
  x, Ax, f, stopFunc, pgStats, itemStats = pg.Init(Op, bDev, nIter, args)
    
  
  # Get subroutines (e.g. step-size computation, stoping criteria, etc.)
  computeCost, computeGrad, computeSS, prox = pg.SetFunctions(args, f, Ax, nIter)
  
  
  # Elapse time per iteration (after initializations)
  start = timer()
  
  for k in range(nIter):
    
    
    grad = computeGrad(x, b, None)      # compute gradien

    alpha = computeSS(k, grad, x)       # compute step-size
    
    
    # --- statistics, record step-Size --
    pg.RecordStats(k, x, b, grad, pgStats, None, computeCost)
    if 'ssX' in itemStats.keys():
       gdStats[k,itemStats['ssX']] = np.mean(alpha)
    #pgStats[k,1] = alpha
    # --- 
                    
    
    x = prox(x - alpha*grad, alpha*lmbda)     # proximal gradient descent step
        
    
    if 'time' in itemStats.keys():
       gdStats[k,itemStats['time']] = timer()-start
    #pgStats[k,2] = timer()-start        # Measure time
    if stopFunc(k, x, pgStats):         # check for exit condition / print stats
      print('Stop criteria -- iteration {0}'.format(k))
      break
    
  
  xOut = args.f2oJax.getDataOut(x)
  
  
  if args.xShape is not None:
     xOut.shape = args.xShape
  
  
  return(xOut, pgStats)
  


def apg(Op, b, lmbda, nIter=100, args=None):  
  r"""Accelerated Proximal gradient (includes ISTA)
  
  Solve the l1 regularized problem problem:

    F(x) = f( Ax(x) - b ) + \lambda || x ||_1
    
  where f is the fidelity functional, Ax is the forward operator and
  b is the input (observed) data, via the iteration
  
    x_{k+1} = prox_{l1}( x_k - \alpha \cdot \nabla f(x_k), lmbda )
    
  where \nabla f can be computed via the f (costGradF2O class in F2O_utils.py)
  and Ax (fwOp class in fwOperator.py) classes
    
  """
  
  
  # -----------------------
  
  if args is None:
    args = argsF2O()

  args.lmbda  = lmbda
  args.f2oJax = f2oJaxUtils()
    
  apg = commomPGD(True, True)          # True: accelerated PG. False --> just PG
    
  # input data is transfered to 'device' (if needed)
  bDev = args.f2oJax.getDataIn(b)  
    
  # Initialization
  x1, Ax, f, stopFunc, apgStats, itemStats = apg.Init(Op, bDev, nIter, args)
   
  y    = x1.copy()
  
  t0 = 1.
  t1 = 1.
  
  # Get subroutine (e.g. step-size computation, stoping criteria, etc)
  computeCost, computeGrad, computeSS, prox, iseq = apg.SetFunctions(args, f, Ax, nIter)

  # if APG is solved using the dualVar, then this will be diferent from identity
  computePrimalVar = apg.SetDualRec(args, Op, Ax)
  
  # if a callback is passed, it takes the primal as input
  if args.callback is not None:
     callbackGetPrimal = lambda x : computePrimalVar(x, [args, bDev, lmbda])
  else:
     callbackGetPrimal = None 
     
     
  # Elapse time per iteration (after initializations)
  apg.iterTimer(args.Timer, True, False)
  
  
  for k in range(nIter):
    
    
    grad = computeGrad(y, bDev, None)         # compute gradient
    
    alpha, grad = computeSS(k, grad, y)       # compute step-size
    
    x0 = x1.copy()



    x1 = prox(y - alpha*grad, alpha*lmbda)    # proximal gradient descent step
    
    
    t1 = iseq(k, t0)                    # compute inertial sequence
    gamma = (t0-1.)/t1


    
    y = x1 + gamma*(x1-x0)             # extrapolation step

    #y = x1 + gamma*(x1-x0) + (t0/t1)*(x1-y)            # extrapolation step --- additional term
                                                       # fessler-2016-optimized
                                                       # Optimized first-order methods for smooth convex minimization
    t0 = t1
    
    # --- statistics -- record step-Size ---
    apg.RecordStats(k, x1, b, grad, apgStats, itemStats, None, computeCost)
    if 'ssX' in itemStats.keys():
       apgStats[k,itemStats['ssX']] = np.mean(alpha)
       
    if 'time' in itemStats.keys():
       apgStats[k,itemStats['time']] = apg.iterTimer(args.Timer, False, True)
    #apgStats[k,1] = alpha
    #apgStats[k,2] = apg.iterTimer(args.Timer, False, True)
    # --- 
    
    
    if stopFunc(k, x1, apgStats, callbackGetPrimal):           # check for exit condition / print stats
      print('Stop criteria -- iteration {0}'.format(k))
      break
    
  
  # If dual is used (eg. TV fista), then recover the primal var. 
  # Also set the correct shape for output
  xOut = args.f2oJax.getDataOut(computePrimalVar(x1,[args, b, lmbda]))
  
  #if args.xShape is not None:
     #xOut.shape = args.xShape
  
  
  return(xOut, apgStats)
  
  
# ======================================================


  
