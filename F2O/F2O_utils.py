

import scipy as sc
import matplotlib.pylab as PLT
import numpy as np


try:
    #import noClass
    import jax
    
except ImportError:
    HAVE_JAX = False
    jnp = np
    
else:
    HAVE_JAX = True  

    from jax import jit
    
    from jax import numpy as jnp
    from jax import random as jrand
    from jax import scipy as jsp
    from jax import image as jimg
    
    from functools import partial

    #jax.config.update('jax_platform_name', 'gpu')

from scipy import signal

import inspect

import F2O.constants as f2oDef

import multiprocessing
import pyfftw

pyfftw.config.NUM_THREADS = multiprocessing.cpu_count()
pyfftw.config.PLANNER_EFFORT = 'FFTW_MEASURE'


# ---------------------------------------
from timeit import default_timer as timer
from datetime import timedelta
# ---------------------------------------


__author__ = """Paul Rodriguez <prodrig@pucp.edu.pe>"""



class f2oJaxUtils(object):
    r"""Simple routine to wrap JAX
    
    """
    
    f2oJaxFlagSet = None
    
    def __init__(self, flagForceJAX=None):
      
      #print('set', f2oJaxUtils.f2oJaxFlagSet)
      
      if f2oJaxUtils.f2oJaxFlagSet is None:

         if flagForceJAX is None:
            f2oJaxUtils.f2oJaxFlagSet = HAVE_JAX
         else:
           
            if flagForceJAX is True:
           
              if HAVE_JAX:
                 f2oJaxUtils.f2oJaxFlagSet = flagForceJAX
              else:
                 f2oJaxUtils.f2oJaxFlagSet = False
            
            else:
                 f2oJaxUtils.f2oJaxFlagSet = False

               
      self.have_jax = f2oJaxUtils.f2oJaxFlagSet
      
      if self.have_jax:
        self.jnp         = jnp
        self.jnp.random  = jrand
        self.jsp         = jsp
        self.jimg        = jimg
      else:
        self.jnp         = np
        self.jsp         = sc
        self.jimg        = None
        
        
        
    #isinstance(xdev, jnp.ndarray)
  
  
    def getModule(self, strModules):
    
      L=len(strModules)
        
      if self.have_jax:
        modName = getattr(jnp, strModules[0])
        for k in range(1,L):
            modName = getattr(modName, strModules[k])
            
        return jax.jit(modName)             
        
      else:
        modName = getattr(np, strModules[0])
        for k in range(1,L):
            modName = getattr(modName, strModules[k])
        
        return modName
    
    
    def getDataIn(self, npData):


      if self.have_jax:
        if isinstance(npData, jnp.ndarray):
          return(npData)
        else:
          return(jnp.array( npData ))
          #return(jax.device_put( npData ))
      else:
        return(npData)
    
  
    def getDataOut(self, jnpData):


      if self.have_jax:
        if isinstance(jnpData, jnp.ndarray):
           return(np.asarray(jnpData))
        else:
           return(jnpData)
      else:
        return(jnpData)

      
    def setShape(self, Data, Shape):

      Data = self.jnp.reshape(Data, Shape)

      return(Data)


    def rfft2(self, a, s=None, axes=(-2, -1), norm=None):
      if self.have_jax:
        return( jnp.fft.rfft2(a, s, axes, norm) )
        #return( self.rfft2JIT(a, s, axes, norm) )
      else:
        return( pyfftw.interfaces.numpy_fft.rfft2(a, s, axes, norm) )


    def irfft2(self, a, s=None, axes=(-2, -1), norm=None):
      if self.have_jax:
        return( jnp.fft.irfft2(a, s, axes, norm) )
        #return( self.irfft2JIT(a, s, axes, norm) )
      else:
        return( pyfftw.interfaces.numpy_fft.irfft2(a, s, axes, norm) )

    #@jit
    #def rfft2JIT(self, a, s=None, axes=(0,1), norm=None):
       #return(  jnp.fft.rfft2(a, s, axes, norm) )
  
    #@jit
    #def irfft2JIT(self, a, s=None, axes=(0,1), norm=None):
        #return( jnp.fft.irfft2(a, s, axes, norm) )
      
      
    def copy(self, Data):
      if self.have_jax:
        if isinstance(Data, jnp.ndarray):
           return( jnp.array(Data) )
        else:
           return( Data.copy() )
      else:
        return( Data.copy() )


    def convolve2d(self, in1, in2, mode='full', boundary='fill', fillvalue=0):
      if self.have_jax:
        return( jax.scipy.signal.convolve2d(in1, in2, mode=mode, boundary=boundary, fillvalue=fillvalue) )
      else:
        return( signal.convolve2d(in1, in2, mode=mode, boundary=boundary, fillvalue=fillvalue) )


    def getJIT(self, npFunc):

      if self.have_jax:
         return( jit( npFunc ) )
      else:
         return( npFunc )
       

class dataShpMixin:
    r"""Geometry (data, solution, observed, etc.)
    
    """
    
    def set_dataShp(self, bShape=None, padFlag=False):

        self.xShape   = None           # Original shape of the solution
        self.x_sptl   = None           # when problems are solve in Fourier,
                                       # this variable keeps track of the spatial /
                                       # temporal version

        self.freqSol  = False
        
        self.bShape   = bShape         # input data shape
        self.bfShape  = None           # input data shape in freq.
        self.Bf       = None           # variable in the freq. domain
        
        self.maxShape = None           # m
        
        self.padShape  = None          # to be used if sol. is comput in freq
        self.padFlag   = padFlag
        self.padMode   = 'constant'    # see np.pad
        self.padOffset = None
        #self.convFreq = f2oDef.convLin   # if convolution is computed in frequency, force the result
                                        ## to be the same as for
                                        ## * linear convolution with symmetric boundary condition
                                        ## * circular convolution
                                        ## * etc.
                                        
        self.dwFactor = None            # downsampling factor (mainly used for Super-Res)


    def cpy_essShp(self, args):
      
        self.xShape   = args.xShape
        self.bShape   = args.bShape
        self.bfShape  = args.bfShape
        self.freqSol  = args.freqSol
        self.dwFactor = args.dwFactor
        
        
    def __LINE__():
        stack_t = inspect.stack()
        ttt = inspect.getframeinfo(stack_t[1][0])
        return ttt.lineno


    def __FUNC__(self):
        stack_t = inspect.stack()
        ttt = inspect.getframeinfo(stack_t[1][0])
        return ttt.function

    def __FILE__(self):
        stack_t = inspect.stack()
        ttt = inspect.getframeinfo(stack_t[1][0])
        return ttt.filename
  
    def cpy_dbgMacro(self):
      
        return self.__LINE__, self.__FUNC__, self.__FILE__



class fwOpShpMixin:
    r"""Geometry (fw operator)
    
    """

    def set_fwOpShp(self, hShape=None, lShape=None, HfShape=None):

        self.hShape   = hShape          # Shape for forward operator
        self.lShape   = lShape          # if any, shape for the operator in the regularization term

        self.HfShape = HfShape

        # variables in the freq. domain
        self.Hf      = None
        self.Lf      = None
        
        self.rndF_flag = False          # generate a randomized fwOp in freq
        self.rndS_flag = False          # generate a randomized fwOp in the spatial domain



class IseqMixin:
    r"""parameters for the inertial sequence
    
    """

    def set_Iseq(self):

        # Inertial sequence
        self.ISeqPolicy  = None
        self.iseq_extra1 = 2.
        self.iseq_extra2 = 80.


    def cpy_Iseq(self, args):

        # Inertial sequence
        self.ISeqPolicy  = args.ISeqPolicy 
        self.iseq_extra1 = args.iseq_extra1
        self.iseq_extra2 = args.iseq_extra2

        if self.ISeqPolicy is None:
          self.ISeqPolicy = f2oDef.iseq.ntrv


class stepSizeMixin:
    r"""parameters for the inertial sequence
    
    """

    def set_SS(self):

        # step-size
        self.ssPolicy  = f2oDef.ss.BBv1
        self.ssCte     = 0.0
        self.AAlength  = 4
        self.mulCte    = -1.0
        self.roGrad    = None
        self.lmbda     = None

    #def set_AASS(self):

        ## step-size
        #self.ssPolicy = f2oDef.ss_AA
        #self.ssCte     = 0.0
        #self.nGrad     = 4              # number of copies to be stored

    def cpy_SS(self, args):

        self.ssPolicy  = args.ssPolicy
        self.ssCte     = args.ssCte
        self.AAlength  = args.AAlength
        self.mulCte    = args.mulCte
        self.lmbda     = args.lmbda

class roGradMixin:
    r"""parameters for robustifying the Gradient
    
    """
    
    def set_roGrad(self):

        self.roGradPolicy  = f2oDef.roGrad.Null
        self.roGrad_ssMul  = 1.0
        self.relaxCte      = 1.0
        self.lIgrad        = 0.
        self.ssCteMin      = 0.65
        self.ssCteIni      = None
        self.lIfactor      = 1.1

    def cpy_roGrad(self, args):

        try:
          self.roGradPolicy = args.roGradPolicy
        except AttributeError:
          self.roGradPolicy = f2oDef.roGrad.Null
          
        self.lIgrad        = 0.

        try:
          self.ssCteIni      = args.mulCte
        except AttributeError:
          self.ssCteIni      = -1.0

        try:
          self.lIfactor      = args.lIfactor
        except AttributeError:
          self.lIfactor      = 1.1
        
        try:
          self.roGrad_ssMul  = args.roGrad_ssMul
        except AttributeError:
          self.roGrad_ssMul  = 0.95
          
        try:
          self.relaxCte      = args.relaxCte
        except AttributeError:
          self.relaxCte      = 0.95
          
        try:
          self.ssCteMin      = args.ssCteMin
        except AttributeError:
          self.ssCteMin      = 0.65
          



class proxOpMixin:
    r"""parameters for the prox / proj operator
    
    """

    def set_proxOp(self):
        # 
        self.proxOp     = None


    def cpy_proxOp(self, args): # Based on the cost functional, the prox/proj
                                # flag is set

        switcher = {
          f2oDef.cost.L2_lin:    f2oDef.prox_None,
          f2oDef.cost.L2L2_lin:  f2oDef.prox_None,
          f2oDef.cost.L2L1_lin:  f2oDef.prox_l1,
          f2oDef.cost.L2TV_lin:  f2oDef.prox_TV
          }
      

        if args.fCostClass is not None:

          self.proxOp = switcher.get(args.fCostClass, f2oDef.prox_None)
          
        else:
          self.proxOp     = None



class fCostMixin:
    r"""parameters related to the cost funtional
    
    """
    def set_fCost(self, fCostClass=None, verbose=True, cmptStats=True):
        # 
        self.fCostClass = fCostClass
        self.lmbda      = 0.
        self.verbose    = verbose
        self.verboseStep = 10
        self.callback    = None                 # lambda function that will be callback at verboseStep
        self.cmptStats  = cmptStats
        if self.cmptStats is False:     #sanity check
           self.verbose = False

    def cpy_fCost(self, args):
        #
        self.fCostClass = args.fCostClass
        self.lmbda      = args.lmbda
        self.verbose    = args.verbose
        self.verboseStep = args.verboseStep
        self.callback   = args.callback
        self.cmptStats  = args.cmptStats
        if self.cmptStats is False:     #sanity check
           self.verbose = False
    

class exitPolMixin:
    r"""parameters related to the cost funtional
    
    """
    def set_exitPol(self):
        # 
        self.stop_Policy    = f2oDef.stop_l2xk
        self.stop_tol       = f2oDef.f2o_stop_l2Tol
        self.cmptStats      = True
        #self.verbose        = True

    def cpy_exitPol(self, args):
        #
        self.stop_Policy    = args.stop_Policy
        self.stop_tol       = args.stop_tol
        self.verbose        = args.verbose
        self.verboseStep    = args.verboseStep
        self.callback       = args.callback
        self.cmptStats      = args.cmptStats
    
    
    
# ================================================
# ================================================


class argsF2O(dataShpMixin, fwOpShpMixin, IseqMixin, stepSizeMixin, proxOpMixin, fCostMixin, exitPolMixin):
    r"""Parameters for F2O (first order optimization). This includes GD (gradient descent)
        Accelerated GD (a.k.a. Nesterov), ISTA (iterative soft-thresholding algorithm),
        FISTA (fast ISTA), etc.

    Attributes:
        ssCte: Float. Valid only if ssPolicy == ss_Cte
        ssPolicy: step-size policy. See ss_xxx in constants.py 
    """
    def __init__(self, fCostClass=None, hShape=None, HfShape=None, bShape=None, lShape=None, padFlag=False, enableJAX=None):
      
        self.set_dataShp(bShape,padFlag)
        self.set_fwOpShp(hShape, lShape, HfShape)
        self.set_Iseq()
        self.set_SS()
        self.set_fCost(fCostClass=fCostClass)
        self.set_proxOp()
        self.set_exitPol()

        # Cte.
        self.f2oDef = f2oDef

        # Timer
        self.Timer   = True             # If True, iteration time will be elapsed
      

        
        # Statistics: cost functional, grad, etc.
        self.fCost     = 1              # compute and record the cost functional
        self.fFid      = 0              # record the fidelity part of the cost functional
        self.fReg      = 0              # record the regularization part of the cost functional
        
        self.alpha     = 1              # record step-size
        self.gradL2    = 0              # compute the \ell_2 norm of the gradient
        self.gradLi    = 0              # compute the \ell_\infty norm of the gradient
        
        self.posAlpha  = None
        self.posGL2    = None
        self.posGLi    = None
        
        
        # geometry (observe, solution, etc.)
        
        self.clipVmax = 1.
        self.clipVmin = 0.

        
        self.f2oJax    = f2oJaxUtils(flagForceJAX=enableJAX)
        
        

    def cleanShpVars(self,hShape=None, HfShape=None, bShape=None, lShape=None):
      
        self.bShape  = bShape
        self.hShape  = hShape
        self.lShape  = lShape
        self.HfShape = HfShape
    
        return
    
    def get_padShape(self, u, mode='symmetric'):
      """Set data shape when the Convolution theorem is used
      """
      if self.padFlag:
        
        if self.hShape is None:
          return u.shape
          
        else:
          self.maxShape = np.asarray(self.hShape)
          L = np.floor( ( self.maxShape +0.5)/2 )
          L = L.astype(int)
          nUpad = (u.shape[0]+L[0]+self.maxShape[0]-1, u.shape[1]+L[1]+self.maxShape[1]-1)
          
          if len(self.dataShape) == 3:
             nUpad += (0,)

          if len(self.dataShape) == 4:
             nUpad += (0,0,)
          
      else: 
        nUpad = u.shape
        self.maxShape = np.ones((2,), dtype=np.int32)
    
      return nUpad

    
    def sel_padShp(self, mode):
      switcher = {
          'symmetric' : self.set_shpSymm,
          'fill'      : self.set_shpFill,
          'wrap'      : self.set_shpWrap
          }
      
      padShp = switcher.get(mode, "nothing")
      
      
      padFun = lambda u,Lr,Lc: padShp(u, Lr, Lc)
    
      return padFun
    
    
    def set_shpSymm(self, u, Lr, Lc):      
      
      Lr0 = np.int( (Lr - np.remainder(Lr,2))/2 )
      Lr1 = Lr - Lr0 - np.remainder(Lr,2)

      Lc0 = np.int( (Lc - np.remainder(Lc,2))/2 )
      Lc1 = Lc - Lc0 - np.remainder(Lc,2)
      
      # The padding offset i.e. (Lb+La,0),(Lb+La,0) has been chosen to match 
      # the (spatial) output g*(h*u - b ), where g = np.flip(np.flip(h,1),0)
      if len(u.shape) == 2:
         uPad = self.f2oJax.jnp.pad(u, ((Lr0+Lr1,0),(Lc0+Lc1,0)), mode='symmetric' )
      if len(u.shape) == 3:
         uPad = self.f2oJax.jnp.pad(u, ((Lr0+Lr1,0),(Lc0+Lc1,0),(0,0)), mode='symmetric' )
      if len(u.shape) == 4:
         uPad = self.f2oJax.jnp.pad(u, ((Lr0+Lr1,0),(Lc0+Lc1,0),(0,0),(0,0)), mode='symmetric' )


      padShp = (uPad.shape[0]+Lr-np.remainder(Lr,2),uPad.shape[1]+Lc-np.remainder(Lc,2))
      
      #self.padOffset = L-1
      if self.lphpFlag:                 # lphp: low-pass / high-pass flag (related to fiterbank fAx_FB2D)
        self.padOffset = (Lr, Lc)
      else:
        self.padOffset = (Lr1, Lc1)
    
      return uPad, padShp
    
    
    def set_shpFill(self, u, Lr, Lc):      #FIXME: consider a non-square kernel
      
      Lr0 = np.int( (Lr - np.remainder(Lr,2))/2 )
      Lr1 = Lr - Lr0 - 1

      Lc0 = np.int( (Lc - np.remainder(Lc,2))/2 )
      Lc1 = Lc - Lc0 - 1
      
      self.padOffset = (Lr1, Lc1)
    
      padShp = (u.shape[0]+Lr-1,u.shape[1]+Lc-1)
      
      return u, padShp
    
    
    def set_shpWrap(self, u, Lr, Lc):      #FIXME: consider a non-square kernel
      
      Lr0 = np.int( (Lr - np.remainder(Lr,2))/2 )
      Lr1 = Lr - Lr0 - np.remainder(Lr,2)

      Lc0 = np.int( (Lc - np.remainder(Lc,2))/2 )
      Lc1 = Lc - Lc0 - np.remainder(Lc,2)

      padShp = u.shape
    
      if self.lphpFlag:                 # lphp: low-pass / high-pass flag (related to fiterbank fAx_FB2D)
        self.padOffset = (-Lr0 + 1 - np.remainder(Lr,2), -Lc0 + 1 - np.remainder(Lc,2) )
      else:
        self.padOffset = (0,0)
        
      return u, padShp
    
    

    
    
    def compute_padFFT2(self, u, mode=None, padFlag=None, forceShp=None):   # add variable --> next power 2 / composite number
      """Computes FFT after input data has been padded
      """        
      
      
      
      if padFlag is None:
         if self.padFlag is None:
            padFlag = True
         else:
            padFlag = self.padFlag
            
            
      if mode is None:
         if self.padMode is None:
            mode = 'symmetric'
         else:
            mode = self.padMode
            
            
      if padFlag:
        
        
        self.dataShape = u.shape

        if forceShp is None:

           if self.hShape is None and self.lShape is not None:
              self.hShape = 0*np.asarray(self.lShape)

           if self.lShape is None and self.hShape is not None:
              self.lShape = 0*np.asarray(self.hShape)

           self.maxShape = np.maximum(np.asarray(self.hShape),np.asarray(self.lShape) )
           
        else:
           self.maxShape = forceShp

        padFun = self.sel_padShp(mode)


        uPad, padShp = padFun(u, self.maxShape[0], self.maxShape[1])      # FIXME: Generalized for non-square case
  
  
        UF = self.f2oJax.rfft2( self.f2oJax.getDataIn(uPad), s=padShp[:2], axes=(0,1))
        

      else:
    
        
        padShp = u.shape 

        UF = self.f2oJax.rfft2(u, s=padShp[:2], axes=(0,1))
                
        self.maxShape = self.f2oJax.jnp.ones((2,), dtype=np.int32)   # See HF = pyfftw.interfaces in compute_KernelFFT as to why
        
        
        
      return UF, padShp


    def compute_DxDyFFT(self, dwFactor = None, cardFB=None):

      if dwFactor is None:
         factor = 1
      else:
         factor = dwFactor

      dx = np.zeros((2,2))
      dx[0,0] =  1.
      dx[0,1] = -1.

      dy = np.zeros((2,2))
      dy[0,0] =  1.
      dy[1,0] = -1.
    
      dx = dx[...,np.newaxis]
      dy = dy[...,np.newaxis]

      
      DF = self.f2oJax.rfft2(self.f2oJax.getDataIn( np.concatenate( (dx,dy), axis=-1) ), s=tuple(factor*np.array(self.padShape[:2])), axes=(0,1))
      
      return(DF)
    
    
    def compute_KernelFFT(self, H, dwFactor = None, cardFB=None):

      #nH = H.shape      # kernel / filter assumed to be 2D
      
      if dwFactor is None:
         factor = 1
      else:
         factor = dwFactor
      
      
      
      if H is not None:


         HF = self.f2oJax.rfft2(self.f2oJax.getDataIn(H), s=tuple(factor*np.array(self.padShape[:2])), axes=(0,1))
            

         # FIXME: add the SR case
         #HF = self.f2oJax.rfft2(self.f2oJax.getDataIn(H), s=self.padShape[0]*factor+self.maxShape[0]-1,
                                #self.padShape[1]*factor+self.maxShape[1]-1), axes=(0,1))

      
      else:
          HF = None     # this would be interpreted as identity
          
          ## FIXME: for a freq- ranzomized filter, use other option
          # generate a randomized FB
          
          #HF = self.fftwHRand(self.padShape[0]*factor+self.maxShape[0]-1, 
                              #self.padShape[1]*factor+self.maxShape[1]-1, cardFB, 4., 0.65)
      
          #HF = self.fftwGauss(self.padShape[0]*factor, 
                              #self.padShape[1]*factor, cardFB, 4.)

          #HF = self.fftwGauss(self.padShape[0]*factor+self.maxShape[0]-1, 
                              #self.padShape[1]*factor+self.maxShape[1]-1, cardFB, 4.)
      
      
      return HF


    def fftwHRand(self, nF1, nF2, L, varWidth=16., density=0.2, flagShow=True):  


        n2F1 = np.int(np.floor(nF1/2.))
        n2F2 = np.int(np.floor(nF2/2.))


        pos = np.array([nF1,n2F2])*np.random.rand(L,2)
        
        var = np.array([[nF1,0],[0,nF2]],dtype=float)/varWidth
    
        nVal = int( np.ceil(density*nF1*nF2/varWidth))  #  
    
        
        M = np.zeros((nF1,n2F2+1,L),dtype=np.complex64)
        H = np.zeros((nF1,nF2),dtype=np.complex64)
    
    
        for k in range(L):
        
          
          z = np.ceil(np.random.multivariate_normal(pos[k,:], var, nVal))
          z = z.astype(int)
          z[:,0] = np.remainder(z[:,0],nF1)
          z[:,1] = np.remainder(z[:,1],nF2)

          H *= 0.
          H[z[:,0],z[:,1]] = np.random.randn(nVal) + 1j*np.random.randn(nVal)

          if pos[k,0] <= n2F1 and pos[k,1] <= n2F2:
             M[1:n2F1,1:n2F2,k] = H[1:n2F1,1:n2F2]

          if pos[k,0] > n2F1 and pos[k,1] <= n2F2:
             M[n2F1+1:nF1,1:n2F2,k] = H[n2F1+1:nF1,1:n2F2]
            
          if pos[k,0] <= n2F1 and pos[k,1] > n2F2:
             M[1:n2F1,n2F2-1:0:-1,k] = np.conj(H[1:n2F1,n2F1+1:nF1])
    
          if pos[k,0] > n2F1 and pos[k,1] > n2F2:
             M[n2F1-1:0:-1,n2F2-1:0:-1,k] = np.conj(H[1:n2F1,1:n2F2])
        
          # normalize filter
          alpha     = np.linalg.norm(M[:,:,k]) / np.sqrt( np.linalg.norm(M[:,:,k])**2 + np.linalg.norm(M[:,1:n2F2,k])**2 )
          M[:,:,k] *= (alpha*np.sqrt(np.float(nF1*nF2)))/np.linalg.norm(M[:,:,k])
        
        return M


    def fftwGauss(self, nF1, nF2, L, varWidth=16., flagShow=True):  

      pos = np.array([nF1,nF2/2])*np.random.rand(L,2)
        
      var = np.array([[nF1,0],[0,nF2]],dtype=float)/varWidth
    
    
      n2F1 = np.int(np.floor(nF1/2.))
      n2F2 = np.int(np.floor(nF2/2.))
    
    
      M = np.zeros((nF1,n2F2+1,L),dtype=np.complex64)
      H = np.zeros((nF1,nF2),dtype=np.complex64)
                
      posLim = np.int(np.ceil(var[0,0]/2.))
      sigma  = var[0,0]/3
      x,y = np.ogrid[-posLim:posLim+1,-posLim:posLim+1]
      xx,yy = np.meshgrid(x,y, indexing='xy') 
    
      h = np.exp( -(x*x + y*y) / (2.*sigma*sigma) )
      hpos = np.array([xx.ravel(),yy.ravel()]).transpose()
    
      h[ h < np.finfo(h.dtype).eps*h.max() ] = 0
    
      for k in range(L):
        
        z = pos[k,:] + hpos
        z = z.astype(int)
        z[:,0] = np.remainder(z[:,0],nF1)
        z[:,1] = np.remainder(z[:,1],nF2)
    
        H *= 0.
        H[z[:,0],z[:,1]] = h.ravel().copy()

        if pos[k,0] <= n2F1 and pos[k,1] <= n2F2:
             M[1:n2F1,1:n2F2,k] = H[1:n2F1,1:n2F2]

        if pos[k,0] > n2F1 and pos[k,1] <= n2F2:
             M[n2F1+1:nF1,1:n2F2,k] = H[n2F1+1:nF1,1:n2F2]
            
        if pos[k,0] <= n2F1 and pos[k,1] > n2F2:
             M[1:n2F1,n2F2-1:0:-1,k] = np.conj(H[1:n2F1,n2F1+1:nF1])
    
        if pos[k,0] > n2F1 and pos[k,1] > n2F2:
             M[n2F1-1:0:-1,n2F2-1:0:-1,k] = np.conj(H[1:n2F1,1:n2F2])
        
        # normalize filter
        alpha     = np.linalg.norm(M[:,:,k]) / np.sqrt( np.linalg.norm(M[:,:,k])**2 + np.linalg.norm(M[:,1:n2F2,k])**2 )
        M[:,:,k] *= (alpha*np.sqrt(np.float(nF1*nF2)))/np.linalg.norm(M[:,:,k])

      return M
    

    def getROI(self, Upad, L=None, mode=None ):

      if self.padFlag is not True:
         return(Upad)
        
      if L is None:
         if self.padOffset is None:
            L = (0,0)
         else:
            L = self.padOffset

            
      if mode is None:
         if self.padMode is None:
            mode = 'symmetric'
         else:
            mode = self.padMode


      if mode == 'wrap':
        z = self.f2oJax.jnp.roll(Upad, (L[0],L[1]), axis=(0,1) )
      else:
      
        
        if (len(Upad.shape))==2:
          z =Upad[L[0]:self.bShape[0]+L[0], L[1]:self.bShape[1]+L[1]]
        else:
          z =Upad[L[0]:self.bShape[0]+L[0], L[1]:self.bShape[1]+L[1], : ]
        
        
        
      return z
        
#=========================================================================
#=========================================================================


class opDxDy(object):
    r"""Display images  
    """
    def __init__(self,enableJAX=None):
      self.f2oJax  = f2oJaxUtils(flagForceJAX=enableJAX)


    #@partial(jit, static_argnums=(0,))
    def Dy(self, x):
            
      return( self.f2oJax.jnp.diff(x,axis=0,append=x[self.f2oJax.jnp.newaxis,-1,:,...]) )
      

    #@partial(jit, static_argnums=(0,))
    def Dx(self, x):
      
      return( self.f2oJax.jnp.diff(x,axis=1,append=x[:,self.f2oJax.jnp.newaxis,-1,...]) )


    #@partial(jit, static_argnums=(0,))
    def DyT(self, x):
      
      return( -self.f2oJax.jnp.diff(x[:-1,:,...],axis=0,prepend=0,append=0) )


    #@partial(jit, static_argnums=(0,))
    def DxT(self, x):
      
      return( -self.f2oJax.jnp.diff(x[:,:-1,...],axis=1,prepend=0,append=0) )

    

#=========================================================================
#=========================================================================

class costGradF2O(dataShpMixin, fCostMixin):
    r"""Compuutes the cost function and gradient associated with an
        given forward operator.
    """
    def __init__(self, fwOp, args):


        self.cost      = 0.0    # cost at current iteration
        self.costFlag  = False
        self.fwOp      = fwOp   # forward operator

        self.cpy_essShp(args)
        self.cpy_fCost(args)
      
        self.Dx        = opDxDy().Dx
        self.Dy        = opDxDy().Dy
        self.DxT       = opDxDy().DxT
        self.DyT       = opDxDy().DyT
        
        try:
          self.f2oJax = args.f2oJax
        except:
          pass
        
        

    # Compute grad & cost for F(x) = 0.5|| f(x) - b ||_2^2 
    def fun_gL2_lin(self, x, b, aliasVar=None):           

      z     = self.fwOp( x, 0, None) - b.ravel()         # f(x) - b        
      grad  = self.fwOp(z, 1, None)                     # < f'(x), f(x) - b >

      if self.cmptStats: # fixme -- change this flag
        
         if self.freqSol is False:
            self.cost = 0.5*z.ravel().dot( z.ravel() )
         else:
            zAbs = self.f2oJax.jnp.real( self.f2oJax.jnp.conj(z)*z )
            zAbs = self.f2oJax.setShape( zAbs, self.bfShape )
            
            self.cost = 0.5*( 1./(self.bShape[0]*self.bShape[1]) )*( 2.*sum(zAbs.ravel()) - sum(zAbs[:,0,].ravel() ) - sum(zAbs[:,-1,].ravel() ) )
            
         self.costFlag = True
      
      
      return grad


    # Compute *only* cost for F(x) = 0.5|| f(x) - b ||_2^2
    def fun_costL2_lin(self, x, b, aliasVar=None):                

      if not self.costFlag:
         
        if self.freqSol is False:
           z     = self.fwOp(x, 0, None) - b.ravel()
           self.cost = 0.5*z.ravel().dot( z.ravel() )      # faster than norm2 and sum(z*z)
        else:
           z         = self.fwOp( x, 0, None) - b.ravel()
           zAbs = self.f2oJax.jnp.real( self.f2oJax.jnp.conj(z)*z )
           zAbs = self.f2oJax.setShape( zAbs, self.bfShape )
           self.cost = 0.5*( 1./(self.bShape[0]*self.bShape[1]) )*( 2.*sum(zAbs.ravel()) - sum(zAbs[:,0,].ravel() ) - sum(zAbs[:,-1,].ravel() ) )
            
        
      self.costFlag = False
      
      return self.cost

    # --------------------------------------------------------------------------

    # Compute grad & cost for F(x) = 0.5|| f(x) - b ||_2^2 + \lambda || x ||_2^2
    def fun_gL2L2_lin(self, x, b, aliasVar=None):               

      x_sptl = aliasVar
      z     = self.fwOp(x, 0, None) - b.ravel()               # f(x) - b        
      grad  = self.fwOp(z, 1, None) + self.lmbda*x.ravel()    # < f'(x), f(x) - b > + lambda \cdot x

      if self.cmptStats:
        
         if self.freqSol is False:
            self.cost = 0.5*z.ravel().dot( z.ravel() ) + self.lmbda*x.ravel().dot( x.ravel() )
         else:
            zAbs   = np.real( np.conj(z)*z )
            zAbs.shape = self.bfShape
            
            self.cost = 0.5*( 1./(self.bShape[0]*self.bShape[1]) )*( 2.*sum(zAbs.ravel()) - sum(zAbs[:,0,].ravel() ) - sum(zAbs[:,-1,].ravel() ) )
            self.cost += self.lmbda*self.x_sptl.ravel().dot( x_sptl.ravel() ) 
            
         self.costFlag = True
         
        
      return grad

    # Compute *only* cost for F(x) = 0.5|| f(x) - b ||_2^2 + 0.5*\lambda || x ||_2^2
    def fun_costL2L2_lin(self, x, b, aliasVar=None):                

      if not self.costFlag:
         
        if self.freqSol is False:
           z     = self.fwOp(x, 0, None) - b.ravel()
           self.cost = 0.5*z.ravel().dot( z.ravel() ) + 0.5*self.lmbda*0.5*x.ravel().dot( x.ravel() )
        else:
           self.cost = np.nan   # FIXME
        
      self.costFlag = False
      
      return self.cost
   
    # --------------------------------------------------------------------------

    # Compute grad & cost for F(x) = 0.5|| f(x) - b ||_2^2 + \lambda || x ||_1
    def fun_gL2L1_lin(self, x, b, aliasVar=None):         

      x_sptl = aliasVar
      z      = self.fwOp(x, 0, None) - b.ravel()         # f(x) - b        
      grad   = self.fwOp(z, 1, None)                     # gradient of the fidelity term only

             
      if self.cmptStats:

         if self.freqSol is False:
            self.cost = 0.5*z.ravel().dot( z.ravel() ) + self.lmbda*np.linalg.norm( x.ravel(), 1)
         else: 

            zAbs   = self.f2oJax.jnp.real( self.f2oJax.jnp.conj(z)*z )
            
            zAbs = self.f2oJax.setShape(zAbs, self.bfShape)
                        
            self.cost = 0.5*( 1./(self.bShape[0]*self.bShape[1]) )*( 2.*sum(zAbs.ravel()) - sum(zAbs[:,0,].ravel() ) - sum(zAbs[:,-1,].ravel() ) )
            
            if x_sptl is not None:
              self.cost += self.lmbda*np.linalg.norm( x_sptl.ravel(), 1)
            
         self.costFlag = True
         
        
      return grad
   
   
    # Compute *only* cost for F(x) = 0.5|| f(x) - b ||_2^2 + \lambda || x ||_1
    def fun_costL2L1_lin(self, x, b, aliasVar=None):                

      if not self.costFlag:
         
        if self.freqSol is False:
           z     = self.fwOp(x, 0, None) - b.ravel()
           self.cost = 0.5*z.ravel().dot( z.ravel() ) + self.lmbda*np.linalg.norm( x.ravel(), 1)
        else:
           self.cost = np.nan   # FIXME
        
      self.costFlag = False
      
      return self.cost

   
    # --------------------------------------------------------------------------

    # Compute grad & cost for F(x) = 0.5|| f(x) - b ||_2^2 + \lambda || \nabla x ||_1
    #@partial(jit, static_argnums=(0,))
    def fun_gL2TV_lin(self, x, b, aliasVar=None):  # FIXME: change x_sptl for a generic name! also SWITCHER       



             
      if self.cmptStats:

         if self.freqSol is False:
           
            lmbda  = aliasVar
            xlocal = lmbda*args.DxT(x[...,0]) + lmbda*args.DyT(x[...,1])
            #z      = self.fwOp(x, 0, None) - b.ravel()         # f(x) - b        
            z      = self.fwOp(x, 0, None)         # f(x) - b        
            grad   = self.fwOp(z, 1, None)         # gradient of the fidelity term only
           
           
            self.cost = 0.5*z.ravel().dot( z.ravel() ) 
            
            self.cost += lmbda*self.f2oJax.jnp.sqrt( x[...,0]*x[...,0] + x[...,1]*x[...,1])
            
           
         else: 


            Df = aliasVar
            z     = self.fwOp(x, 0, None) - b.ravel()         # f(x) - b        
            grad  = self.fwOp(z, 1, None)                     # gradient of the fidelity term only

            zAbs   = self.f2oJax.jnp.real( self.f2oJax.jnp.conj(z)*z )
            
            zAbs = self.f2oJax.setShape(zAbs, self.bfShape)
                        
            self.cost = 0.5*( 1./(self.bShape[0]*self.bShape[1]) )*( 2.*sum(zAbs.ravel()) - sum(zAbs[:,0,].ravel() ) - sum(zAbs[:,-1,].ravel() ) )
            
            
            if Df is not None: # FIXME: super-res TV case would break this
               Du = self.f2oJax.irfft2( self.f2oJax.jnp.einsum('ij...k,ij...->ij...k', Df, self.f2oJax.setShape(x, self.xShape ) ), s=self.padShape[:2], axes=(0,1))
               self.cost += self.lmbda*sum( self.getROI( self.f2oJax.jnp.sqrt(Du[:,:,:,0]*Du[:,:,:,0] + Du[:,:,:,1]*Du[:,:,:,1]) ).ravel() )

            
         self.costFlag = True
         
        
      return grad


    # Compute *only* cost for F(x) = 0.5|| f(x) - b ||_2^2 + \lambda || \nabla x ||_1
    #@partial(jit, static_argnums=(0,))
    def fun_costL2TV_lin(self, x, b, aliasVar=None):                

      
      if not self.costFlag:
         
        if self.freqSol is False:
          
           lmbda  = aliasVar
           xlocal = lmbda*self.DxT(x[...,0]) + lmbda*self.DyT(x[...,1])
          
           z     = self.fwOp(xlocal, 0, None)
           #z     = self.fwOp(xlocal, 0, None) - b.ravel()
           self.cost = 0.5*z.ravel().dot( z.ravel() )  # 
           
           self.cost += lmbda*sum( self.f2oJax.jnp.sqrt( x[...,0]*x[...,0] + x[...,1]*x[...,1]).ravel() )
           
        else:
          
           Df = aliasVar
          
           self.cost = self.f2oJax.jnp.nan   # FIXME
        
      self.costFlag = False
      
      return self.cost
        

    # --------------------------------------------------------------------------
    # --------------------------------------------------------------------------

    def fun_costDualL2TV_lin(self, x, b, aliasVar=None):                

      if not self.costFlag:
         
         if self.freqSol is False:
            pass 
         else:
            pass 
              
      self.costFlag = False
      
      return self.cost




    # Compute grad & cost for F(x) = 0.5|| u - b ||_2^2 + \lambda || \nabla u ||_1
    # when solved via its dual i.e.: F(x) = \max_p 0.5|| u - b ||_2^2 + \lambda <D^Tp,u>, D = [Dx' Dy']', p = p1' p2']'
    # then:
    #                                       || b - \lambda\cdot D^Tp ||_2^2  --> grad = \lambda\cdot D(\lambda\cdot D^Tp - b)
    def fun_gDualL2TV_lin(self, x, b, aliasVar=None):         

      #x_sptl = aliasVar
      z      = self.lmbda*self.fwOp(x, 0, None) - b.ravel()     # \lambda\cdot D^Tp - b <--> -u
      #grad   = self.lmbda*self.fwOp(z, 1, None)                 # \lambda\cdot D(\lambda\cdot D^Tp - b)
      grad   = self.fwOp(z, 1, None)                 # \lambda\cdot D(\lambda\cdot D^Tp - b)  see step-size for daul TV

      if self.cmptStats:

         if self.freqSol is False:
            
            fid = z #+ b.ravel()
            self.cost = 0.5*fid.ravel().dot( fid.ravel() )
            
            origShp = grad.shape
            grad = self.f2oJax.setShape(grad, self.xShape)
            self.cost += self.lmbda*sum( self.f2oJax.jnp.sqrt( grad[...,0]*grad[...,0] + grad[...,1]*grad[...,1]).ravel() )
            grad = self.f2oJax.setShape(grad, origShp)
                        
         else:
   
            self.cost = self.f2oJax.jnp.nan

         self.costFlag = True


      return grad


    # --------------------------------------------------------------------------

    def sel_cost(self, dual=False):
      
      if dual is False:
         switcher = {
          f2oDef.cost.L2_lin:    self.fun_costL2_lin,
          f2oDef.cost.L2L2_lin:  self.fun_costL2L2_lin,
          f2oDef.cost.L2L1_lin:  self.fun_costL2L1_lin,
          f2oDef.cost.L2TV_lin:  self.fun_costL2TV_lin
          }
      else:
         switcher = {
          f2oDef.cost.L2_lin:    self.fun_costL2_lin,
          f2oDef.cost.L2L2_lin:  self.fun_costL2L2_lin,
          f2oDef.cost.L2TV_lin:  self.fun_costDualL2TV_lin
          }
	
      cost = switcher.get(self.fCostClass, "nothing")
      
      costFun = lambda x, b, aliasVar=None: cost(x, b, aliasVar=aliasVar)
      
      # --- ---
      
      if dual is False:
         switcher = {
          f2oDef.cost.L2_lin:    self.fun_gL2_lin,
          f2oDef.cost.L2L2_lin:  self.fun_gL2L2_lin,
          f2oDef.cost.L2L1_lin:  self.fun_gL2L1_lin,
          f2oDef.cost.L2TV_lin:  self.fun_gL2TV_lin
          }
      else: 
         switcher = {
          f2oDef.cost.L2_lin:    self.fun_gL2_lin,
          f2oDef.cost.L2L2_lin:  self.fun_gL2L2_lin,
          f2oDef.cost.L2TV_lin:  self.fun_gDualL2TV_lin
          }
	
	
      grad = switcher.get(self.fCostClass, "nothing")
      
      gradFun = lambda x, b, aliasVar=None: grad(x, b, aliasVar=aliasVar)
      
      return costFun, gradFun






class stopCondF2O(exitPolMixin):
    r"""Select the exit condition.

    """
    def __init__(self, args, nIterMax, dlFlag = False):
      
        self.nIterMax  = nIterMax
        self.k         = None
        self.xprv      = None
        self.dlFlag    = dlFlag
        
        self.cpy_exitPol(args)
        
        
          
        if self.dlFlag is False:        # dl: dictionary learning
           if self.verbose:
              print('\n')
              print("==="*20)
              print('Iter \t Cost \t\t step-size     Time     |x_k - x_{k-1}|')
              
           if self.cmptStats:
              self.dict = {'cost':0, 'ssX':1, 'time':2, 'xDiff':3, 'last':4}

              
        else:
           if self.verbose:
              print('\n')
              print("==="*22)
              print('Iter \t Cost \t\t ss X \t     ss D \t  Time      |x_k - x_{k-1}|')

           if self.cmptStats:
              self.dict = {'cost':0, 'ssX':1, 'ssD':2, 'time':3, 'xDiff':4, 'last':5}


        if hasattr(self,'dict') and args.callback is not None:
                
           pos = list(self.dict.keys()).index('last') 
           items = list(self.dict.items()) 
           items.insert(pos, ('callback', self.dict.get('last'))) 
           items.remove( ('last', self.dict.get('last')) ) 
           items.insert(pos+1, ('last', self.dict.get('last')+1)) 
           self.dict = dict(items)

        #FIXME: if NO-RECORDS
        #   self.dict = {'last':0}




    def sel_SC(self):
      switcher = {
          f2oDef.stop_nIter:        self.stop_nIter,
          f2oDef.stop_l2xk:         self.stop_l2xk
          }
      
      stopFun = switcher.get(self.stop_Policy, "nothing")
      
      func = lambda k, x, optStats, callbackGetPrimal=None: stopFun(k, x, optStats, callbackGetPrimal)
      
      return func

    
    def print_stats(self, k, x, xprv, optStats, callbackGetPrimal=None):
      
      if xprv is not None:
        norm = np.linalg.norm( x.ravel() - xprv.ravel(), 2) / np.linalg.norm( x.ravel() )
      else:
        norm = np.float('Nan')
        
      if self.verbose:
        if (np.remainder(k,self.verboseStep)==0) or (k+1==self.nIterMax):   
           
           if self.callback is not None:  # callback is a 'lambda' function provided by the user
              optStats[k,self.dict['callback']] = self.callback( callbackGetPrimal(x) )
           
           if self.dlFlag is False:
              print( '{:>3d}\t {:.3e}\t {:.2e}    {:.2e}    {:>.3e}'.format(k, optStats[k,self.dict['cost']], 
                                                                          optStats[k,self.dict['ssX']], 
                                                                          optStats[k,self.dict['time']], 
                                                                          norm) )
           else:
             print( '{:>3d}\t {:.3e}\t {:.2e}    {:.2e}    {:.2e}    {:>.3e}'.format(k, optStats[k,self.dict['cost']], 
                                                                          optStats[k,self.dict['ssX']], 
                                                                          optStats[k,self.dict['ssD']], 
                                                                          optStats[k,self.dict['time']], 
                                                                          norm) )
     

      #if self.verbose:        
        #print( '{:>3d}\t {:.3e}\t {:.2e}\t {:.2e}\t {:>.3e}'.format(k, optStats[k,0], optStats[k,1], optStats[k,2], norm) )
              
      return norm
    

    def stop_nIter(self, k, x, optStats, callbackGetPrimal=None):
      
      stop = False
      
      if k == 0:
        self.xprv = x      
        self.print_stats(k, x, None, optStats, callbackGetPrimal)
      else:
        self.print_stats(k, x, self.xprv, optStats, callbackGetPrimal)
        
      
      if k >= self.nIterMax-1:
        stop = True
        
      return stop
    
    
    def stop_l2xk(self, k, x, optStats, callbackGetPrimal=None):
      
      stop = False
      
      if k == 0:
        self.xprv = x
        self.print_stats(k, x, None, optStats, callbackGetPrimal)
        
      else:
        
        norm = self.print_stats(k, x, self.xprv, optStats, callbackGetPrimal)
        
        if norm <= self.stop_tol:
          stop = True

        self.xprv = x
  
      return stop
    
#=========================================================================
#=========================================================================

class stepSize(dataShpMixin, stepSizeMixin):
    r"""Step-size policy and routines.

    """
    def __init__(self, args, fAx, HfShp=None):
      
        self.fAx         = fAx
        self.xprv        = None           # previous solution
        self.gprv        = None           # previous solution's gradient
        self.alphaPrv    = None
        self.HfShape     = HfShp
        self.gradDen     = None
        
        self.cpy_essShp(args)
        self.cpy_SS(args)
        
        try:
          self.f2oJax = args.f2oJax
        except:
          pass

        
      
    def sel_SS(self):
      switcher = {
          f2oDef.ss.Cte.val:          self.cte_SS,
          f2oDef.ss.Cauchy.val:       self.Cauchy_SS,
          f2oDef.ss.CauchyRand.val:   self.CauchyRand_SS,
          f2oDef.ss.CauchyLagged.val: self.CauchyLagged_SS,
          f2oDef.ss.CauchySupp.val:   self.CauchySupp_SS,
          f2oDef.ss.CauchyLSupp.val:  self.CauchyLSupp_SS,
          f2oDef.ss.BBv1.val:         self.BBv1_SS,
          f2oDef.ss.BBv2.val:         self.BBv2_SS,
          f2oDef.ss.BBv3.val:         self.BBv3_SS,
          f2oDef.ss.AA.val:           self.cteAA_SS,
          f2oDef.ss.dualTV.val:       self.dualTV_SS,
          f2oDef.ss.CteCauchy.val:    self.cteAuto_SS
          }

      ssFun = switcher.get(self.ssPolicy.val, "nothing")
      
      func = lambda k, grad, x, x_sptl=None: ssFun(k, grad, x, x_sptl=x_sptl)
      
      return func


    # ----------------------------------------
    # ----------------------------------------

    def dualTV_SS(self, k, grad, x, x_sptl=None):
  
      #alpha = 1./(8.*self.lmbda*self.lmbda) see also dual_costTV
      alpha = 1./(8.*self.lmbda)
  
      return alpha, grad

      
    def cte_SS(self, k, grad, x, x_sptl=None):
  
      alpha = self.ssCte
  
      return alpha, grad
  
  

    def Cauchy_SS(self, k, grad, x, x_sptl=None):
  
      den = self.fAx(grad, 0, None)
  
      if self.freqSol is False:
        alpha = sum(grad*grad)/sum(den*den)
      else:

         # Compensate for upsamplng / downsampling
         factor = self.dwFactor_SS(den, grad)

         if len(self.bfShape) <= 3:   # single input image (grayscale or color)

            alpha = factor*self.f2oJax.jnp.sum( self.f2oJax.jnp.real( self.f2oJax.jnp.conj(grad) * grad ) ) / self.f2oJax.jnp.sum( self.f2oJax.jnp.real( self.f2oJax.jnp.conj(den) * den ) )

         if len(self.bfShape) == 4:   # multiple input image
  
            den.shape  = self.bfShape
            grad.shape = (self.bfShape[0], self.bfShape[1], self.bfShape[2], self.xShape[3], self.bfShape[3])
              
            alpha = factor*self.f2oJax.jnp.sum( self.f2oJax.jnp.real( self.f2oJax.jnp.conj(grad) * grad ), axis=(0,1,2,3) ) / self.f2oJax.jnp.sum( self.f2oJax.jnp.real( self.f2oJax.jnp.conj(den) * den ), axis=(0,1,2) )
  
            alpha.shape = (self.bfShape[3],)


      if self.mulCte > 0:
         alpha *= self.mulCte

      return alpha, grad



    def CauchyRand_SS(self, k, grad, x, x_sptl=None):
  
      den = self.fAx(grad, 0, None)
  
      if self.freqSol is False:
         alpha = 2.0*np.random.rand(1)*sum(grad*grad)/sum(den*den)
      else:
  
         # Compensate for upsamplng / downsampling
         factor = self.dwFactor_SS(den, grad)

  
         if len(self.bfShape) <= 3:   # single input image (grayscale or color)
        
            alpha = factor*2.0*np.random.rand(1)*np.sum( np.real( np.conj(grad) * grad ) ) / np.sum( np.real( np.conj(den) * den ) )
  
  
         if len(self.bfShape) == 4:   # multiple input image
  
            den.shape  = self.bfShape
            grad.shape = (self.bfShape[0], self.bfShape[1], self.bfShape[2], self.xShape[3], self.bfShape[3])
              
            alpha = factor*np.sum( np.real( np.conj(grad) * grad ), axis=(0,1,2,3) ) / np.sum( np.real( np.conj(den) * den ), axis=(0,1,2) )
  
            alpha.shape = (self.bfShape[3],)
  
            alpha *= 2.0*np.random.rand(self.bfShape[3])
            
  
      return alpha, grad

    def gradDen3_SS(self, grad, den):
        return( self.f2oJax.jnp.sum( self.f2oJax.jnp.real( self.f2oJax.jnp.conj(grad) * grad ) ) / self.f2oJax.jnp.sum( self.f2oJax.jnp.real( self.f2oJax.jnp.conj(den) * den ) ) )

    def gradDen4_SS(self, grad, den):
        return( self.f2oJax.jnp.sum( self.f2oJax.jnp.real( self.f2oJax.jnp.conj(grad) * grad ), axis=(0,1,2,3) ) / self.f2oJax.jnp.sum( self.f2oJax.jnp.real( self.f2oJax.jnp.conj(den) * den ), axis=(0,1,2) ) )
    

    #@partial(jit, static_argnums=(0,))
    def CauchyLagged_SS(self, k, grad, x, x_sptl=None):
        
      if k%2 == 0:
        
        den = self.fAx(grad, 0, None)
                   
        if self.freqSol is False:           
           alpha = sum(grad*grad)/sum(den*den)
           self.alphaPrv = alpha.copy()
           
        else:
             
           # Compensate for upsamplng / downsampling
           factor = self.dwFactor_SS(den, grad)
           
            
           if len(self.bfShape) <= 3:   # single input image
             
              #if self.gradDen is None:
                 #self.gradDen = self.f2oJax.getJIT( self.gradDen3_SS )
                 
              #alpha = factor*self.f2oJax.jnp.sum( self.f2oJax.jnp.real( self.f2oJax.jnp.conj(grad) * grad ) ) / self.f2oJax.jnp.sum( self.f2oJax.jnp.real( self.f2oJax.jnp.conj(den) * den ) )
              alpha = factor*self.gradDen3_SS(grad, den)
              self.alphaPrv = alpha.copy()


           if len(self.bfShape) == 4:   # multiple input image
             
              den.shape  = self.bfShape
              #grad.shape = (self.bfShape[0], self.bfShape[1], self.bfShape[2], self.HfShape[2], self.bfShape[3])
              grad.shape = (self.bfShape[0], self.bfShape[1], self.bfShape[2], self.xShape[3], self.bfShape[3])
              
              #if self.gradDen is None:
                 #self.gradDen = self.gradDen4_SS
              
              #alpha = factor*self.f2oJax.jnp.sum( self.f2oJax.jnp.real( self.f2oJax.jnp.conj(grad) * grad ), axis=(0,1,2,3) ) / self.f2oJax.jnp.sum( self.f2oJax.jnp.real( self.f2oJax.jnp.conj(den) * den ), axis=(0,1,2) )
              alpha = factor*self.gradDen4_SS(grad, den)
              
              alpha.shape = (self.bfShape[3],)

              self.alphaPrv = alpha.copy()

           ## "correct": Note that we need the l2-norm of its inverse FFT
           #grad.shape = self.xShape
           #den.shape  = self.bfShape
           #alpha = ( 2.*np.sum( np.real( np.conj(grad) * grad ).ravel() ) - np.sum( np.real( np.conj(grad[:,0,]) * grad[:,0,] ).ravel() )
                     #- np.sum( np.real( np.conj(grad[:,-1,]) * grad[:,-1,] ).ravel() ) )
                    
           #alpha /= ( 2.*np.sum( np.real( np.conj(den) * den ).ravel() ) - np.sum( np.real( np.conj(den[:,0,]) * den[:,0,] ).ravel() )
                     #- np.sum( np.real( np.conj(den[:,-1,]) * den[:,-1,] ).ravel() ) )
                
                
        
      else:

        if self.freqSol is False: 
           alpha = self.alphaPrv.copy()
           
        else:
          if len(self.bfShape) == 3:
             alpha = self.alphaPrv.copy()
       
          if len(self.bfShape) == 4:
             alpha = self.alphaPrv.copy()
  
      if self.mulCte > 0:
         alpha *= self.mulCte
  
  
      return alpha, grad



    def CauchySupp_SS(self, k, grad, x, x_sptl=None):
  
      if(k==0):
    
        
        den = self.fAx(grad, 0, None)
        
        if self.freqSol is False:           
           alpha = sum(grad*grad)/sum(den*den)
        else:
          
           # Compensate for upsamplng / downsampling
           factor = self.dwFactor_SS(den, grad)
          
           if len(self.bfShape) <= 3:   # single input image (grayscale or color)
              alpha = factor*self.f2oJax.jnp.sum( self.f2oJax.jnp.real( self.f2oJax.jnp.conj(grad) * grad ) ) / \
                             self.f2oJax.jnp.sum( self.f2oJax.jnp.real( self.f2oJax.jnp.conj(den) * den ) )
              
           if len(self.bfShape) == 4:   # multiple input image

              den   = self.f2oJax.setShape(den, self.bfShape)
              grad  = self.f2oJax.setShape(grad, self.xShape)

              alpha = self.f2oJax.jnp.reshape(factor*
                                              self.f2oJax.jnp.sum( self.f2oJax.jnp.real( self.f2oJax.jnp.conj(grad) * grad ), axis=(0,1,2,3) ) / \
                                              self.f2oJax.jnp.sum( self.f2oJax.jnp.real( self.f2oJax.jnp.conj(den) * den ), axis=(0,1,2) ),
                                              (self.bfShape[3],))
                       
          
           self.roGradFun(k, grad, None)
        
      else:
      
  
        if self.freqSol is False:
         
          mask  = np.abs( x ) > 0
          gSupp = grad*mask.astype(np.float32)
          den = self.fAx(gSupp, 0, None)
         
          alpha = sum(gSupp*gSupp)/sum(den*den)
         
        else:

          gSupp, grad = self.roGradFun(k, grad, x_sptl)
          
          #mask = np.abs( x_sptl ) > 0 
          #grad.shape = self.xShape                    
          #gSupp = pyfftw.interfaces.numpy_fft.rfft2( pyfftw.interfaces.numpy_fft.irfft2(grad, axes=(0,1) )*mask.astype(np.float32), axes=(0,1) ).ravel()
  
          den = self.fAx(gSupp, 0, None)
          
          # Compensate for upsamplng / downsampling
          factor = self.dwFactor_SS(den, grad)
          
          if len(self.bfShape) <= 3:   # single input image (grayscale or color)
              alpha = factor*self.f2oJax.jnp.sum( self.f2oJax.jnp.real( self.f2oJax.jnp.conj(gSupp) * gSupp ) ) / \
                             self.f2oJax.jnp.sum( self.f2oJax.jnp.real( self.f2oJax.jnp.conj(den) * den ) )
              
          if len(self.bfShape) == 4:   # multiple input image

            den   = self.f2oJax.setShape(den, self.bfShape)
            gSupp = self.f2oJax.setShape(gSupp, self.xShape)

            alpha = self.f2oJax.setShape( factor*
                                          self.f2oJax.jnp.sum( self.f2oJax.jnp.real( self.f2oJax.jnp.conj(gSupp) * gSupp ), axis=(0,1,2,3) ) / \
                                          self.f2oJax.jnp.sum( self.f2oJax.jnp.real( self.f2oJax.jnp.conj(den) * den ), axis=(0,1,2) ),              
                                          (self.bfShape[3],))
                               
      
      if self.mulCte > 0:
         alpha = alpha*self.mulCte
           
      return alpha, grad



    def CauchyLSupp_SS(self, k, grad, x, x_sptl=None):
        
      if k%2 == 0:
        

        if(k==0): # equivalent to Cauchy
        
        
          den = self.fAx(grad, 0, None)
        
          if self.freqSol is False:           
            
            alpha = sum(grad*grad)/sum(den*den)
            self.alphaPrv = alpha
            
          else:
            
            
            # Compensate for upsamplng / downsampling
            factor = self.dwFactor_SS(den, grad)
            
            
            if len(self.bfShape) <= 3:   # single input image (grayscale or color)
               #if self.gradDen is None:
                 #self.gradDen = self.f2oJax.getJIT( self.gradDen3_SS )
                 
               alpha = factor*self.gradDen3_SS(grad, den)
               #alpha = factor*np.sum( np.real( np.conj(grad) * grad ) ) / np.sum( np.real( np.conj(den) * den ) )
               self.alphaPrv = alpha
               
            if len(self.bfShape) == 4:   # multiple input image
             
               den  = self.f2oJax.setShape(den, self.bfShape)
               grad = self.f2oJax.setShape(grad, self.xShape)

               #if self.gradDen is None:
                 #self.gradDen = self.f2oJax.getJIT( self.gradDen4_SS)
                 
               alpha = factor*self.gradDen4_SS(grad, den)
              
               #alpha = factor*np.sum( np.real( np.conj(grad) * grad ), axis=(0,1,2,3) ) / np.sum( np.real( np.conj(den) * den ), axis=(0,1,2) )
               alpha = self.f2oJax.setShape(alpha, (self.bfShape[3],) )

               self.alphaPrv = alpha.copy()
               
        
            self.roGradFun(k, grad, None)
                           
        else: # main selection
      
  
          if self.freqSol is False:
         
            mask  = np.abs( x ) > 0
            gSupp = grad*mask.astype(np.float32)
            den = self.fAx(gSupp, 0, None)
         
            alpha = sum(gSupp*gSupp)/sum(den*den)
         
            self.alphaPrv = alpha
         
          else:

            gSupp, grad = self.roGradFun(k, grad, x_sptl)
              
            den = self.fAx(gSupp, 0, None)

            # Compensate for upsamplng / downsampling
            factor = self.dwFactor_SS(den, grad)


            if len(self.bfShape) <= 3:   # single input image (grayscale or color)
               alpha = factor*np.sum( np.real( np.conj(gSupp) * gSupp ) ) / np.sum( np.real( np.conj(den) * den ) )
               self.alphaPrv = alpha.copy()
              
            if len(self.bfShape) == 4:   # multiple input image
             
               den   = self.f2oJax.setShape(den, self.bfShape)
               gSupp = self.f2oJax.setShape(gSupp, self.xShape)
              
               alpha = factor*np.sum( np.real( np.conj(gSupp) * gSupp ), axis=(0,1,2,3) ) / np.sum( np.real( np.conj(den) * den ), axis=(0,1,2) )
               alpha = self.f2oJax.setShape(alpha, (self.bfShape[3],) )

               self.alphaPrv = alpha.copy()
          
                
        
      else: # copy
        
        if self.freqSol is False:        # Make it more general
          alpha = self.alphaPrv
          
        else:
          # No step-size computation, but (potentially) grad must be corrected
          gSupp, grad = self.roGradFun(k, grad, x_sptl)
          
          if len(self.bfShape) == 3:
             alpha = self.alphaPrv
  
          if len(self.bfShape) == 4:
             alpha = self.alphaPrv.copy()
               
  
      if self.mulCte > 0:
         alpha = alpha*self.mulCte
      
  
      return alpha, grad

    

    def BBv1_SS(self, k, grad, x, x_sptl=None):
  
      if(k==0):
    
        self.xprv = x.ravel().copy()          # records previous solution
        
        den = self.fAx(grad, 0, None)
        
        
        if self.freqSol is False:           
           alpha = sum(grad*grad)/sum(den*den)
        else:
          
           # Compensate for upsamplng / downsampling
           factor = self.dwFactor_SS(den, grad)
                    
           if len(self.bfShape) <= 3:   # single input image (grayscale or color)
              alpha = factor*jnp.sum( jnp.real( jnp.conj(grad) * grad ) ) / jnp.sum( jnp.real( jnp.conj(den) * den ) )
              
           if len(self.bfShape) == 4:   # multiple input image
             
              #den.shape  = self.bfShape
              #grad.shape = (self.bfShape[0], self.bfShape[1], self.bfShape[2], self.xShape[3], self.bfShape[3])
              den  = self.f2oJax.setShape( den, self.bfShape)
              grad = self.f2oJax.setShape( grad, (self.bfShape[0], self.bfShape[1], self.bfShape[2], self.xShape[3], self.bfShape[3]) )
              
              alpha = factor*jnp.sum( jnp.real( jnp.conj(grad) * grad ), axis=(0,1,2,3) ) / jnp.sum( jnp.real( jnp.conj(den) * den ), axis=(0,1,2) )
              
              alpha = self.f2oJax.setShape( alpha, (self.bfShape[3],) )
              #alpha.shape = (self.bfShape[3],)
        
      else:

        s = x - self.xprv
        #y = Phi.transpose().dot( Phi.dot(s) )
        y = self.fAx( self.fAx(s,0,None), 1, None)      # A^T * ( A*s )
    
        
        if self.freqSol is False:           
           alpha =  sum(s*s)/sum(s*y)
        else:
          
          
          if len(self.bfShape) <= 3:
             alpha = jnp.sum( jnp.real( jnp.conj(s) * s ) ) / jnp.sum( np.real( jnp.conj(s) * y ) )
          
          if len(self.bfShape) == 4:   # multiple input image
             
              s  = self.f2oJax.setShape(s, self.xShape)
              y  = self.f2oJax.setShape(y, self.xShape)
              
              alpha = jnp.sum( jnp.real( jnp.conj(s) * s ), axis=(0,1,2,3) ) / jnp.sum( jnp.real( jnp.conj(s) * y ), axis=(0,1,2,3) )

              alpha = self.f2oJax.setShape( alpha, (self.bfShape[3],) )
              #alpha.shape = (self.bfShape[3],)

          
        self.xprv = x.ravel().copy()          # records previous solution
        
      return alpha, grad
      
      
    def BBv2_SS(self, k, grad, x, x_sptl=None):
  
      if(k==0):
    
        self.xprv = x.ravel().copy()          # records previous solution
        
        den = self.fAx(grad, 0, None)
        
        if self.freqSol is False:           
           alpha = sum(grad*grad)/sum(den*den)
        else:

           # Compensate for upsamplng / downsampling
           factor = self.dwFactor_SS(den, grad)

           if len(self.bfShape) <= 3:   # single input image (grayscale or color)
              alpha = factor*np.sum( np.real( np.conj(grad) * grad ) ) / np.sum( np.real( np.conj(den) * den ) )
              
           if len(self.bfShape) == 4:   # multiple input image
             
              den  = self.f2oJax.setShape(den, self.bfShape)
              grad = self.f2oJax.setShape(grad, (self.bfShape[0], self.bfShape[1], self.bfShape[2], self.xShape[3], self.bfShape[3]) )
              
              alpha = factor*np.sum( np.real( np.conj(grad) * grad ), axis=(0,1,2,3) ) / np.sum( np.real( np.conj(den) * den ), axis=(0,1,2) )
              
              alpha = self.f2oJax.setShape(alpha, (self.bfShape[3],))

    
      else:
        s = x - self.xprv
        y = self.fAx( self.fAx(s,0,None), 1, None)      # A^T * ( A*s )
    
        if self.freqSol is False:           
           alpha =  sum(s*y)/sum(y*y)
        else:
          
          if len(self.bfShape) <= 3:
             alpha = np.sum( np.real( np.conj(s) * y ) ) / np.sum( np.real( np.conj(y) * y ) )

          if len(self.bfShape) == 4:   # multiple input image
             
              s  = self.f2oJax.setShape(s, self.xShape)
              y  = self.f2oJax.setShape(y, self.xShape)
              
              alpha = np.sum( np.real( np.conj(s) * y ), axis=(0,1,2,3) ) / np.sum( np.real( np.conj(y) * y ), axis=(0,1,2,3) )

              alpha = self.f2oJax.setShape(alpha, (self.bfShape[3],))

        self.xprv = x.ravel().copy()          # records previous solution
  
      return alpha, grad
      
      
    def BBv3_SS(self, k, grad, x, x_sptl=None):
  
      if(k==0):
    
        self.xprv = x.ravel().copy()          # records previous solution
        
        den = self.fAx(grad, 0, None)
        
        if self.freqSol is False:           
           alpha = sum(grad*grad)/sum(den*den)
        else:

           # Compensate for upsamplng / downsampling
           factor = self.dwFactor_SS(den, grad)

           if len(self.bfShape) <= 3:   # single input image (grayscale or color)
              alpha = factor*np.sum( np.real( np.conj(grad) * grad ) ) / np.sum( np.real( np.conj(den) * den ) )
              
           if len(self.bfShape) == 4:   # multiple input image
             
              den  = self.f2oJax.setShape(den, self.bfShape)
              grad = self.f2oJax.setShape(grad, (self.bfShape[0], self.bfShape[1], self.bfShape[2], self.xShape[3], self.bfShape[3]) )
              
              alpha = factor*np.sum( np.real( np.conj(grad) * grad ), axis=(0,1,2,3) ) / np.sum( np.real( np.conj(den) * den ), axis=(0,1,2) )
              alpha = self.f2oJax.setShape(alpha, (self.bfShape[3],) )
    
      else:
        s = x - self.xprv
        y = self.fAx( self.fAx(s,0,None), 1, None)      # A^T * ( A*s )
    
        if self.freqSol is False:           
           alpha =  np.sqrt( sum(s*s)/sum(y*y) )
        else:
          
          if len(self.bfShape) <= 3:
             alpha = np.sqrt( np.sum( np.real( np.conj(s) * s ) ) / np.sum( np.real( np.conj(y) * y ) ) )
            
          if len(self.bfShape) == 4:   # multiple input image
             
              s  = self.f2oJax.setShape(s, self.xShape)
              y  = self.f2oJax.setShape(y, self.xShape)
              
              alpha = np.sqrt( np.sum( np.real( np.conj(s) * s ), axis=(0,1,2,3) ) / np.sum( np.real( np.conj(y) * y ), axis=(0,1,2,3) ) )

              alpha = self.f2oJax.setShape(alpha, (self.bfShape[3],) )


        self.xprv = x.ravel().copy()          # records previous solution
  
      return alpha, grad
      

    def cteAA_SS(self, k, grad, x, x_sptl=None):
        
      if k == 0:
        
        den = self.fAx(grad, 0, None)
                   
        if self.freqSol is False:           
           alpha = sum(grad*grad)/sum(den*den)
        else:
                      
           alpha = np.sum( np.real( np.conj(grad) * grad ) ) / np.sum( np.real( np.conj(den) * den ) )
                
        self.alphaPrv = alpha
        
      else:
        alpha = self.alphaPrv
  
      if self.mulCte > 0:
         alpha *= self.mulCte
  
      return alpha

      
    def cteAuto_SS(self, k, grad, x, x_sptl=None):
        
      if k == 0:
        
        den = self.fAx(grad, 0, None)
                   
        if self.freqSol is False:           
           alpha = sum(grad*grad)/sum(den*den)
        else:
                      
           alpha = np.sum( np.real( np.conj(grad) * grad ) ) / np.sum( np.real( np.conj(den) * den ) )
                
        self.alphaPrv = 0.5*alpha
        
      else:
        alpha = self.alphaPrv
  
      return alpha


    def dwFactor_SS(self, den, grad):

      # Compensate for upsamplng / downsampling
      if self.dwFactor is not None:
         factor = np.float(den.ravel().shape[0]) / np.float(grad.ravel().shape[0]) 
      else:
         factor = 1.0 
      
      return np.sqrt(factor)


#=========================================================================
#=========================================================================

class robustGrad(dataShpMixin, stepSizeMixin, roGradMixin):
    r"""robustGrad policy and routines.

    """
    def __init__(self, args):
      
        self.cpy_essShp(args)
        self.cpy_SS(args)
        self.cpy_roGrad(args)
        self.gprv      = None           # previous solution's gradient        
        self.gSptlprv  = None           # previous solution's gradient      
        #self.maskprv   = None

        try:
          self.f2oJax = args.f2oJax
        except:
          pass
      
      
    def sel_roGrad(self):
      switcher = {
          f2oDef.roGrad.Null.val:     self.Null_roGrad,
          f2oDef.roGrad.lI.val:       self.lI_roGrad,
          f2oDef.roGrad.Rel.val:      self.Rel_roGrad,
          f2oDef.roGrad.Supp.val:     self.Supp_roGrad
          }
      
      roGradFun = switcher.get(self.roGradPolicy.val, "nothing")
      
      func = lambda k, grad, x_sptl=None: roGradFun(k, grad, x_sptl)
      
      return func


    def Null_roGrad(self, k, grad, x_sptl):
      
        if self.freqSol:
          
           if k==0:
             
              gSupp = None
              
           else:
      
              mask = self.f2oJax.jnp.abs( x_sptl ) > 0 
              
              grad = self.f2oJax.setShape(grad, self.xShape)
              g_sptl = self.f2oJax.irfft2(grad, axes=(0,1))
              gSupp  = self.f2oJax.rfft2( g_sptl*mask.astype(self.f2oJax.jnp.float32), axes=(0,1) ).ravel()
              
      
        return gSupp, grad
      
      
      
    def lI_roGrad(self, k, grad, x_sptl):
      
        if self.freqSol:
          
           if k==0:

              grad = self.f2oJax.setShape(grad, self.xShape)

              g_sptl = self.f2oJax.irfft2(grad, axes=(0,1))                                                          
              self.lIgrad = self.f2oJax.jnp.max( self.f2oJax.jnp.abs (g_sptl) )
                          
              gSupp = None
              
           else:
          
              mask = self.f2oJax.jnp.abs( x_sptl ) > 0 
              
              grad = self.f2oJax.setShape(grad, self.xShape)

              g_sptl = self.f2oJax.irfft2(grad, axes=(0,1))
                         
              lI_grad = self.f2oJax.jnp.max( self.f2oJax.jnp.abs(g_sptl) )

              if lI_grad > self.lIgrad:  # update Gradient 
                 g_sptl = self.f2oJax.jnp.where( self.f2oJax.jnp.abs(g_sptl) > self.lIgrad, self.f2oJax.jnp.sign(g_sptl)*self.lIgrad, g_sptl)
                 grad   = self.f2oJax.rfft2( g_sptl, axes=(0,1) ).ravel()
               
                 if self.mulCte > self.ssCteMin*self.ssCteIni:
                    self.mulCte *= self.roGrad_ssMul

                 #print('lI rule ',self.mulCte)
              else:
                 self.lIgrad = lI_grad
                                            
    
              gSupp = self.f2oJax.rfft2( g_sptl*mask.astype(np.float32), axes=(0,1) ).ravel()
      
      
        return gSupp, grad


    def Rel_roGrad(self, k, grad, x_sptl):
      
        
        if self.freqSol:
          
           if k==0:
             
              grad = self.f2oJax.setShape(grad,self.xShape)
              g_sptl = self.f2oJax.irfft2(grad, axes=(0,1))                                                          
              self.lIgrad = self.f2oJax.jnp.max( self.f2oJax.jnp.abs(g_sptl) )
              
              self.gSptlprv = g_sptl
              
              gSupp  = None
              
           else:
      
              mask = self.f2oJax.jnp.abs( x_sptl ) > 0 
              grad = self.f2oJax.setShape(grad, self.xShape)
          
              g_sptl = self.f2oJax.irfft2(grad, axes=(0,1))
                         
              lI_grad = self.f2oJax.jnp.max( self.f2oJax.jnp.abs(g_sptl) )
               
              if lI_grad > self.lIfactor*self.lIgrad:  # update Gradient

                 #print('relax',self.relaxCte, 'mulCte', self.mulCte, 'mulCond', self.ssCteMin*self.ssCteIni )                       
                 
                 g_sptl = self.relaxCte*g_sptl + (1.0-self.relaxCte)*self.gSptlprv
                     
                 self.mulCte = np.max(np.array([self.ssCteMin, self.mulCte*self.roGrad_ssMul]))   
                     
              else: 

                 self.lIgrad = lI_grad
                 
              grad = self.f2oJax.rfft2( g_sptl*mask.astype(np.float32), axes=(0,1) ).ravel()
              gSupp = self.f2oJax.copy(grad).ravel()

              self.gSptlprv = g_sptl

                
        return gSupp, grad
      


    def Supp_roGrad(self, k, grad, x_sptl):
      
        if self.freqSol:
          
           if k==0:
             
              grad = self.f2oJax.setShape(grad, self.xShape)
              g_sptl = self.f2oJax.irfft2(grad, axes=(0,1))                                                          
              self.lIgrad = self.f2oJax.jnp.max( self.f2oJax.jnp.abs (g_sptl) )
              
              gSupp  = None
              
           else:
      
              mask = self.f2oJax.jnp.abs( x_sptl ) > 0 
              grad = self.f2oJax.setShape(grad, self.xShape)
          
              g_sptl = self.f2oJax.irfft2(grad, axes=(0,1))
                         
              lI_grad = self.f2oJax.jnp.max( self.f2oJax.jnp.abs (g_sptl) )
               
              if lI_grad > self.lIfactor*self.lIgrad:  # update Gradient

                 self.mulCte = np.max(np.array([self.ssCteMin, self.mulCte*self.roGrad_ssMul]))   
                     
              else: 

                 self.lIgrad = lI_grad
                 
              grad  = self.f2oJax.rfft2( g_sptl*mask.astype(np.float32), axes=(0,1) ).ravel()
              gSupp = self.f2oJax.copy(grad).ravel()

              self.gSptlprv = g_sptl

                
        return gSupp, grad
            
      
#=========================================================================
#=========================================================================



class proxOp(proxOpMixin):
    r"""proximal and projection operators and routines.

    """
    def __init__(self, args, enableJAX=None):
      
        self.xprv      = None
        self.alphaPrv  = None
        self.xShape    = args.xShape

        self.cpy_proxOp(args)

        self.f2oJax  = f2oJaxUtils(flagForceJAX=enableJAX)
        
      
    def sel_ppOp(self, dual=False):
      
      if dual is False:
         switcher = {
          f2oDef.prox_None:       self.prox_none,
          f2oDef.prox_l1:         self.prox_l1,
          f2oDef.prox_l0:         self.prox_l0,
          f2oDef.proj_l1:         self.proj_l1,
          f2oDef.proj_l0:         self.proj_l0,
          f2oDef.prox_TV:         self.prox_TV,
          f2oDef.prox_l0TV:       self.prox_l0TV
          }
      else:
         switcher = {
          f2oDef.prox_None:       self.prox_none,
          f2oDef.prox_TV:         self.prox_TV_dual,
          f2oDef.prox_l0TV:       self.prox_l0TV_dual
          }
      
      pFun = switcher.get(self.proxOp, "nothing")
      
      func = lambda u, lmbda: pFun(u, lmbda)
      
      return func


    # ----------------------------------------
    # ----------------------------------------
        
    def prox_none(self, u, lmbda=None):
      
      return u

        
    def prox_l1(self, u, lmbda):
      
      #th = np.maximum(0, np.abs(u) - lmbda)    
      #return( np.sign(u)*th )
    
      return np.sign(u) * (np.clip(np.abs(u) - lmbda, 0, float('Inf')))
      #return np.sign(u) * np.maximum(np.abs(u) - lmbda, 0.)
    
    
    def prox_l0(self, u, lmbda):
      # FIXME: write this routine
      pass
    
    def proj_l1(self, u, lmbda):
      # FIXME: write this routine      
      pass
    
    def proj_l0(self, u, lmbda):
      # FIXME: write this routine      
      pass
    
    
    #def prox_TV(self, u, lmbda):
      
      #sq = np.sqrt(np.sum(u**2, keepdims=True))
      #th = np.maximum(0, sq - lmbda)
    
      #th = np.divide(th, sq, out=np.zeros_like(th), where=(sq != 0))
    
      #return( u*th )
      
    def prox_TV(self, v, lmbda):

      sq = np.sum(v**2.0, axis=-1, keepdims=True) 
      if len(sq.shape) <= 3:
        sq = np.sqrt(sq)
      else:
        sq = np.sqrt( np.sum(sq,axis=-2, keepdims=True) )
        
      th = np.clip(sq - lmbda, 0, float('Inf'))
    
      w =  np.where(sq==0, 0, th)/np.where(sq==0, 1, sq)
    

      return(w*v)
    
    
    def prox_l0TV(self, v, lmbda):

      sq = np.sum(np.abs(v), axis=-1, keepdims=True)         
      th = sq < np.sqrt(2.*lmbda)
      w =  np.where(th, 0., 1.)
        
      return(w*v)


    def prox_TV_dual(self, v, lmbda): # This is just the normalization step

      origShp = v.shape
      
      v = self.f2oJax.setShape(v, self.xShape)
      
      mask = self.f2oJax.jnp.sqrt(v[...,0]*v[...,0] + v[...,1]*v[...,1])    
    
      w = self.f2oJax.jnp.einsum('ij...k,ij...->ij...k', v, 1./self.f2oJax.jnp.where(mask>1,mask,1.0)).ravel()

      v = self.f2oJax.setShape(v, origShp)
      
      return(w)

    
    def prox_l0TV_dual(self, v, lmbda):
      
      pass
    

    
#=========================================================================
#=========================================================================

    
class ISeq(IseqMixin):
    r"""Inertial sequence for Nesterov acceleration.

    """
    def __init__(self, args):
      
        self.cpy_Iseq(args)
        
        self.xprv          = None
        self.alphaPrv      = None

        
      
    def sel_iseqOp(self):
      switcher = {
          f2oDef.iseq.ntrv.val:       self.iseq_ntrv,
          f2oDef.iseq.lin.val:        self.iseq_lin,
          f2oDef.iseq.glin.val:       self.iseq_glin
          }
      
      iseqFun = switcher.get(self.ISeqPolicy.val, "nothing")
      
      func = lambda k, t: iseqFun(k, t)
      
      return func


    # ----------------------------------------
    # ----------------------------------------


    def iseq_ntrv(self, k, t):
        """Nesterov inertial sequence """
        gamma = 0.5 * float(1. + np.sqrt(1. + 4. * t**2.))
        
        return(gamma)


    def iseq_lin(self, k, t):
        """Linear REF inertial sequence """
      
        gamma = ((k+1.) - 1. + self.iseq_extra1)/self.iseq_extra1

        return(gamma)


    def iseq_glin(self, k, t):
        """Generalized linear REF inertial sequence """
      
        gamma = ((k+1.) - 1. + self.iseq_extra2)/self.iseq_extra1

        return(gamma)


