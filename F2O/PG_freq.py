import scipy as sc
import matplotlib.pylab as PLT
import numpy as np


from F2O.F2O_utils import f2oJaxUtils, argsF2O, stepSize, stopCondF2O, costGradF2O, proxOp, ISeq, robustGrad
from F2O.F2O_freq import commomPGD

from F2O.fwOp.fwOperator import conv2DOp



from timeit import default_timer as timer


import multiprocessing

import pyfftw

pyfftw.config.NUM_THREADS = multiprocessing.cpu_count()
pyfftw.config.PLANNER_EFFORT = 'FFTW_MEASURE'
pyfftw.interfaces.cache.enable()
pyfftw.interfaces.cache.set_keepalive_time(300)


__author__ = """Paul Rodriguez <prodrig@pucp.edu.pe>"""



def pg(Op, b, lmbda, nIter=100, args=None):  
  r"""Proximal gradient (includes ISTA)
  
  Solve the l1 regularized problem problem in frequency:

    F(x) = f( Ax(x) - b ) + \lambda || x ||_1
    
  where f *is* the l2 or the Frobenious norm, Ax is a forward operator 
  such the Convolution theorem applies, b is the input (observed) data, 
  via the iteration
  
    g_k = \nabla f(x_k) = ifft( conj( fft(Ax) ) *  ( fft(A) * fft(x) - fft(b) )
  
    x_{k+1} = prox_{l1}( x_k - \alpha \cdot g_k, lmbda )
    
  where 
  
  * Ax represent the function (operations) to be carried out in the frequency domain.
    See fwOp_f class in fwOperator.py
  * The gradient computation is carried out by the costGradF2O class (see F2O_utils.py), which
    uses the Ax operator
  * b, the observed data, is already in the frequency domain
    
  """
  #=================================================================
  
  
  
  if args is None:
    return(None, None)
    
    
    
  # Initialization

  pg = commomPGD(False)          # True: accelerated PG. False --> just PG


  # input data is transfered to 'device' (if needed)
  bDev = args.f2oJax.getDataIn(b)  

  args.lmbda = lmbda
  x, Ax, f, stopFunc, pgStats, itemStats = pg.Init(Op, bDev, nIter, args)
    
  
  # Get subroutine (e.g. step-size computation, stoping criteria, etc)
  computeCost, computeGrad, computeSS, prox = pg.SetFunctions(args, f, Ax, nIter)
  
  
  # NOTE: args.Bf <--> FFT(b), Op.Af <--> FFT(fwOp), x is in freq (grad as well)
  
  x_sptl = np.zeros(1)
  
  # Elapse time per iteration (after initializations)
  pg.iterTimer(args.Timer, True, False)

  
  for k in range(nIter):
    
    
    grad = computeGrad(x, args.Bf, x_sptl)      # compute gradient
                                                # NOTE: x_sptl is used to compute the cost functional along
                                                # the gradient. See costGradF2O class (in F2O_utils.py)
    alpha = computeSS(k, grad, x)               # compute step-size
    
    
    # --- statistics, record step-Size --
    pg.RecordStats(k, x, args.Bf, grad, pgStats, itemStats, x_sptl, computeCost)
    #pgStats[k,1] = alpha
    if 'ssX' in itemStats.keys():
       pgStats[k,itemStats['ssX']] = np.mean(alpha)
    # --- 
          
          
    
    x.shape = args.xShape                       # needed to apply FFT / IFFT
    grad.shape = args.xShape
    
    # proximal gradient descent step
    x_sptl = prox(args.f2oJax.irfft2( x - alpha*grad, axes=(0,1) ), alpha*lmbda)     
    x      = args.f2oJax.rfft2( x_sptl, axes=(0,1) ).ravel()
    
        
    
    #pgStats[k,2] = pg.iterTimer(args.Timer, False, True)        # Elapse time
    if 'time' in itemStats.keys():
       pgStats[k,itemStats['time']] = pg.iterTimer(args.Timer, False, True)
    
    if stopFunc(k, x_sptl.ravel(), pgStats):                    # check for exit condition / print stats
      print('Stop criteria -- iteration {0}'.format(k))
      break
    
  
  if args.xShape is not None:
    x.shape = args.xShape
  
  
  return(x, pgStats)
  
  
  
def apg(Op, b, lmbda, nIter=100, args=None):  
  r"""Accelerated Proximal gradient (includes ISTA)
  
  Solve the l1 regularized problem problem:

    F(x) = f( Ax(x) - b ) + \lambda || x ||_1
    
  where f is the fidelity functional, Ax is the forward operator and
  b is the input (observed) data, via the iteration
  
    x_{k+1} = prox_{l1}( x_k - \alpha \cdot \nabla f(x_k), lmbda )
    
  where \nabla f can be computed via the f (costGradF2O class in F2O_utils.py)
  and Ax (fwOp class in fwOperator.py) classes
    
  """
  #=================================================================
  
  
  # -----------------------
  
  if args is None:
    return(None, None)
    

    
  # Initialization
  
  apg = commomPGD(True)          # True: accelerated PG. False --> just PG
  

  # input data is transfered to 'device' (if needed)
  bDev = args.f2oJax.getDataIn(b)  
  
  args.lmbda = lmbda
  x1, Ax, f, stopFunc, apgStats, itemStats = apg.Init(Op, bDev, nIter, args)


  y = args.f2oJax.copy(x1)
  
  
  t0 = 1.
  t1 = 1.
  
  # Get subroutine (e.g. step-size computation, stoping criteria, etc)
  computeCost, computeGrad, computeSS, prox, iseq = apg.SetFunctions(args, f, Ax, nIter)
  

  x_sptl = np.zeros(1)
  
  # Elapse time per iteration (after initializations)
  apg.iterTimer(args.Timer, True, False)
  
  
  
  for k in range(nIter):
    
    if k > 1:
       Op.thresh = alpha*lmbda
       
    grad = computeGrad(y, args.Bf, x_sptl)      # compute the gradient
                                                # NOTE: x_sptl is used to compute the cost functional along
                                                # the gradient. See costGradF2O class (in F2O_utils.py)
                                                
    alpha, grad = computeSS(k, grad, y, x_sptl) # compute step-size

    x0 = args.f2oJax.copy(x1)


    # proximal gradient descent step
    y = args.f2oJax.setShape(y, args.xShape)            # needed to apply FFT / IFFT
    grad = args.f2oJax.setShape(grad, args.xShape)

       
    x_sptl = prox(args.f2oJax.irfft2( y - alpha*grad, s=args.xSptlShape[:2], axes=(0,1) ), alpha*lmbda)     
    x1     = args.f2oJax.rfft2( x_sptl, s=args.xSptlShape[:2], axes=(0,1) ).ravel()
    

    ### proximal gradient descent step
    ##x_sptl = prox(pyfftw.interfaces.numpy_fft.irfft2( y - alpha*grad, axes=(0,1) ), alpha*lmbda)     
    ##x1     = pyfftw.interfaces.numpy_fft.rfft2( x_sptl, axes=(0,1) ).ravel()
    
    t1 = iseq(k, t0)                            # compute inertial sequence
    gamma = (t0-1)/t1
    
    y = x1 + gamma*(x1-x0)                     # extrapolation step

    t0 = t1

    

    
    # --- statistics -- record step-Size ---
    apg.RecordStats(k, x1, b, grad, apgStats, itemStats, x_sptl, computeCost)
    if 'ssX' in itemStats.keys():
       apgStats[k,itemStats['ssX']] = np.mean(alpha)
       
    if 'time' in itemStats.keys():
       apgStats[k,itemStats['time']] = apg.iterTimer(args.Timer, False, True)
    # --- 
    
    
    
    if stopFunc(k, x_sptl.ravel(), apgStats):           # check for exit condition / print stats
      print('Stop criteria -- iteration {0}'.format(k))
      break
    
  
  
  xOut = args.f2oJax.getDataOut(x1)    
  
  if args.xShape is not None:      
    xOut.shape = args.xShape
  
  
  return(xOut, apgStats)


# ======================================================


def apgTV(Op, b, lmbda, nIter=np.array([100,10]), args=None):  
  r"""Accelerated Proximal gradient (includes ISTA) for l1/l0 TV
  
  Solve the l1 regularized problem problem:

    F(x) = f( Ax(x) - b ) + \lambda || \nabla x ||_1
    
  where f is the fidelity functional, Ax is the forward operator and
  b is the input (observed) data.
    
  """
  #=================================================================
  
  
  # -----------------------
  
  if args is None:
    return(None, None)
    
  print('apgTV')

  if hasattr(args, 'beta'):      # this is the step-size for TV
     pass
  else:
     args.beta = 1./8.          # see FISTA-TV paper
     
  # Initialization
  
  apg = commomPGD(True)          # True: accelerated PG. False --> just PG
  

  # input data is transfered to 'device' (if needed)
  bDev = args.f2oJax.getDataIn(b)  
  
  args.lmbda = lmbda
  x1, Ax, f, stopFunc, apgStats, itemStats = apg.Init(Op, bDev, nIter[0], args)


  y = args.f2oJax.copy(x1)
  
  
  t0 = 1.
  t1 = 1.
  
  # Get subroutine (e.g. step-size computation, stoping criteria, etc)
  computeCost, computeGrad, computeSS, prox, iseq = apg.SetFunctions(args, f, Ax, nIter[0])
  

  x_sptl = np.zeros(1)
  
  # Elapse time per iteration (after initializations)
  apg.iterTimer(args.Timer, True, False)
  
  
  # outer-loop
  for k in range(nIter[0]):
    
    if k > 1:
       Op.thresh = alpha*lmbda
       
    grad = computeGrad(y, args.Bf, Op.Df)      # compute the gradient of the global problem
                                               #
                                               # NOTE: Df is the Dx / Dy operator in freq.
                                               # 
                                                
    alpha, grad = computeSS(k, grad, y, x_sptl) # compute step-size


    Dbkf = args.f2oJax.jnp.einsum('ij...k,ij...->ij...k', Op.Df,  args.f2oJax.setShape(x1 - alpha*grad,Op.xShape) )    
                                                                            # || Au - b ||_2^2 --> || u - (u_k - alpha*nabla_k) ||_2^2
                                                                            #
                                                                            # || b_k - \lambda*D^T v ||_2^2 --> D b_k  

    if k == 0:
      
       x1_dual = args.f2oJax.jnp.zeros( (*Op.xShape,2), dtype='complex64')
       y_dual  = x1_dual.copy()
    
    else:
      
       x1_dual *= 0.
       y_dual  *= 0.
       
    
    
    t0dual = 1.
    t1dual = 1.
    
    # inner-loop
    for itTV in range(nIter[1]):
            
        # \nabla  0.5|| \lambda*D^T v - b_k ||_2^2  --> D D^T v - D b_k
        
        DTv = args.f2oJax.jnp.einsum('ij...k,ij...k->ij...', args.f2oJax.jnp.conj(Op.Df), y_dual)        
        grad_dual = alpha*lmbda*args.f2oJax.jnp.einsum('ij...k,ij...->ij...k', Op.Df, DTv) - Dbkf
    
    
        # FISTA step
    
        x0_dual = x1_dual.copy()
    
        x1_dual = y_dual - (args.beta/(alpha*lmbda))*grad_dual
                       
    
        # normalization step
        #xSp = args.getROI( args.f2oJax.irfft2( x1_dual, s=args.padShape[:2], axes=(0,1)) )
        xSp = args.f2oJax.irfft2( x1_dual, s=args.padShape[:2], axes=(0,1)) 
        
        mask = args.f2oJax.jnp.sqrt(xSp[:,:,:,0]*xSp[:,:,:,0] + xSp[:,:,:,1]*xSp[:,:,:,1])
    
        xSp = args.f2oJax.jnp.einsum('ij...k,ij...->ij...k', xSp, 1./args.f2oJax.jnp.where(mask>1,mask,1.0))

        x1_dual = args.f2oJax.rfft2( xSp, s=args.padShape[:2], axes=(0,1) )
        
        # extra-gradient step
        t1dual = iseq(itTV, t0dual)
        gamma = (t0dual-1.)/t1dual
        
        y_dual = x1_dual + gamma*(x1_dual - x0_dual)
        
        t0dual = t1dual
    
    
    # (outer) FISTA step

    x0 = args.f2oJax.copy(x1)

    x1 = y - alpha*grad - alpha*lmbda*args.f2oJax.jnp.einsum('ij...k,ij...k->ij...', args.f2oJax.jnp.conj(Op.Df), x1_dual).ravel()

    # proximal gradient descent step
    #y = args.f2oJax.setShape(y, args.xShape)            # needed to apply FFT / IFFT
    #grad = args.f2oJax.setShape(grad, args.xShape)

    
    #x_sptl = prox(args.f2oJax.irfft2( y - alpha*grad, s=args.xSptlShape[:2], axes=(0,1) ), alpha*lmbda)     
    #x1     = args.f2oJax.rfft2( x_sptl, s=args.xSptlShape[:2], axes=(0,1) ).ravel()
    

    ### proximal gradient descent step
    ##x_sptl = prox(pyfftw.interfaces.numpy_fft.irfft2( y - alpha*grad, axes=(0,1) ), alpha*lmbda)     
    ##x1     = pyfftw.interfaces.numpy_fft.rfft2( x_sptl, axes=(0,1) ).ravel()
    
    t1 = iseq(k, t0)                           # compute inertial sequence
    gamma = (t0-1)/t1
    
    y = x1 + gamma*(x1-x0)                     # extrapolation step

    t0 = t1


    # 
    
    
    # --- statistics -- record step-Size ---
    apg.RecordStats(k, x1, b, grad, apgStats, itemStats, x_sptl, computeCost)
    if 'ssX' in itemStats.keys():
       apgStats[k,itemStats['ssX']] = np.mean(alpha)
       
    if 'time' in itemStats.keys():
       apgStats[k,itemStats['time']] = apg.iterTimer(args.Timer, False, True)
    # --- 
    
    
    
    if stopFunc(k, x1.ravel(), apgStats):           # check for exit condition / print stats
      print('Stop criteria -- iteration {0}'.format(k))
      break
    
  
  x1 = args.f2oJax.setShape(x1, args.xShape)
  #xOut = args.getROI( args.f2oJax.getDataOut( args.f2oJax.irfft2(x1, s=args.padShape[:2], axes=(0,1)) )  )
  xOut = args.f2oJax.getDataOut( args.f2oJax.irfft2(x1, s=args.padShape[:2], axes=(0,1)) ) 
  
  
  #xOut = args.f2oJax.getDataOut(x1)    
  
  #if args.xShape is not None:      
    #xOut.shape = args.xShape
  
  
  return(xOut, apgStats)


# ======================================================


def apgTVfqsp(Op, b, lmbda, nIter=np.array([10,10]), args=None):  
  r"""Accelerated Proximal gradient (includes ISTA) for l1/l0 TV
  
  Solve the l1 regularized problem problem:

    F(x) = f( Ax(x) - b ) + \lambda || \nabla x ||_1
    
  where f is the fidelity functional, Ax is the forward operator and
  b is the input (observed) data.
    
  """
  #=================================================================
  
  
  # -----------------------
  
  if args is None:
    return(None, None)
    
  print('apgTV-2')

  if hasattr(args, 'beta'):      # this is the step-size for TV
     pass
  else:
     args.beta = 1./8.          # see FISTA-TV paper
     
  # Initialization
  
  apg = commomPGD(True)          # True: accelerated PG. False --> just PG
  

  # input data is transfered to 'device' (if needed)
  bDev = args.f2oJax.getDataIn(b)  
  
  args.lmbda = lmbda
  x1, Ax, f, stopFunc, apgStats, itemStats = apg.Init(Op, bDev, nIter[0], args)


  y = args.f2oJax.copy(x1)
  
  
  t0 = 1.
  t1 = 1.
  
  # Get subroutine (e.g. step-size computation, stoping criteria, etc)
  computeCost, computeGrad, computeSS, prox, iseq = apg.SetFunctions(args, f, Ax, nIter[0])
  

  
  # Elapse time per iteration (after initializations)
  apg.iterTimer(args.Timer, True, False)
  
  x_sptl = 0.
  
  # outer-loop
  for k in range(nIter[0]):
    
    if k > 1:
       Op.thresh = alpha*lmbda
       
    grad = computeGrad(y, args.Bf, Op.Df)      # compute the gradient of the global problem
                                               #
                                               # NOTE: Df is the Dx / Dy operator in freq.
                                               # 

                                                
    alpha, grad = computeSS(k, grad, y, x_sptl) # compute step-size


    #x_sptl = args.getROI( args.f2oJax.irfft2( args.f2oJax.setShape(y-alpha*grad,args.xShape), s=args.padShape[:2], axes=(0,1)) )
    x_sptl = args.f2oJax.irfft2( args.f2oJax.setShape(y-alpha*grad,args.xShape), s=args.padShape[:2], axes=(0,1))
    
    DxVar = args.Dx(x_sptl)
    DyVar = args.Dy(x_sptl)
    Dbk   = args.f2oJax.jnp.concatenate( (DxVar[...,args.f2oJax.jnp.newaxis],DyVar[...,args.f2oJax.jnp.newaxis]), axis=-1)
    
                                                                            # || Au - b ||_2^2 --> || u - (u_k - alpha*nabla_k) ||_2^2
                                                                            #
                                                                            # || b_k - \lambda*D^T v ||_2^2 --> D b_k  

    if k == 0:
      
       x1_dual = args.f2oJax.jnp.zeros( Dbk.shape )
       y_dual  = args.f2oJax.copy( x1_dual )
    
    else:
      
       x1_dual *= 0.
       y_dual  *= 0.
       
    
    
    t0dual = 1.
    t1dual = 1.
    
    # inner-loop
    for itTV in range(nIter[1]):
            
        # \nabla  0.5|| \lambda*D^T v - b_k ||_2^2  --> D D^T v - D b_k
        
        DTv   = args.DxT(y_dual[...,0])+args.DyT(y_dual[...,1])
        
        DxVar = args.Dx(DTv)
        DyVar = args.Dy(DTv)
        
        grad_dual = alpha*lmbda*args.f2oJax.jnp.concatenate( (DxVar[...,args.f2oJax.jnp.newaxis],DyVar[...,args.f2oJax.jnp.newaxis]), axis=-1) - Dbk
            
    
        # FISTA step
    
        x0_dual = args.f2oJax.copy( x1_dual )
    
        x1_dual = y_dual - (args.beta/(alpha*lmbda))*grad_dual
                       
    
        # normalization step
        
        mask = args.f2oJax.jnp.sqrt(x1_dual[...,0]*x1_dual[...,0] + x1_dual[...,1]*x1_dual[...,1])    
        x1_dual = args.f2oJax.jnp.einsum('ij...k,ij...->ij...k', x1_dual, 1./args.f2oJax.jnp.where(mask>1,mask,1.0))

        
        # extra-gradient step
        t1dual = iseq(itTV, t0dual)
        gamma = (t0dual-1.)/t1dual
        
        y_dual = x1_dual + gamma*(x1_dual - x0_dual)
        
        t0dual = t1dual
    
    
    # (outer) FISTA step

    x0 = args.f2oJax.copy( x1 )



    x1 = args.f2oJax.rfft2( x_sptl - alpha*lmbda*(args.DxT(x1_dual[...,0]) + args.DyT(x1_dual[...,1])), s=args.padShape[:2], axes=(0,1) ).ravel()
        
    
    t1 = iseq(k, t0)                           # compute inertial sequence
    gamma = (t0-1)/t1
    
    y = x1 + gamma*(x1-x0)                     # extrapolation step

    t0 = t1


    # 
    
    
    # --- statistics -- record step-Size ---
    apg.RecordStats(k, x1, b, grad, apgStats, itemStats, x_sptl, computeCost)
    if 'ssX' in itemStats.keys():
       apgStats[k,itemStats['ssX']] = np.mean(alpha)
       
    if 'time' in itemStats.keys():
       apgStats[k,itemStats['time']] = apg.iterTimer(args.Timer, False, True)
    # --- 
    
    
    
    if stopFunc(k, x1.ravel(), apgStats):           # check for exit condition / print stats
      print('Stop criteria -- iteration {0}'.format(k))
      break
    
  
  x1 = args.f2oJax.setShape(x1, args.xShape)
  #xOut = args.getROI( args.f2oJax.getDataOut( args.f2oJax.irfft2(x1, s=args.padShape[:2], axes=(0,1)) )  )
  xOut = args.f2oJax.getDataOut( args.f2oJax.irfft2(x1, s=args.padShape[:2], axes=(0,1)) ) 
  
  
  #xOut = args.f2oJax.getDataOut(x1)    
  
  #if args.xShape is not None:      
    #xOut.shape = args.xShape
  
  
  return(xOut, apgStats)





      