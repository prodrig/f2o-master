

.. raw:: html

    <style> .red {color:#aa0060; font-weight:bold; font-size:16px} </style>

.. role:: red


.. raw:: html

    <style> .bgray {font-weight:bold;padding-top:0.5ex;padding-bottom:0.5ex;padding-right:95%;margin-top:1ex;color:black;background-color:rgb(224,224,224)} </style>

.. role:: bgray

  

Papers reproducibility
----------------------

:bgray:`2020`
=============
.. .. toctree::
..    :maxdepth: 1
  
Asilomar (ACSSC)
^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 1

..    notebooks/GD_ex1.ipynb
..    notebooks/GD_ex2.ipynb

   
   

:bgray:`2021`
=============
Eusipco
^^^^^^^

.. toctree::
   :maxdepth: 1

..    notebooks/GD_ex1.ipynb
..    notebooks/GD_ex2.ipynb
   

   
STSIVA
^^^^^^

.. toctree::
   :maxdepth: 1

..    notebooks/GD_ex1.ipynb
..    notebooks/GD_ex2.ipynb
   


   
:bgray:`2022`
=============
.. .. toctree::
..    :maxdepth: 1
ICIP
^^^^

.. toctree::
   :maxdepth: 1

..    notebooks/GD_ex1.ipynb
..    notebooks/GD_ex2.ipynb
   

   
Eusipco
^^^^^^^

.. toctree::
   :maxdepth: 1

..    notebooks/GD_ex1.ipynb
..    notebooks/GD_ex2.ipynb
   

   