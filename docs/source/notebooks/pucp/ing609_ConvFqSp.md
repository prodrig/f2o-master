---
jupytext:
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

<!-- .dDown[open] > summary {
  box-shadow: 3px 3px 4px black;
  cursor: pointer;
  list-style: none;}
  
  DarkOliveGreen
  DarkRed
  #283747;  similar (but darker) to
  DarkSlateGray;
-->


<style type="text/css">
<!--
 .tab1 { margin-left: 1em;}
 .tab2 { margin-left: 2em;}
 .dDown[open] > summary {
  color: DarkSlateGray;}
 .dDown > summary {
  color: DarkSlateGray;}
-->
</style>

 
# Convolution in the frequency domain


Let $\mathbf{u} \in \mathbb{R}^{N_r \times N_c}$ and $\mathbf{h} \in \mathbb{R}^{L_r \times L_c}$
represent variables with zero-pad boundary conditions, i.e.

$$\begin{array}{rcl}
u_{n_1,n_2} = \left\{ \begin{array}{lc}
  0 &  n_1,\,n_2 < 0 \\
  u_{n_1,n_2} & \\
  0 & n_1 \geq N_r, \, n_2 \geq N_c
\end{array}\right.
& \; \mbox{ and } \; &
h_{n_1,n_2} = \left\{ \begin{array}{lc}
  0 &  n_1,\,n_2 < 0 \\
  h_{n_1,n_2} & \\
  0 & n_1 \geq L_r, \, n_2 \geq L_c
\end{array}\right.
\end{array}
$$

Then

$\begin{array}{l} \mbox{Linear Convolution} \\ \hline
\begin{array}{rcl}\mathbf{b} & = & \mathbf{u} * \mathbf{h} \\
 b_{k_1,k_2}& = & \displaystyle \sum_{n_1=0}^{L_r} \sum_{n_2=0}^{L_c}  h_{n_1 , n_2} \cdot u_{n_1 - k_1, n_2 - k_2}
\end{array} \\ 
\; \\
\mbox{Circular Convolution} \\ \hline
\begin{array}{rcl}\mathbf{b} & = & \mathbf{u} \circledast \mathbf{h} \\
 \tilde{b}_{k_1,k_2}& = & \displaystyle \sum_{n_1=0}^{L_r} \sum_{n_2=0}^{L_c}  h_{n_1 , n_2} \cdot \tilde{u}_{n_1 - k_1, n_2 - k_2} 
\end{array} \\
\mbox{where} \\
\begin{array}{rcl}
\tilde{u}_{n_1,n_2} & = & u_{(n_1)_{N_r},(n_2)_{N_c}}   \qquad \color{darkgray} \mbox{ circular extension of } \mathbf{u}\\
(a)_{b} & = & \mbox{remainder}(a,b) 
\end{array}
\end{array}$


<div style="line-height:50%;"> <br> </div>

<hr style="border:0.125px solid gray"> </hr> 


## Convolution theorem

Recall that

$$ \begin{array}{rcl}
%
X & = & {\cal{F}}\{ \mathbf{x} \} \\
%
X_{k_1,k_2} & = & \displaystyle \sum_{k_1=0}^{N_r-1}\sum_{k_2=0}^{N_c-1} x_{m_1,m_2} \cdot  W^{m_1\cdot k_1}_{N_r} \cdot W^{m_2\cdot k_2}_{N_c} 
\qquad  \color{darkgray} W^a_b = \mbox{e}^{-j\cdot 2\pi \cdot \frac{a}{b}}
%
\end{array}
$$

and that, by definition,
* ${\cal{F}}\{ \mathbf{x} \} = {\cal{F}}\{ \tilde{\mathbf{x}} \} \qquad
\color{darkgray}(\mbox{equivalent to } \tilde{\mathbf{x}} = {\cal{F}}^{-1}\{ X \})$

then 

$$ {\cal{F}}\{ \mathbf{u} \circledast \mathbf{h} \} = U \odot H $$

where
* ${\cal{F}}\{ \cdot \}$ represents the Fourier transform;
* $H = {\cal{F}}\{ \mathbf{h}, \mbox{shape}= \mathbf{x}\}. \qquad \color{darkgray} \mathbf{h} \mbox{ is zero-padded to match } 
\mathbf{x}\mbox{'s size}$


<!-- ### 1. Load F2O -->
<details>
  <summary><span style="font-size:1.25em;"><strong>1. Proof</strong></span></summary>

```{tabbed} Summary

Let $\mathbf{x} \in \mathbb{R}^{N_r \times N_c}$ and $\mathbf{h} \in \mathbb{R}^{L_r \times L_c}$

* $U = {\cal{F}}\{ \mathbf{u} \}$
* $H = {\cal{F}}\{ \mathbf{h}, \mbox{shape}= \mathbf{x}\}$
* $B = X \odot H$.


The proof is based on {cite:ps}`dudgeon-1984-multidimensional`.

```

```{tabbed} Step 1

$$
%
\begin{array}{rcl}
\mathbf{b} & = & \displaystyle{\cal{F}}^{-1}\{ U \odot H \} \\
  b_{n_1,n_2}& = & \displaystyle\frac{1}{N_r\cdot N_c} \sum_{k_1=0}^{N_r-1}\sum_{k_2=0}^{N_c-1} H_{k_1,k_2} \cdot 
  \bbox[white,4px,border:2px solid white]{U_{k_1,k_2}}
 \cdot W^{\scriptscriptstyle -n_1\cdot k_1}_{N_r} \cdot W^{-n_2\cdot k_2}_{N_c}
 \qquad  \color{darkgray} W^a_b = \mbox{e}^{-j\cdot 2\pi \cdot \frac{a}{b}} \\
% 
\end{array}
$$

$\color{white}\mbox{Considering }  U_{k_1,k_2} = 
 \bbox[white,6px,border:2px solid white]{ \displaystyle 
 \bbox[white,4px,border:2px solid white]{ \sum_{k_1=0}^{N_r-1}\sum_{k_2=0}^{N_c-1} 
  \tilde{u}_{m_1,m_2} } \cdot \bbox[white,4px,border:2px solid white]{ W^{m_1\cdot k_1}_{N_r} \cdot W^{m_2\cdot k_2}_{N_c} } \; }$
  
$\color{white}\therefore$
  
$$\color{white}\begin{array}{rcl}
  b_{n_1,n_2} & = &  \displaystyle\sum_{k_1=0}^{N_r-1}\sum_{k_2=0}^{N_c-1} 
  \tilde{u}_{m_1,m_2} \cdot \bbox[white,4px,border:2px solid white]{ \displaystyle\frac{1}{N_r\cdot N_c} \sum_{k_1=0}^{N_r-1}\sum_{k_2=0}^{N_c-1} 
  H_{k_1,k_2} \cdot W^{(m_1-n_1)\cdot k_1}_{N_r} \cdot W^{(m_2-n_2)\cdot k_2}_{N_c} }\\
%
  & = & \displaystyle\sum_{k_1=0}^{N_r-1}\sum_{k_2=0}^{N_c-1} \tilde{u}_{m_1,m_2} \cdot  \tilde{h}_{n_1 - m_1, n_2 - m_2} \\
%
  & = & h_{n_1,n_2}  \circledast u_{n_1,n_2}  
\end{array}
$$


```

```{tabbed} Step 2

$$
%
\begin{array}{rcl}
\mathbf{b} & = & \displaystyle{\cal{F}}^{-1}\{ U \odot H \} \\
  b_{n_1,n_2}& = & \displaystyle\frac{1}{N_r\cdot N_c} \sum_{k_1=0}^{N_r-1}\sum_{k_2=0}^{N_c-1} H_{k_1,k_2} \cdot 
  \bbox[white,4px,border:2px solid green]{U_{k_1,k_2}}
 \cdot W^{\scriptscriptstyle -n_1\cdot k_1}_{N_r} \cdot W^{-n_2\cdot k_2}_{N_c}
 \qquad  \color{darkgray} W^a_b = \mbox{e}^{-j\cdot 2\pi \cdot \frac{a}{b}} \\
% 
\end{array}
$$

$\mbox{Considering }  U_{k_1,k_2} = 
 \bbox[white,6px,border:2px solid green]{ \displaystyle 
 \bbox[white,4px,border:2px solid lightblue]{ \sum_{k_1=0}^{N_r-1}\sum_{k_2=0}^{N_c-1} 
  \tilde{u}_{m_1,m_2} } \cdot \bbox[white,4px,border:2px solid red]{ W^{m_1\cdot k_1}_{N_r} \cdot W^{m_2\cdot k_2}_{N_c} } \; }$
  
$\color{white}\therefore$
  
$$\color{white}\begin{array}{rcl}
  b_{n_1,n_2} & = &  \displaystyle\sum_{k_1=0}^{N_r-1}\sum_{k_2=0}^{N_c-1} 
  \tilde{u}_{m_1,m_2} \cdot \bbox[white,4px,border:2px solid white]{ \displaystyle\frac{1}{N_r\cdot N_c} \sum_{k_1=0}^{N_r-1}\sum_{k_2=0}^{N_c-1} 
  H_{k_1,k_2} \cdot W^{(m_1-n_1)\cdot k_1}_{N_r} \cdot W^{(m_2-n_2)\cdot k_2}_{N_c} }\\
%
  & = & \displaystyle\sum_{k_1=0}^{N_r-1}\sum_{k_2=0}^{N_c-1} \tilde{u}_{m_1,m_2} \cdot  \tilde{h}_{n_1 - m_1, n_2 - m_2} \\
%
  & = & h_{n_1,n_2}  \circledast x_{n_1,n_2}  
\end{array}
$$

```


```{tabbed} Step 3

$$
% 
\begin{array}{rcl}
\mathbf{b} & = & \displaystyle{\cal{F}}^{-1}\{ U \odot H \} \\
  b_{n_1,n_2}& = & \displaystyle\kern-0.5em\bbox[lightblue,4px]{^{\;}}\kern-0.22em\frac{1}{N_r\cdot N_c} \sum_{k_1=0}^{N_r-1}\sum_{k_2=0}^{N_c-1} H_{k_1,k_2} \cdot 
  \bbox[white,4px,border:2px solid white]{\phantom{U_{k_1,k_2}}\kern-1.625em \bbox[red,4px]{^{\;\;}}\;\;}
 \cdot W^{\scriptscriptstyle -n_1\cdot k_1}_{N_r} \cdot W^{-n_2\cdot k_2}_{N_c}
 \qquad  \color{darkgray} W^a_b = \mbox{e}^{-j\cdot 2\pi \cdot \frac{a}{b}} \\
% 
\end{array}
$$

$\mbox{Considering }  U_{k_1,k_2} = 
 \bbox[white,6px,border:2px solid green]{ \displaystyle 
 \bbox[white,4px,border:2px solid lightblue]{ \sum_{k_1=0}^{N_r-1}\sum_{k_2=0}^{N_c-1} 
  \tilde{u}_{m_1,m_2} } \cdot \bbox[white,4px,border:2px solid red]{ W^{m_1\cdot k_1}_{N_r} \cdot W^{m_2\cdot k_2}_{N_c} } \; }$
  
$\therefore$
  
$$\begin{array}{rcl}
  b_{n_1,n_2} & = &  \displaystyle\sum_{k_1=0}^{N_r-1}\sum_{k_2=0}^{N_c-1} 
  \tilde{u}_{m_1,m_2} \cdot \bbox[white,4px,border:2px solid white]{ \displaystyle\frac{1}{N_r\cdot N_c} \sum_{k_1=0}^{N_r-1}\sum_{k_2=0}^{N_c-1} 
  H_{k_1,k_2} \cdot W^{(m_1-n_1)\cdot k_1}_{N_r} \cdot W^{(m_2-n_2)\cdot k_2}_{N_c} }\\
%
  & \color{white}= & \color{white}\displaystyle\sum_{k_1=0}^{N_r-1}\sum_{k_2=0}^{N_c-1} \tilde{u}_{m_1,m_2} \cdot  \tilde{h}_{n_1 - m_1, n_2 - m_2} \\
%
  & \color{white}= & \color{white}h_{n_1,n_2}  \circledast x_{n_1,n_2}
\end{array}
$$

```

```{tabbed} Step 4

$$
%
\begin{array}{rcl}
\mathbf{b} & = & \displaystyle{\cal{F}}^{-1}\{ U \odot H \} \\
  b_{n_1,n_2}& = & \displaystyle\frac{1}{N_r\cdot N_c} \sum_{k_1=0}^{N_r-1}\sum_{k_2=0}^{N_c-1} H_{k_1,k_2} \cdot 
  \bbox[white,4px,border:2px solid white]{ U_{k_1,k_2} }
 \cdot W^{\scriptscriptstyle -n_1\cdot k_1}_{N_r} \cdot W^{-n_2\cdot k_2}_{N_c}
 \qquad  \color{darkgray} W^a_b = \mbox{e}^{-j\cdot 2\pi \cdot \frac{a}{b}} \\
% 
\end{array}
$$

$\mbox{Considering }  U_{k_1,k_2} = 
 \bbox[white,6px,border:2px solid white]{ \displaystyle 
 \bbox[white,4px,border:2px solid white]{ \sum_{k_1=0}^{N_r-1}\sum_{k_2=0}^{N_c-1} 
  \tilde{u}_{m_1,m_2} } \cdot \bbox[white,4px,border:2px solid white]{ W^{m_1\cdot k_1}_{N_r} \cdot W^{m_2\cdot k_2}_{N_c} } \; }$
  
$\therefore$
  
$$\begin{array}{rcl}
  b_{n_1,n_2} & = &  \displaystyle\sum_{k_1=0}^{N_r-1}\sum_{k_2=0}^{N_c-1} 
  \tilde{u}_{m_1,m_2} \cdot \bbox[white,4px,border:2px solid green]{ \displaystyle\frac{1}{N_r\cdot N_c} \sum_{k_1=0}^{N_r-1}\sum_{k_2=0}^{N_c-1} 
  H_{k_1,k_2} \cdot W^{(m_1-n_1)\cdot k_1}_{N_r} \cdot W^{(m_2-n_2)\cdot k_2}_{N_c} }\\
%
  & = & \displaystyle\sum_{k_1=0}^{N_r-1}\sum_{k_2=0}^{N_c-1} \tilde{u}_{m_1,m_2} \cdot  \tilde{h}_{n_1 - m_1, n_2 - m_2} \\
%
  & = & h_{n_1,n_2}  \circledast x_{n_1,n_2}
\end{array}
$$

```

  
</details>

<!-- ### 2. Load test images (SIPI Image database) -->
<div style="line-height:50%;"> <br> </div>
<details>
  <summary><span style="font-size:1.25em;"><strong>2. Additional comments </strong></span></summary>

* $\tilde{\mathbf{h}}$ is the circular extension of the zero-padded version of $\mathbf{h}$
* To be consintant, $b_{n_1,n_2}$ should be replaced by $\tilde{b}_{n_1,n_2}$ to emphasize
that it represents a circular extended vector.
</details>

<div style="line-height:50%;"> <br> </div>

<hr style="border:0.125px solid gray"> </hr> 


## Matrix extensions: zero-pad, circurlar and symmetric

The result of convolving image (matrix) $\mathbf{u}$ with a filter $\mathbf{h}$ is influenced
by the boundary conditions of the former; this is specially important for deconvolution
(for instance, see {cite:ps}`serra-capizzano-2004-note`).


The collapsible items below show the visual effect of three different boundary conditions:

```{code-cell} ipython3
---
tags: [remove-input]
---
%load_ext tikzmagic

preamble = '''
    \definecolor{OliveGreen}{RGB}{60,128,49}
    \definecolor{BrickRed}{RGB}{182,50,28}
    \pgfplotsset{compat=1.16}
'''

```

<!-- ### 1 -->
<div style="line-height:50%;"> <br> </div>
<details>
  <summary><span style="font-size:1.0em;"><strong>1. Zero-pad</strong></span></summary>

```{code-cell} ipython3
---
tags: [remove-input]
---

%%tikz -s 800,400 -x $preamble -p pgfplots,tikz-3dplot -l arrows,matrix,fit,calc,hobby,3d -f png


\tdplotsetmaincoords{75}{-40}

\begin{scope}[tdplot_main_coords, canvas is xz plane at y=0,transform shape]

\node[anchor=south west] (img0) at (0.25,0.25) {\includegraphics[width=5cm]{imgs/gField-0.png}};

\foreach \ii [count = \xi] in {0,1,2,...,25}{
  \foreach \jj  [count = \yi]in {0,1,2,...,25}{
     
      \pgfmathsetmacro{\nn}{int(\xi-3+20*(\yi-3))}

      \pgfmathparse{ int(\xi>=3)*int(\xi<=22)*int(\yi>=3)*int(\yi<=22)}
      \ifnum0=\pgfmathresult\relax
        \node[white,draw,minimum size=.25cm,opacity=0.0] at ($(0.25*\ii,0.25*\jj)$) {};
      \else
        \node[blue!50,draw,minimum size=.25cm,opacity=0.0] (A\nn) at ($(0.25*\ii,0.25*\jj)$) {};
      \fi
     
     
  }
}

\node[anchor=south west] at ($(img0)+(2.375,-2.625)$) {\includegraphics[width=5cm]{imgs/gField-black.png}};
\node[anchor=south west] at ($(img0)+(-7.625,-2.625)$) {\includegraphics[width=5cm]{imgs/gField-black.png}};

\node[anchor=south west] (c0) at ($(img0)+(-2.625,2.375)$) {\includegraphics[width=5cm]{imgs/gField-black.png}};
\node[anchor=south west] (c1) at ($(img0)+(-2.625,-7.63)$) {\includegraphics[width=5cm]{imgs/gField-black.png}};

\node[anchor=south west] at ($(c0)+(2.375,-2.625)$) {\includegraphics[width=5cm]{imgs/gField-black.png}};
\node[anchor=south west] at ($(c0)+(-7.625,-2.625)$) {\includegraphics[width=5cm]{imgs/gField-black.png}};

\node[anchor=south west] at ($(c1)+(2.375,-2.62)$) {\includegraphics[width=5cm]{imgs/gField-black.png}};
\node[anchor=south west] at ($(c1)+(-7.625,-2.62)$) {\includegraphics[width=5cm]{imgs/gField-black.png}};


\node[anchor=south west] (orig) at ($(img0)+(-19,0)$) {\includegraphics[width=5cm]{imgs/gField-0.png}};

\draw[BrickRed,ultra thick,opacity=0.75] (A0.south west) to (A19.south east) to 
                                   (A399.north east) to (A380.north west) to  (A0.south west) ;

\node[black] at ($(orig.south)+(0,-0.5)$) {\Large Original};    
\node[black] at ($(c1.south)+(0,-0.5)$) {\Large Zero-pad};    
    
\end{scope}
```
  
</details>



<!-- ### 2 -->
<div style="line-height:50%;"> <br> </div>
<details>
  <summary><span style="font-size:1.0em;"><strong>2. Circular extension</strong></span></summary>

```{code-cell} ipython3
---
tags: [remove-input]
---

%%tikz -s 800,400 -x $preamble -p pgfplots,tikz-3dplot -l arrows,matrix,fit,calc,hobby,3d -f png


\tdplotsetmaincoords{75}{-40}

\begin{scope}[tdplot_main_coords, canvas is xz plane at y=0,transform shape]

\node[anchor=south west] (img0) at (0.25,0.25) {\includegraphics[width=5cm]{imgs/gField-0.png}};

\foreach \ii [count = \xi] in {0,1,2,...,25}{
  \foreach \jj  [count = \yi]in {0,1,2,...,25}{
     
      \pgfmathsetmacro{\nn}{int(\xi-3+20*(\yi-3))}

      \pgfmathparse{ int(\xi>=3)*int(\xi<=22)*int(\yi>=3)*int(\yi<=22)}
      \ifnum0=\pgfmathresult\relax
        \node[white,draw,minimum size=.25cm,opacity=0.0] at ($(0.25*\ii,0.25*\jj)$) {};
      \else
        \node[blue!50,draw,minimum size=.25cm,opacity=0.0] (A\nn) at ($(0.25*\ii,0.25*\jj)$) {};
      \fi
     
     
  }
}

\node[anchor=south west] at ($(img0)+(2.375,-2.625)$) {\includegraphics[width=5cm]{imgs/gField-0.png}};
\node[anchor=south west] at ($(img0)+(-7.625,-2.625)$) {\includegraphics[width=5cm]{imgs/gField-0.png}};

\node[anchor=south west] (c0) at ($(img0)+(-2.625,2.375)$) {\includegraphics[width=5cm]{imgs/gField-0.png}};
\node[anchor=south west] (c1) at ($(img0)+(-2.625,-7.63)$) {\includegraphics[width=5cm]{imgs/gField-0.png}};

\node[anchor=south west] at ($(c0)+(2.375,-2.625)$) {\includegraphics[width=5cm]{imgs/gField-0.png}};
\node[anchor=south west] at ($(c0)+(-7.625,-2.625)$) {\includegraphics[width=5cm]{imgs/gField-0.png}};

\node[anchor=south west] at ($(c1)+(2.375,-2.62)$) {\includegraphics[width=5cm]{imgs/gField-0.png}};
\node[anchor=south west] at ($(c1)+(-7.625,-2.62)$) {\includegraphics[width=5cm]{imgs/gField-0.png}};


\node[anchor=south west] (orig) at ($(img0)+(-19,0)$) {\includegraphics[width=5cm]{imgs/gField-0.png}};

\draw[BrickRed,ultra thick,opacity=0.75] (A0.south west) to (A19.south east) to 
                                   (A399.north east) to (A380.north west) to  (A0.south west) ;

\node[black] at ($(orig.south)+(0,-0.5)$) {\Large Original};    
\node[black] at ($(c1.south)+(0,-0.5)$) {\Large Circular extension};    
    
\end{scope}
```
</details>

<!-- ### 3 -->
<div style="line-height:50%;"> <br> </div>
<details>
  <summary><span style="font-size:1.0em;"><strong>3. Symmetric extension  </strong></span></summary>

```{code-cell} ipython3
---
tags: [remove-input]
---

%%tikz -s 800,400 -x $preamble -p pgfplots,tikz-3dplot -l arrows,matrix,fit,calc,hobby,3d -f png


\tdplotsetmaincoords{75}{-40}

\begin{scope}[tdplot_main_coords, canvas is xz plane at y=0,transform shape]

\node[anchor=south west] (img0) at (0.25,0.25) {\includegraphics[width=5cm]{imgs/gField-0.png}};

\foreach \ii [count = \xi] in {0,1,2,...,25}{
  \foreach \jj  [count = \yi]in {0,1,2,...,25}{
     
      \pgfmathsetmacro{\nn}{int(\xi-3+20*(\yi-3))}

      \pgfmathparse{ int(\xi>=3)*int(\xi<=22)*int(\yi>=3)*int(\yi<=22)}
      \ifnum0=\pgfmathresult\relax
        \node[white,draw,minimum size=.25cm,opacity=0.0] at ($(0.25*\ii,0.25*\jj)$) {};
      \else
        \node[blue!50,draw,minimum size=.25cm,opacity=0.0] (A\nn) at ($(0.25*\ii,0.25*\jj)$) {};
      \fi
     
     
  }
}

\node[anchor=south west] at ($(img0)+(2.375,-2.625)$) {\includegraphics[width=5cm]{imgs/gField-flipH.png}};
\node[anchor=south west] at ($(img0)+(-7.625,-2.625)$) {\includegraphics[width=5cm]{imgs/gField-flipH.png}};

\node[anchor=south west] (c0) at ($(img0)+(-2.625,2.375)$) {\includegraphics[width=5cm]{imgs/gField-flipV.png}};
\node[anchor=south west] (c1) at ($(img0)+(-2.625,-7.63)$) {\includegraphics[width=5cm]{imgs/gField-flipV.png}};

\node[anchor=south west] at ($(c0)+(2.375,-2.625)$) {\includegraphics[width=5cm]{imgs/gField-r180.png}};
\node[anchor=south west] at ($(c0)+(-7.625,-2.625)$) {\includegraphics[width=5cm]{imgs/gField-r180.png}};

\node[anchor=south west] at ($(c1)+(2.375,-2.62)$) {\includegraphics[width=5cm]{imgs/gField-r180flipV.png}};
\node[anchor=south west] at ($(c1)+(-7.625,-2.62)$) {\includegraphics[width=5cm]{imgs/gField-r180flipV.png}};


\node[anchor=south west] (orig) at ($(img0)+(-19,0)$) {\includegraphics[width=5cm]{imgs/gField-0.png}};

\draw[BrickRed,ultra thick,opacity=0.75] (A0.south west) to (A19.south east) to 
                                   (A399.north east) to (A380.north west) to  (A0.south west) ;

\node[black] at ($(orig.south)+(0,-0.5)$) {\Large Original};    
\node[black] at ($(c1.south)+(0,-0.5)$) {\Large Symmetric extension};    
    
\end{scope}
```
</details>

<div style="line-height:50%;"> <br> </div>

<hr style="border:0.125px solid gray"> </hr> 


## Spatial and Fourier based convolution: Matching their results


In order to match the computation of the convolution of image $\mathbf{u} \in \mathbb{R}^{N_r \times N_c}$ and 
filter $\mathbf{h} \in \mathbb{R}^{L_r \times L_c}$ in the spatial domain

$$ \mathbf{b} = \mathbf{u} * \mathbf{h}, $$

and in the Fourier domain, 
the convolution theorem must be taken into account; the Fourier-based
computation of the convolution can be broken down into three steps:

<!-- ### 1 -->
<!-- <div style="line-height:50%;"> <br> </div> -->
<details open>
  <summary><span style="font-size:1.0em;"><strong>1. Pre-processing  </strong></span></summary>

The input is extended / zero-padded depending on the boundary conditions; the filter
is zero-pad to match the size of the pre-processed image.

```{code-cell} ipython3
---
tags: [remove-input]
---


%%tikz -s 800,400 -x $preamble -p pgfplots,tikz-3dplot,amsfonts -l arrows,matrix,fit,calc,hobby,3d -f png

\tikzset{every picture/.style={remember picture}}

\tdplotsetmaincoords{75}{-40}


\begin{scope}[tdplot_main_coords, canvas is xz plane at y=0,transform shape, scale=1.25]

\foreach \ii [count = \xi] in {0,1,2,...,28}{
  \foreach \jj  [count = \yi]in {0,1,2,...,28}{
     
      \pgfmathsetmacro{\nn}{int(\xi-4+20*(\yi-7))}
      \pgfmathsetmacro{\mm}{int(\xi-1+29*(\yi-1))}

      \pgfmathparse{ int(\xi>=4)*int(\xi<=23)*int(\yi>=7)*int(\yi<=26)}
      \ifnum0=\pgfmathresult
      
        \pgfmathparse{ int(\xi>=1)*int(\yi>=4)*int(\yi<=29)*int(\xi<=26)}
        \ifnum0=\pgfmathresult
            \node[fill=black,minimum size=.25cm,opacity=0.2] (B\mm) at ($(0.25*\ii,0.25*\jj)$) {};
            
         \else
            \node[fill=OliveGreen,minimum size=.25cm,opacity=0.1] (B\mm) at ($(0.25*\ii,0.25*\jj)$) {};
         \fi
       
        \pgfmathparse{ int(\xi>=1)*int(\yi>=1)*int(\yi<=29)*int(\xi<=29)}
      
      \else
        \node[blue!50,draw,minimum size=.25cm] (A\nn) at ($(0.25*\ii,0.25*\jj)$) {};
      \fi
     
% ---------

      \pgfmathsetmacro{\nn}{int(\xi-1+20*(\yi-10))}
      \pgfmathsetmacro{\mm}{int(\xi-1+29*(\yi-1))}

      \pgfmathparse{ int(\xi>=1)*int(\xi<=6)*int(\yi>=24)*int(\yi<=29)}
      \ifnum0=\pgfmathresult      
      
        \node[fill=black,minimum size=.25cm,opacity=0.2] (F\mm) at ($(10.0,-1.35)+(0.25*\ii,0.25*\jj)$) {};                   
           
      \else
        \node[blue!50,draw,minimum size=.25cm] (E\nn) at ($(10.0,-1.35)+(0.25*\ii,0.25*\jj)$) {};
      \fi
     

  }
}

\node[blue] at ($(A390.north)+(0,0.25)$) {\large$\mathbf{u}$};

\node[OliveGreen] at ($(A399.north)+(0.35,0.35)$) {\large$\mathbf{u}_{BC}$};

\node[black] at ($(A399.north)+(1.25,1.05)$) {\large$\mathbf{u}_{ZP}$};


\draw[blue,thick,opacity=0.75] (A0.south west) to (A19.south east) to 
                               (A399.north east) to (A380.north west) to  (A0.south west) ;
\draw[OliveGreen,thick,opacity=0.75] (B87.south west) to (B112.south east) to 
                               (B837.north east) to (B812.north west) to  (B87.south west) ;
\draw[black,thick,opacity=0.75] (B0.south west) to (B28.south east) to 
                               (B840.north east) to (B812.north west) to  (B0.south west) ;

% ---

\node[blue] at ($(E382.north)+(0,0.35)$) {\large$\mathbf{h}$};

\node[black] at ($(F825.north)+(0.0,0.35)$) {\large$\mathbf{h}_{ZP}$};


\draw[blue,thick,opacity=0.75] (E280.south west) to (E285.south east) to 
                               (E385.north east) to (E380.north west) to  (E280.south west) ;
\draw[black,thick,opacity=0.75] (F0.south west) to (F28.south east) to 
                               (F840.north east) to (E380.north west) to  (F0.south west) ;

                               
% ================================
% ruler x                               
\draw[|-|, BrickRed] ($(B0.south west) + (0.0,-0.15)$) --  ($(B2.south east) + (0.0,-0.15)$) ;
\draw[|-|, BrickRed] ($(B3.south west) + (0.0,-0.15)$) --  ($(B22.south east) + (0.0,-0.15)$) ;
\draw[|-|, BrickRed] ($(B23.south west) + (0.0,-0.15)$) --  ($(B25.south east) + (0.0,-0.15)$) ;
\draw[|-|, BrickRed] ($(B26.south west) + (0.0,-0.15)$) --  ($(B28.south east) + (0.0,-0.15)$) ;

\node[BrickRed] at ($(B1)+(0.0,-0.75)$) {$L_c^{(0)}$};
\node[BrickRed] at ($(B13)+(0.0,-0.75)$) {$N_c$};
\node[BrickRed] at ($(B24)+(0.0,-0.75)$) {$L_c^{(1)}$};
\node[BrickRed] at ($(B27)+(0.0,-0.75)$) {$L_c\mbox{-}1$};

% ---

\draw[|-|, BrickRed] ($(F0.south west) + (0.0,-0.15)$) --  ($(F5.south east) + (0.0,-0.15)$) ;
\draw[|-|, BrickRed] ($(F6.south west) + (0.0,-0.15)$) --  ($(F28.south east) + (0.0,-0.15)$) ;

\node[BrickRed] at ($(F2)+(0.0,-0.75)$) {$L_c$};
\node[BrickRed] at ($(F19)+(0.0,-0.75)$) {$N_c+L_c^{(0)}+L_c^{(1)}+L_c-1$};


% ================================
% ruler y
\draw[|-|, BrickRed] ($(B0.south west) + (-0.25,0.0)$) --  ($(B58.north west) + (-0.25,0.0)$) ;
\draw[|-|, BrickRed] ($(B87.south west) + (-0.25,0.0)$) --  ($(B145.north west) + (-0.25,0.0)$) ;
\draw[|-|, BrickRed] ($(B174.south west) + (-0.25,0.0)$) --  ($(B725.north west) + (-0.25,0.0)$) ;
\draw[|-|, BrickRed] ($(B754.south west) + (-0.25,0.0)$) --  ($(B812.north west) + (-0.25,0.0)$) ;

\node[BrickRed] at ($(B29)+(-1.0,0.0)$) {$L_r\mbox{-}1$};
\node[BrickRed] at ($(B116)+(-1.0,0.0)$) {$L_r^{(1)}$};
\node[BrickRed] at ($(B464)+(-1.0,0.0)$) {$N_r$};
\node[BrickRed] at ($(B783)+(-1.0,0.0)$) {$L_r^{(0)}$};

% ---

\draw[|-|, BrickRed] ($(F0.south west) + (-0.25,0.0)$) --  ($(F638.north west) + (-0.25,0.0)$) ;
\draw[|-|, BrickRed] ($(E280.south west) + (-0.25,0.0)$) --  ($(E380.north west) + (-0.25,0.0)$) ;

\node[BrickRed] at ($(E340)+(-1.0,0.0)$) {$L_r$};

\node[BrickRed] at ($(F348)+(-1.0,0.0)$) {$\begin{array}{c}
N_r \\ + \\ L_r^{(0)} \\ + \\ L_r^{(1)} \\ + \\ L_r \\ - \\ 1
\end{array}$};

\end{scope}


\node[black, right of=] at ($(B8)+(0,-2.85)$) {\Large$\begin{array}{cl}
 \bullet & \mathbf{u}_{BC} = {\cal{P}}_{BC}\{ \mathbf{u} \} \qquad \mbox{ \color{darkgray} boundary conditon} \\ [5pt]
 \bullet & \mathbf{u}_{ZP} = {\cal{P}}_{ZP}\{ \mathbf{u}_{BC} \} \quad \mbox{ \color{darkgray} zero-pad} \\ [5pt] 
 \bullet & L_n^{(0)},  L_n^{(1)}, n \in \{r,\,c\}  \mbox{ : depend on B.C.} \\ [5pt]
\end{array}$};


\node[black, right of=] at ($(F8)+(0,-2.55)$) {\Large$\begin{array}{cl}
 \bullet & \mathbf{h}_{ZP} = {\cal{P}}_{ZP}\{ \mathbf{h}, \mbox{shape} = \mathbf{u}_{ZP} \} \\ [5pt]
\end{array}$};


%\node[black] at ($(B579)+(5.0,0)$) {\large$\begin{array}{cl}
% 
%\bullet & \mathbf{u}_{BC} = {\cal{P}}_{BC}\{ \mathbf{u} \} \qquad \mbox{ \color{darkgray} boundary conditon} \\ [5pt]
% 
%\bullet & \mathbf{u}_{ZP} = {\cal{P}}_{ZP}\{ \mathbf{u}_{BC} \} \quad \mbox{ \color{darkgray} zero-pad} \\ [5pt]
% 
%\bullet & \mathbf{h}_{ZP} = {\cal{P}}_{ZP}\{ \mathbf{h}, \mbox{shape} = \mathbf{u}_{ZP} \}  \\ [5pt]
% 
%\bullet & L_n^{(0)},  L_n^{(1)}, n \in \{1,\,2\}  \mbox{ : depend on B.C.} \\ [5pt]
%\bullet & L_n^{(0)} = \lfloor \frac{L_n - ( L_n (\mbox{mod } 2) )}{2} \rfloor\\
%
%\bullet & L_n^{(1)} = L_n - L_n^{(0)} - 1
%\end{array}$};

``` 

<div style="margin-top:-1.0em;"></div>
<p style="text-align: center;"><a name="fig-convFPre">&nbsp;</a> Figure 1.a Preprocessing </p>

</details>

<!-- ### 2 -->
<div style="line-height:50%;"> <br> </div>
<details open>
  <summary><span style="font-size:1.0em;"><strong>2. Product in the Fourier domain  </strong></span></summary>

This step includes the computation of the forward Fourier transform of the 
  pre-processed versions of the input and filter, and the inverse
  Fourier tranform of the Hadamard product between them.
  
* $ U_F  = {\cal{F}}\{ \mathbf{u}_{ZP} \}$
  <div style="margin-top:0.35em;"></div>

* $ H_F  = {\cal{F}}\{ \mathbf{h}_{ZP} \}$
  <div style="margin-top:0.35em;"></div>

$$ \begin{eqnarray*}
  \mathbf{b}_{BC} & = &  {\cal{F}}^{-1}\{ U_F \odot H_F \} 
\end{eqnarray*}
$$

</details>

<!-- ### 3 -->
<div style="line-height:50%;"> <br> </div>
<details open>
  <summary><span style="font-size:1.0em;"><strong>3. Post-processing  </strong></span></summary>
 
In order to match the output of the spatial convolution, the previous result must be cropped out.


```{code-cell} ipython3
---
tags: [remove-input]
---


%%tikz -s 800,400 -x $preamble -p pgfplots,tikz-3dplot,amsfonts -l arrows,matrix,fit,calc,hobby,3d -f png

\tikzset{every picture/.style={remember picture}}

\tdplotsetmaincoords{75}{-40}


\begin{scope}[tdplot_main_coords, canvas is xz plane at y=0,transform shape]

\foreach \ii [count = \xi] in {0,1,2,...,25}{
  \foreach \jj  [count = \yi]in {0,1,2,...,25}{
     
      \pgfmathsetmacro{\nn}{int(\xi-4+20*(\yi-4))}
      \pgfmathsetmacro{\mm}{int(\xi-1+26*(\yi-1))}

      \pgfmathparse{ int(\xi>=4)*int(\xi<=23)*int(\yi>=4)*int(\yi<=23)}
      \ifnum0=\pgfmathresult      
        \node[fill=OliveGreen,minimum size=.25cm,opacity=0.1] (B\mm) at ($(0.25*\ii,0.25*\jj)$) {};             
      \else
        \node[blue!50,draw,minimum size=.25cm] (A\nn) at ($(0.25*\ii,0.25*\jj)$) {};
      \fi
     
     
  }
}

\node[blue] at ($(A390.north)+(0,0.25)$) {\large$\mathbf{b}$};

\node[OliveGreen] at ($(A399.north)+(0.35,0.35)$) {\large$\mathbf{b}_{BC}$};



\draw[blue,thick,opacity=0.75] (A0.south west) to (A19.south east) to 
                               (A399.north east) to (A380.north west) to  (A0.south west) ;
\draw[OliveGreen,thick,opacity=0.75] (B0.south west) to (B25.south east) to 
                               (B675.north east) to (B650.north west) to  (B0.south west) ;


% ruler x                               
\draw[|-|, BrickRed] ($(B0.south west) + (0.0,-0.15)$) --  ($(B2.south east) + (0.0,-0.15)$) ;
\draw[|-|, BrickRed] ($(B3.south west) + (0.0,-0.15)$) --  ($(B22.south east) + (0.0,-0.15)$) ;
\draw[|-|, BrickRed] ($(B23.south west) + (0.0,-0.15)$) --  ($(B25.south east) + (0.0,-0.15)$) ;

\node[BrickRed] at ($(B1)+(0.0,-0.75)$) {$L_c^{(0)}$};
\node[BrickRed] at ($(B13)+(0.0,-0.75)$) {$N_c$};
\node[BrickRed] at ($(B24)+(0.0,-0.75)$) {$L_c^{(1)}$};

% ruler y
\draw[|-|, BrickRed] ($(B0.south west) + (-0.25,0.0)$) --  ($(B52.north west) + (-0.25,0.0)$) ;
\draw[|-|, BrickRed] ($(B78.south west) + (-0.25,0.0)$) --  ($(B572.north west) + (-0.25,0.0)$) ;
\draw[|-|, BrickRed] ($(B598.south west) + (-0.25,0.0)$) --  ($(B650.north west) + (-0.25,0.0)$) ;

\node[BrickRed] at ($(B26)+(-1.0,0.0)$) {$L_c^{(1)}$};
\node[BrickRed] at ($(B338)+(-1.0,0.0)$) {$N_c$};
\node[BrickRed] at ($(B624)+(-1.0,0.0)$) {$L_c^{(0)}$};


\end{scope}

\node[black] at ($(B519)+(5.0,0)$) {\large$\begin{array}{cl}
\bullet & \mathbf{b} = {\cal{P}}^{-1}\{ \mathbf{b}_{BC} \} \\ [5pt]
\bullet & L_n^{(0)},  L_n^{(1)}  , n \in \{r,\,c\} \mbox{ : depend on B.C.} \\ [5pt]
\end{array}$};

``` 

<div style="margin-top:-2.0em;"></div>
<p style="text-align: center;"><a name="fig-convFPost">&nbsp;</a>Figure 1.b Postprocessing</p>

</details>



The collapsible items below include details and simulations associated with three particular
boundary conditions usually found when using the convolution operator:


  

<!-- ### 1 -->
<!-- <div style="line-height:50%;"> <br> </div> -->
<details>
  <summary><span style="font-size:1.0em;"><strong>Linear convolution  </strong></span></summary>

  
```{code-cell} ipython3
---
tags: [remove-input]
---


%%tikz -s 800,400 -x $preamble -p pgfplots,tikz-3dplot,amsfonts -l arrows,matrix,fit,calc,hobby,3d -f png

\tikzset{every picture/.style={remember picture}}

\tdplotsetmaincoords{75}{-40}


\begin{scope}[tdplot_main_coords, canvas is xz plane at y=0,transform shape]

\foreach \ii [count = \xi] in {0,1,2,...,25}{
  \foreach \jj  [count = \yi]in {0,1,2,...,25}{
     
      \pgfmathsetmacro{\nn}{int(\xi-1+20*(\yi-7))}
      \pgfmathsetmacro{\mm}{int(\xi-1+26*(\yi-1))}

      \pgfmathparse{ int(\xi>=1)*int(\xi<=20)*int(\yi>=7)*int(\yi<=26)}
      \ifnum0=\pgfmathresult      
      
           \node[fill=black,minimum size=.25cm,opacity=0.2] (B\mm) at ($(0.25*\ii,0.25*\jj)$) {};                   
           
      \else
        \node[blue!50,draw,minimum size=.25cm] (A\nn) at ($(0.25*\ii,0.25*\jj)$) {};
      \fi
     
% -------------

      \pgfmathsetmacro{\mm}{int(\xi-1+26*(\yi-1))}

      \pgfmathparse{ int(\xi>=1)*int(\xi<=6)*int(\yi>=21)*int(\yi<=26)}
      \ifnum0=\pgfmathresult      
      
        \node[fill=black,minimum size=.25cm,opacity=0.2] (F\mm) at ($(8.5,-1.35)+(0.25*\ii,0.25*\jj)$) {};                   
           
      \else
        \node[blue!50,draw,minimum size=.25cm] (E\nn) at ($(8.5,-1.35)+(0.25*\ii,0.25*\jj)$) {};
      \fi
     
% -------------

      \pgfmathsetmacro{\kk}{int(\xi-4+20*(\yi-4))}
      \pgfmathsetmacro{\ll}{int(\xi-1+26*(\yi-1))}

      \pgfmathparse{ int(\xi>=4)*int(\xi<=23)*int(\yi>=4)*int(\yi<=23)}
      \ifnum0=\pgfmathresult      
        \node[fill=OliveGreen,minimum size=.25cm,opacity=0.1] (D\ll) at ($(17.0,-2.7)+(0.25*\ii,0.25*\jj)$) {};             
      \else
        \node[blue!50,draw,minimum size=.25cm] (C\kk) at ($(17.0,-2.7)+(0.25*\ii,0.25*\jj)$) {};
      \fi

     
  }
}

\node[blue] at ($(A390.north)+(0,0.35)$) {\large$\mathbf{u}$};

\node[black] at ($(B670.north)+(0.75,0.35)$) {\large$\mathbf{u}_{ZP}$};


\draw[blue,thick,opacity=0.75] (A0.south west) to (A19.south east) to 
                               (A399.north east) to (A380.north west) to  (A0.south west) ;
\draw[black,thick,opacity=0.75] (B0.south west) to (B25.south east) to 
                               (B675.north east) to (A380.north west) to  (B0.south west) ;

% ---

\node[blue] at ($(E382.north)+(0,0.35)$) {\large$\mathbf{h}$};

\node[black] at ($(F665.north)+(0.0,0.35)$) {\large$\mathbf{h}_{ZP}$};


\draw[blue,thick,opacity=0.75] (E280.south west) to (E285.south east) to 
                               (E385.north east) to (E380.north west) to  (E280.south west) ;
\draw[black,thick,opacity=0.75] (F0.south west) to (F25.south east) to 
                               (F675.north east) to (E380.north west) to  (F0.south west) ;

% ---

\node[blue] at ($(C390.north)+(0,0.35)$) {\large$\mathbf{b}$};

\node[OliveGreen] at ($(C399.north)+(0.35,0.35)$) {\large$\mathbf{b}_{BC}$};



\draw[blue,thick,opacity=0.75] (C0.south west) to (C19.south east) to 
                               (C399.north east) to (C380.north west) to  (C0.south west) ;
\draw[OliveGreen,thick,opacity=0.75] (D0.south west) to (D25.south east) to 
                               (D675.north east) to (D650.north west) to  (D0.south west) ;


                               
% =====================
% ruler x                               
\draw[|-|, BrickRed] ($(B0.south west) + (0.0,-0.15)$) --  ($(B19.south east) + (0.0,-0.15)$) ;
\draw[|-|, BrickRed] ($(B20.south west) + (0.0,-0.15)$) --  ($(B25.south east) + (0.0,-0.15)$) ;

\node[BrickRed] at ($(B10)+(0.0,-0.75)$) {$N_c$};
\node[BrickRed] at ($(B23)+(0.0,-0.75)$) {$L_c\mbox{-}1$};

% ---

\draw[|-|, BrickRed] ($(F0.south west) + (0.0,-0.15)$) --  ($(F5.south east) + (0.0,-0.15)$) ;
\draw[|-|, BrickRed] ($(F6.south west) + (0.0,-0.15)$) --  ($(F25.south east) + (0.0,-0.15)$) ;

\node[BrickRed] at ($(F2)+(0.0,-0.75)$) {$L_c$};
\node[BrickRed] at ($(F16)+(0.0,-0.75)$) {$N_c\mbox{-}1$};

% ---

\draw[|-|, BrickRed] ($(D0.south west) + (0.0,-0.15)$) --  ($(D2.south east) + (0.0,-0.15)$) ;
\draw[|-|, BrickRed] ($(D3.south west) + (0.0,-0.15)$) --  ($(D22.south east) + (0.0,-0.15)$) ;
\draw[|-|, BrickRed] ($(D23.south west) + (0.0,-0.15)$) --  ($(D25.south east) + (0.0,-0.15)$) ;

\node[BrickRed] at ($(D1)+(0.0,-0.75)$) {$L_c^{(1)}$};
\node[BrickRed] at ($(D13)+(0.0,-0.75)$) {$N_c$};
\node[BrickRed] at ($(D24)+(0.0,-0.75)$) {$L_c^{(0)}$};


% ===================
% ruler y
\draw[|-|, BrickRed] ($(B0.south west) + (-0.25,0.0)$) --  ($(B130.north west) + (-0.25,0.0)$) ;
\draw[|-|, BrickRed] ($(A0.south west) + (-0.25,0.0)$) --  ($(A380.north west) + (-0.25,0.0)$) ;

\node[BrickRed] at ($(B78)+(-1.0,0.0)$) {$L_r\mbox{-}1$};
\node[BrickRed] at ($(A200)+(-1.0,0.0)$) {$N_r$};
                               
% ---

\draw[|-|, BrickRed] ($(F0.south west) + (-0.25,0.0)$) --  ($(F494.north west) + (-0.25,0.0)$) ;
\draw[|-|, BrickRed] ($(E280.south west) + (-0.25,0.0)$) --  ($(E380.north west) + (-0.25,0.0)$) ;

\node[BrickRed] at ($(F260)+(-1.0,0.0)$) {$N_r\mbox{-}1$};
\node[BrickRed] at ($(E320)+(-1.0,0.0)$) {$L_r$};
                               
% ---

\draw[|-|, BrickRed] ($(D0.south west) + (-0.25,0.0)$) --  ($(D52.north west) + (-0.25,0.0)$) ;
\draw[|-|, BrickRed] ($(D78.south west) + (-0.25,0.0)$) --  ($(D572.north west) + (-0.25,0.0)$) ;
\draw[|-|, BrickRed] ($(D598.south west) + (-0.25,0.0)$) --  ($(D650.north west) + (-0.25,0.0)$) ;

\node[BrickRed] at ($(D26)+(-1.0,0.0)$) {$L_r^{(0)}$};
\node[BrickRed] at ($(D338)+(-1.0,0.0)$) {$N_r$};
\node[BrickRed] at ($(D624)+(-1.0,0.0)$) {$L_r^{(1)}$};
                               
\end{scope}

% ===================


\node[black, right of=] at ($(B4)+(0,-1.75)$) {\large$\begin{array}{cl}
 \bullet & \mathbf{u}_{ZP} = {\cal{P}}_{ZP}\{ \mathbf{u} \} \\ [5pt]
 \bullet & U_F = {\cal{F}}\{ \mathbf{u}_{ZP} \}
\end{array}$};


\node[black, right of=] at ($(F8)+(0,-2.025)$) {\large$\begin{array}{cl}
 \bullet & \mathbf{h}_{ZP} = {\cal{P}}_{ZP}\{ \mathbf{h}, \mbox{shape} = \mathbf{u}_{ZP} \} \\ [5pt]
 \bullet & H_F = {\cal{F}}\{ \mathbf{h}_{ZP} \}
\end{array}$};


\node[black, right of=] at ($(D10)+(0,-3.0)$) {\large$\begin{array}{cl}
 \bullet & \mathbf{b}_{ZP} = {\cal{F}}^{-1}\{ U_F \odot H_F \} \\ [5pt]
 \bullet & L_n^{(0)} = \lfloor \frac{L_n - ( L_n (\mbox{mod } 2) )}{2} \rfloor,  n \in \{r,\,c\}   \\ [5pt]
  & L_n^{(1)} = L_n - L_n^{(0)} - 1   \\ [5pt]
 \bullet & \mathbf{b} = {\cal{P}}^{-1}\{ \mathbf{b}_{ZP} \}
\end{array}$};

``` 

<!-- <div style="margin-top:-1.0em;"></div> -->
<p style="text-align: center;"><a name="fig-convLinearA">&nbsp;</a>Figure 2. Linear convolution. </p>


<!-- ============= -->
<!-- sub-details 1 -->
<details class="dDown tab1">
    <summary><span style="font-size:1.0em;"><u>Code snippet </u></span></summary>
  
````{tab} Code snippet
```python
# Pre-processing  -- (np: numpy)
Lr0 = np.int( (Lr - np.remainder(Lr,2))/2 )
Lr1 = Lr - Lr0 - 1

padShp = (u.shape[0]+Lr-1,u.shape[1]+Lc-1)

# Product in the Fourier domain (see additional comments)
UF = rfft2( u, s=padShp, axes=(0,1) )
HF = rfft2( h, s=padShp, axes=(0,1) )

bBC = irfft2( UF*HF, s=padShp, axes=(0,1) )

# Post-processing
b = bBC[Lr0:u.shape[0]+Lr0,Lc0:u.shape[1]+Lc0] 
```
````

````{tab} Additional comments

* If `pyfftw` is to be used, then
  ```python
  import pyfftw
  
  rfft2 = pyfftw.interfaces.numpy_fft.rfft2
  irfft2 = pyfftw.interfaces.numpy_fft.irfft2
  ```
* if JAX is to be used, then  
  ```python
  import jax
  from jax import numpy as jnp
  
  rfft2 = jnp.fft.rfft2
  irfft2 = jnp.fft.irfft2
  ```
````

  </details>

<!-- ============= -->
<!-- sub-details 2 -->
<div style="line-height:25%;"> <br> </div>
<details class="dDown tab1">
        <summary><span style="font-size:1.0em;"><u>Stand-alone simulation </u></span></summary>

```{code-cell} ipython3
        
from PIL import Image 
import numpy as np
from scipy import signal

import matplotlib.pyplot as PLT
%matplotlib inline

import pyfftw
        
# Imports needed to read images from an URL address
import requests
from io import BytesIO

        
# Test images from the SIPI image database
fname = {0: requests.get('http://sipi.usc.edu/database/misc/5.2.10.tiff'),   # bridge (grayscale)
         1: requests.get('http://sipi.usc.edu/database/misc/boat.512.tiff'), # boat (grayscale)
         2: requests.get('http://sipi.usc.edu/database/misc/4.2.03.tiff'),   # mandrill (color)
        }     

# Read input image      
u = np.asarray( Image.open(BytesIO(fname.get(0).content)) ).astype(float) / 256.0
     
# Generate a random filter     
Lr = 7
Lc = 8
h = np.random.randn(Lr,Lc)
      
# Linear convolution (sptail domain)
bSpt = signal.convolve2d(u, h, boundary='fill', mode='same')

# --- Linear convolution (frequency domain) ---
# Freq. domain: pre-processing
Lr0 = np.int( (Lr - np.remainder(Lr,2))/2 )
Lr1 = Lr - Lr0 - 1
Lc0 = np.int( (Lc - np.remainder(Lc,2))/2 )
Lc1 = Lc - Lc0 - 1

# Freq. domain: product in Fourier
padShp = (u.shape[0]+Lc-1,u.shape[1]+Lr-1)
UF = pyfftw.interfaces.numpy_fft.rfft2( u, s=padShp, axes=(0,1) )
HF = pyfftw.interfaces.numpy_fft.rfft2( h, s=padShp, axes=(0,1) )

bBC = pyfftw.interfaces.numpy_fft.irfft2( UF*HF, s=padShp, axes=(0,1) )

# Freq. domain: post-processing
bFrq = bBC[Lc1:u.shape[0]+Lc1,Lr1:u.shape[1]+Lr1] 

# ---  ---

# Plot results

diff = np.linalg.norm(bSpt-bFrq) 

figure, f = PLT.subplots(ncols=3, figsize=(12, 18)) 
f[0].imshow( u , cmap='gray') 
f[0].set_title('original') 
f[0].set_axis_off()
f[1].imshow( bSpt, cmap='gray') 
f[1].set_title(r'$\mathbf{b}_{spt} = conv(\mathbf{u},\mathbf{h})$') 
f[1].set_axis_off()
f[2].imshow( bSpt-bFrq , cmap='gray') 
txt = r'$\mathbf{b}_{spt} - \mathbf{b}_{frq}$, ($\|\| \cdot \|\|_2 =$'+'{:01.2g})'.format(diff)
f[2].set_title(txt) 
f[2].set_axis_off()

PLT.show()

```

</details>

<!-- ============= -->
<!-- sub-details 3 -->
<div style="line-height:25%;"> <br> </div>
<details class="dDown tab1">
        <summary><span style="font-size:1.0em;"><u>F2O-based simulation </u></span></summary>
  

  </details> 

</details><!-- _END_ Linear convolution -->


<!-- ======================================================================================= -->
<div style="line-height:50%;"> <br> </div>
<details>
  <summary><span style="font-size:1.0em;"><strong>Convolution with circular B.C.  </strong></span></summary>

```{code-cell} ipython3
---
tags: [remove-input]
---


%%tikz -s 800,400 -x $preamble -p pgfplots,tikz-3dplot,amsfonts -l arrows,matrix,fit,calc,hobby,3d -f png

\tikzset{every picture/.style={remember picture}}

\tdplotsetmaincoords{75}{-40}


\begin{scope}[tdplot_main_coords, canvas is xz plane at y=0,transform shape]

\foreach \ii [count = \xi] in {0,1,2,...,22}{
  \foreach \jj  [count = \yi]in {0,1,2,...,22}{
     
      \pgfmathsetmacro{\nn}{int(\xi-1+20*(\yi-4))}
      
      \pgfmathparse{ int(\xi>=1)*int(\xi<=20)*int(\yi>=4)*int(\yi<=23)}
      \ifnum1=\pgfmathresult      
        \node[blue!50,draw,minimum size=.25cm] (A\nn) at ($(0.25*\ii,0.25*\jj)$) {};
      \fi
% -------------

      \pgfmathsetmacro{\mm}{int(\xi-1+23*(\yi-1))}

      \pgfmathparse{ int(\xi>=1)*int(\xi<=6)*int(\yi>=18)*int(\yi<=23)}
      \ifnum0=\pgfmathresult      
      
        \pgfmathparse{ int(\xi>=1)*int(\xi<=20)*int(\yi>=4)*int(\yi<=23)}
        \ifnum1=\pgfmathresult      
          \node[fill=black,minimum size=.25cm,opacity=0.2] (F\nn) at ($(7.5,-1.35)+(0.25*\ii,0.25*\jj)$) {};                   
        \fi 
      \else
        \node[blue!50,draw,minimum size=.25cm] (E\nn) at ($(7.5,-1.35)+(0.25*\ii,0.25*\jj)$) {};
      \fi
     
% -------------


      \pgfmathparse{ int(\xi>=1)*int(\xi<=20)*int(\yi>=4)*int(\yi<=23)}
      \ifnum1=\pgfmathresult      
        \node[blue!50,draw,minimum size=.25cm] (C\nn) at ($(15.0,-2.7)+(0.25*\ii,0.25*\jj)$) {};
        \pgfmathparse{ int(\xi>=4)*int(\xi<=20)*int(\yi>=4)*int(\yi<=20)}
        \ifnum0=\pgfmathresult      
          \node[fill=OliveGreen,minimum size=.25cm,opacity=0.2] (D\mm) at ($(15.0,-2.7)+(0.25*\ii,0.25*\jj)$) {};             
        \fi
      \else
          \pgfmathparse{ int(\xi>=21)*int(\yi<=20)}
          \ifnum1=\pgfmathresult
            \node[fill=OliveGreen,minimum size=.25cm,opacity=0.1] (D\mm) at ($(15.0,-2.7)+(0.25*\ii,0.25*\jj)$) {};             
          \fi
      \fi

      \pgfmathparse{ int(\xi>=4)*int(\xi<=20)*int(\yi>=1)*int(\yi<=3)}
      \ifnum1=\pgfmathresult      
            \node[fill=OliveGreen,minimum size=.25cm,opacity=0.1] (D\mm) at ($(15.0,-2.7)+(0.25*\ii,0.25*\jj)$) {};             
      \fi
     
  }
}


\node[opacity=0,minimum size=.25cm] (D0) at ($(15.0,-2.7)$) {}; 
\node[opacity=0,minimum size=.25cm] (D2) at ($(15.0,-2.7)+(0.5,0.0)$) {}; 
\node[opacity=0,minimum size=.25cm] (D26) at ($(15.0,-2.7)+(0.0,0.5)$) {}; 


\node[blue] at ($(A390.north)+(0,0.35)$) {\large$\mathbf{u}$};

\draw[blue,thick,opacity=0.75] (A0.south west) to (A19.south east) to 
                               (A399.north east) to (A380.north west) to  (A0.south west) ;

% ---

\node[blue] at ($(E382.north)+(0,0.35)$) {\large$\mathbf{h}$};

\node[black] at ($(F392.north)+(0.0,0.35)$) {\large$\mathbf{h}_{ZP}$};


\draw[blue,thick,opacity=0.75] (E280.south west) to (E285.south east) to 
                               (E385.north east) to (E380.north west) to  (E280.south west) ;
\draw[black,thick,opacity=0.75] (F0.south west) to (F19.south east) to 
                               (F399.north east) to (E380.north west) to  (F0.south west) ;

% ---

\node[blue] at ($(D459.north)+(0,0.35)$) {\large$\mathbf{b}$};

\node[OliveGreen] at ($(C380.north west)+(-0.5,0.5)$) {\large$\mathbf{b}_{BC}$};



\draw[OliveGreen,thick,opacity=0.75] (C0.south west) to (C19.south east) to 
                               (C399.north east) to (C380.north west) to  (C0.south west) ;
\draw[blue,thick,opacity=0.75] (D3.south west) to (D22.south east) to 
                               (D459.north east) to (C323.north west) to  (D3.south west) ;


                               
% =====================
% ruler x                               
\draw[|-|, BrickRed] ($(A0.south west) + (0.0,-0.15)$) --  ($(A19.south east) + (0.0,-0.15)$) ;

\node[BrickRed] at ($(A10)+(0.0,-0.75)$) {$N_c$};

% ---

\draw[|-|, BrickRed] ($(F0.south west) + (0.0,-0.15)$) --  ($(F5.south east) + (0.0,-0.15)$) ;
\draw[|-|, BrickRed] ($(F6.south west) + (0.0,-0.15)$) --  ($(F19.south east) + (0.0,-0.15)$) ;

\node[BrickRed] at ($(F2)+(0.0,-0.75)$) {$L_c$};
\node[BrickRed] at ($(F14)+(0.0,-0.75)$) {$N_c-L_c$};

% ---

\draw[|-|, BrickRed] ($(D0.south west) + (0.0,-0.15)$) --  ($(D2.south east) + (0.0,-0.15)$) ;
\draw[|-|, BrickRed] ($(D3.south west) + (0.0,-0.15)$) --  ($(D22.south east) + (0.0,-0.15)$) ;

\draw[|-|, BrickRed] ($(C380.north west) + (0.0,0.15)$) --  ($(C399.north east) + (0.0,0.15)$) ;

\node[BrickRed] at ($0.5*(D0)+0.5*(D2)+(0.0,-0.75)$) {$L_c^{(0)}$};
\node[BrickRed] at ($(D13)+(0.0,-0.75)$) {$N_c$};

\node[BrickRed] at ($(C390)+(0.0,0.65)$) {$N_c$};


% ===================
% ruler y

\draw[|-|, BrickRed] ($(A0.south west) + (-0.25,0.0)$) --  ($(A380.north west) + (-0.25,0.0)$) ;

\node[BrickRed] at ($(A200)+(-1.0,0.0)$) {$N_r$};

% ---


\draw[|-|, BrickRed] ($(E280.south west) + (-0.25,0.0)$) --  ($(E380.north west) + (-0.25,0.0)$) ;
\draw[|-|, BrickRed] ($(F0.south west) + (-0.25,0.0)$) --  ($(F260.north west) + (-0.25,0.0)$) ;

\node[BrickRed] at ($(E320)+(-1.0,0.0)$) {$L_c$};

% \node[BrickRed] at ($(F120)+(-1.0,0.0)$) {$\begin{array}{c}N_c \\ - \\ L_c} \end{array}$};
\node[BrickRed] at ($(F120)+(-1.0,0.0)$) {$N_c \mbox{-} L_c$};

% ---

\draw[|-|, BrickRed] ($(D0.south west) + (-0.25,0.0)$) --  ($(D26.north west) + (-0.25,0.0)$) ;
\draw[|-|, BrickRed] ($(C0.south west) + (-0.25,0.0)$) --  ($(C380.north west) + (-0.25,0.0)$) ;

\draw[|-|, BrickRed] ($(D22.south west) + (0.5,0.0)$) --  ($(D459.north west) + (0.5,0.0)$) ;

\node[BrickRed] at ($0.5*(D0)+0.5*(D26)+(-1.0,0.0)$) {$L_r^{(0)}$};
\node[BrickRed] at ($(C200)+(-1.0,0.0)$) {$N_r$};

\node[BrickRed] at ($(D229)+(0.75,0.0)$) {$N_r$};

% ---

                               

                               
\end{scope}

% ===================

\node[black, right of=] at ($(A4)+(0,-3.15)$) {\large$\begin{array}{cl}
 \bullet & U_F = {\cal{F}}\{ \mathbf{u} \} \\ [5pt]
 \bullet & L_n^{(0)} = \lfloor \frac{L_n - ( L_n (\mbox{mod } 2) )}{2} \rfloor   \\ [5pt]
  & L_n^{(1)} = L_n - L_n^{(0)} - 1   \\ [5pt]
  & n \in \{r,\,c\}
\end{array}$};

\node[black, right of=] at ($(F4)+(-0.25,-3.15)$) {\large$\begin{array}{cl}
 \bullet & \mathbf{h}_{ZP} = {\cal{P}}_{ZP}\{ \mathbf{h}, \mbox{shape}=\mathbf{u} \} \\ [5pt]
 \bullet & H_F = {\cal{F}}\{ \mathbf{h}_{ZP} \} \\ [5pt]
\end{array}$};


\node[black, right of=] at ($(D4)+(0.0,-2.35)$) {\large$\begin{array}{cl}
 \bullet & \mathbf{b}_{BC} = {\cal{F}}^{-1}\{ U_F \odot H_F \} \\ [5pt]
 \bullet & \mathbf{b} = {\cal{R}}\{ \mathbf{b}_{BC} \} \\ [5pt] 
         & \color{darkgray} {\cal{R}}: \mbox{roll / circular shift.}
\end{array}$};

``` 

<div style="margin-top:-1.0em;"></div>
<p style="text-align: center;"><a name="fig-convFSymm">&nbsp;</a> Figure 3. Convolution with circular B.C. </p>


<!-- ============= -->
<!-- sub-details 1 -->
<details class="dDown tab1">
    <summary><span style="font-size:1.0em;"><u>Code snippet </u></span></summary>
  
````{tab} Code snippet
```python
# Pre-processing  -- (np: numpy)
Lr0 = np.int( (Lr - np.remainder(Lr,2))/2 )
Lr1 = Lr - Lr0 - np.remainder(Lr,2)
Lc0 = np.int( (Lc - np.remainder(Lc,2))/2 )
Lc1 = Lc - Lc0 - np.remainder(Lc,2)

padShp = u.shape

# Product in the Fourier domain (see additional comments)
UF = rfft2( u, s=padShp, axes=(0,1) )
HF = rfft2( h, s=padShp, axes=(0,1) )

bBC = irfft2( UF*HF, s=padShp, axes=(0,1) )

# Post-processing
bFrq = np.roll(bBC, (-Lr0 + 1 - np.remainder(Lr,2),-Lc0 + 1 - np.remainder(Lc,2)), axis=(0,1) )
```
````

````{tab} Additional comments

* If `pyfftw` is to be used, then
  ```python
  import pyfftw
  
  rfft2 = pyfftw.interfaces.numpy_fft.rfft2
  irfft2 = pyfftw.interfaces.numpy_fft.irfft2
  ```
* if JAX is to be used, then  
  ```python
  import jax
  from jax import numpy as jnp
  
  rfft2 = jnp.fft.rfft2
  irfft2 = jnp.fft.irfft2
  ```
````

</details>

<!-- ============= -->
<!-- sub-details 2 -->
<div style="line-height:25%;"> <br> </div>
<details class="dDown tab1">
        <summary><span style="font-size:1.0em;"><u>Stand-alone simulation </u></span></summary>

```{code-cell} ipython3
        
from PIL import Image, ImageOps
import numpy as np
from scipy import signal

import matplotlib.pyplot as PLT
%matplotlib inline

import pyfftw
        
# Imports needed to read images from an URL address
import requests
from io import BytesIO

        
# Test images from the SIPI image database
fname = {0: requests.get('http://sipi.usc.edu/database/misc/5.2.10.tiff'),   # bridge (grayscale)
         1: requests.get('http://sipi.usc.edu/database/misc/boat.512.tiff'), # boat (grayscale)
         2: requests.get('http://sipi.usc.edu/database/misc/4.2.03.tiff'),   # mandrill (color)
        }     

# Read input image      
u = np.asarray( ImageOps.grayscale( Image.open(BytesIO(fname.get(2).content)) ) ).astype(float) / 256.0
     
# Generate a random filter     
Lr = 7
Lc = 8
h = np.random.randn(Lr,Lc)
      
# Convolution with circular BC (sptail domain)
bSpt = signal.convolve2d(u, h, boundary='wrap', mode='same')

# --- Convolution with circular BC (frequency domain) ---
# Freq. domain: pre-processing
Lr0 = np.int( (Lr - np.remainder(Lr,2))/2 )
Lr1 = Lr - Lr0 - np.remainder(Lr,2)
Lc0 = np.int( (Lc - np.remainder(Lc,2))/2 )
Lc1 = Lc - Lc0 - np.remainder(Lc,2)


padShp = u.shape

# Freq. domain: product in Fourier
UF = pyfftw.interfaces.numpy_fft.rfft2( u, s=padShp, axes=(0,1) )
HF = pyfftw.interfaces.numpy_fft.rfft2( h, s=padShp, axes=(0,1) )

bBC = pyfftw.interfaces.numpy_fft.irfft2( UF*HF, s=padShp, axes=(0,1) )

# Freq. domain: post-processing
bFrq = np.roll(bBC, (-Lr0 + 1 - np.remainder(Lr,2),-Lc0 + 1 - np.remainder(Lc,2)), axis=(0,1) )

# ---  ---

# Plot results

diff = np.linalg.norm(bSpt-bFrq) 

figure, f = PLT.subplots(ncols=3, figsize=(12, 18)) 
f[0].imshow( u , cmap='gray') 
f[0].set_title('original') 
f[0].set_axis_off()
f[1].imshow( bSpt, cmap='gray') 
f[1].set_title(r'$\mathbf{b}_{spt} = conv(\mathbf{u},\mathbf{h})$') 
f[1].set_axis_off()
f[2].imshow( bSpt-bFrq , cmap='gray') 
txt = r'$\mathbf{b}_{spt} - \mathbf{b}_{frq}$, ($\|\| \cdot \|\|_2 =$'+'{:01.2g})'.format(diff)
f[2].set_title(txt) 
f[2].set_axis_off()

PLT.show()

```
        
</details>

<!-- ============= -->
<!-- sub-details 2 -->
<div style="line-height:25%;"> <br> </div>
<details class="dDown tab1">
        <summary><span style="font-size:1.0em;"><u>F2O-based simulation </u></span></summary>

</details>


</details>

<!-- ======================================================================================= -->
<div style="line-height:50%;"> <br> </div>
<details>
  <summary><span style="font-size:1.0em;"><strong>Convolution with symmetric B.C.  </strong></span></summary>

```{code-cell} ipython3
---
tags: [remove-input]
---


%%tikz -s 1200,600 -x $preamble -p pgfplots,tikz-3dplot,amsfonts -l arrows,matrix,fit,calc,hobby,3d -f png

\tikzset{every picture/.style={remember picture}}

\tdplotsetmaincoords{75}{-40}


\begin{scope}[tdplot_main_coords, canvas is xz plane at y=0,transform shape, scale=1.0]

\foreach \ii [count = \xi] in {0,1,2,...,28}{
  \foreach \jj  [count = \yi]in {0,1,2,...,28}{
     
      \pgfmathsetmacro{\nn}{int(\xi-4+20*(\yi-7))}
      \pgfmathsetmacro{\mm}{int(\xi-1+29*(\yi-1))}

      \pgfmathparse{ int(\xi>=4)*int(\xi<=23)*int(\yi>=7)*int(\yi<=26)}
      \ifnum0=\pgfmathresult
      
        \pgfmathparse{ int(\xi>=1)*int(\yi>=4)*int(\yi<=29)*int(\xi<=26)}
        \ifnum0=\pgfmathresult
            \node[fill=black,minimum size=.25cm,opacity=0.2] (B\mm) at ($(0.25*\ii,0.25*\jj)$) {};
            
         \else
            \node[fill=OliveGreen,minimum size=.25cm,opacity=0.1] (B\mm) at ($(0.25*\ii,0.25*\jj)$) {};
         \fi
       
      
      \else
        \node[blue!50,draw,minimum size=.25cm] (A\nn) at ($(0.25*\ii,0.25*\jj)$) {};
      \fi
     
% ---------

      \pgfmathsetmacro{\nn}{int(\xi-1+20*(\yi-10))}
      \pgfmathsetmacro{\mm}{int(\xi-1+29*(\yi-1))}

      \pgfmathparse{ int(\xi>=1)*int(\xi<=6)*int(\yi>=24)*int(\yi<=29)}
      \ifnum0=\pgfmathresult      
      
        \node[fill=black,minimum size=.25cm,opacity=0.2] (F\mm) at ($(9.5,-1.35)+(0.25*\ii,0.25*\jj)$) {};                   
           
      \else
        \node[blue!50,draw,minimum size=.25cm] (E\nn) at ($(9.5,-1.35)+(0.25*\ii,0.25*\jj)$) {};
      \fi
     
% -------------

      \pgfmathsetmacro{\kk}{int(\xi-6+20*(\yi-5))}
      \pgfmathsetmacro{\ll}{int(\xi-1+29*(\yi-1))}

      \pgfmathparse{ int(\xi>=6)*int(\xi<=25)*int(\yi>=5)*int(\yi<=24)}
      \ifnum0=\pgfmathresult      
        \node[fill=OliveGreen,minimum size=.25cm,opacity=0.1] (D\ll) at ($(19.0,-2.7)+(0.25*\ii,0.25*\jj)$) {};             
      \else
        \node[blue!50,draw,minimum size=.25cm] (C\kk) at ($(19.0,-2.7)+(0.25*\ii,0.25*\jj)$) {};
      \fi


     
  }
}

\node[blue] at ($(A390.north)+(0,0.25)$) {\large$\mathbf{u}$};

\node[OliveGreen] at ($(A399.north)+(0.35,0.35)$) {\large$\mathbf{u}_{BC}$};

\node[black] at ($(A399.north)+(1.25,1.05)$) {\large$\mathbf{u}_{ZP}$};


\draw[blue,thick,opacity=0.75] (A0.south west) to (A19.south east) to 
                               (A399.north east) to (A380.north west) to  (A0.south west) ;
\draw[OliveGreen,thick,opacity=0.75] (B87.south west) to (B112.south east) to 
                               (B837.north east) to (B812.north west) to  (B87.south west) ;
\draw[black,thick,opacity=0.75] (B0.south west) to (B28.south east) to 
                               (B840.north east) to (B812.north west) to  (B0.south west) ;

% ---

\node[blue] at ($(E382.north)+(0,0.35)$) {\large$\mathbf{h}$};

\node[black] at ($(F825.north)+(0.0,0.35)$) {\large$\mathbf{h}_{ZP}$};


\draw[blue,thick,opacity=0.75] (E280.south west) to (E285.south east) to 
                               (E385.north east) to (E380.north west) to  (E280.south west) ;
\draw[black,thick,opacity=0.75] (F0.south west) to (F28.south east) to 
                               (F840.north east) to (E380.north west) to  (F0.south west) ;

% ---

\node[blue] at ($(C390.north)+(0,0.35)$) {\large$\mathbf{b}$};

\node[OliveGreen] at ($(C398.north)+(0.75,0.25)$) {\large$\mathbf{b}_{BC}$};



\draw[blue,thick,opacity=0.75] (C0.south west) to (C19.south east) to 
                               (C399.north east) to (C380.north west) to  (C0.south west) ;
\draw[OliveGreen,thick,opacity=0.75] (D0.south west) to (D28.south east) to 
                               (D840.north east) to (D812.north west) to  (D0.south west) ;

                               
                               
% ================================
% ruler x                               
\draw[|-|, BrickRed] ($(B0.south west) + (0.0,-0.15)$) --  ($(B2.south east) + (0.0,-0.15)$) ;
\draw[|-|, BrickRed] ($(B3.south west) + (0.0,-0.15)$) --  ($(B22.south east) + (0.0,-0.15)$) ;
\draw[|-|, BrickRed] ($(B23.south west) + (0.0,-0.15)$) --  ($(B25.south east) + (0.0,-0.15)$) ;
\draw[|-|, BrickRed] ($(B26.south west) + (0.0,-0.15)$) --  ($(B28.south east) + (0.0,-0.15)$) ;

\node[BrickRed] at ($(B1)+(0.0,-0.75)$) {$L_c^{(0)}$};
\node[BrickRed] at ($(B13)+(0.0,-0.75)$) {$N_c$};
\node[BrickRed] at ($(B24)+(0.0,-0.75)$) {$L_c^{(1)}$};
\node[BrickRed] at ($(B27)+(0.0,-0.75)$) {$L_c\mbox{-}1$};

% ---

\draw[|-|, BrickRed] ($(F0.south west) + (0.0,-0.15)$) --  ($(F5.south east) + (0.0,-0.15)$) ;
\draw[|-|, BrickRed] ($(F6.south west) + (0.0,-0.15)$) --  ($(F28.south east) + (0.0,-0.15)$) ;

\node[BrickRed] at ($(F2)+(0.0,-0.75)$) {$L_c$};
\node[BrickRed] at ($(F19)+(0.0,-0.75)$) {$N_c+L_c^{(0)}+L_c^{(1)}+L_c-1$};

% ---

% ruler x                               
\draw[|-|, BrickRed] ($(D0.south west) + (0.0,-0.15)$) --  ($(D4.south east) + (0.0,-0.15)$) ;
\draw[|-|, BrickRed] ($(D5.south west) + (0.0,-0.15)$) --  ($(D24.south east) + (0.0,-0.15)$) ;
\draw[|-|, BrickRed] ($(D25.south west) + (0.0,-0.15)$) --  ($(D28.south east) + (0.0,-0.15)$) ;

\node[BrickRed] at ($(D2)+(0.0,-0.75)$) {$L_c - 1$};
\node[BrickRed] at ($(D13)+(0.0,-0.75)$) {$N_c$};
\node[BrickRed] at ($(D27)+(0.0,-0.75)$) {$L_c^{(0)} + L_c^{(1)} $};

% ================================
% ruler y
\draw[|-|, BrickRed] ($(B0.south west) + (-0.25,0.0)$) --  ($(B58.north west) + (-0.25,0.0)$) ;
\draw[|-|, BrickRed] ($(B87.south west) + (-0.25,0.0)$) --  ($(B145.north west) + (-0.25,0.0)$) ;
\draw[|-|, BrickRed] ($(B174.south west) + (-0.25,0.0)$) --  ($(B725.north west) + (-0.25,0.0)$) ;
\draw[|-|, BrickRed] ($(B754.south west) + (-0.25,0.0)$) --  ($(B812.north west) + (-0.25,0.0)$) ;

\node[BrickRed] at ($(B29)+(-1.0,0.0)$) {$L_r\mbox{-}1$};
\node[BrickRed] at ($(B116)+(-1.0,0.0)$) {$L_r^{(1)}$};
\node[BrickRed] at ($(B464)+(-1.0,0.0)$) {$N_r$};
\node[BrickRed] at ($(B783)+(-1.0,0.0)$) {$L_r^{(0)}$};

% ---

\draw[|-|, BrickRed] ($(F0.south west) + (-0.25,0.0)$) --  ($(F638.north west) + (-0.25,0.0)$) ;
\draw[|-|, BrickRed] ($(E280.south west) + (-0.25,0.0)$) --  ($(E380.north west) + (-0.25,0.0)$) ;

\node[BrickRed] at ($(E340)+(-1.0,0.0)$) {$L_r$};

\node[BrickRed] at ($(F348)+(-1.0,0.0)$) {$\begin{array}{c}
L_r^{(0)} \\ + \\ L_r^{(1)} \\ + \\ N_r + \\ L_r - 1
\end{array}$};


% ---

\draw[|-|, BrickRed] ($(D0.south west) + (-0.25,0.0)$) --  ($(D116.north west) + (-0.25,0.0)$) ;
\draw[|-|, BrickRed] ($(D145.south west) + (-0.25,0.0)$) --  ($(D696.north west) + (-0.25,0.0)$) ;
\draw[|-|, BrickRed] ($(D725.south west) + (-0.25,0.0)$) --  ($(D812.north west) + (-0.25,0.0)$) ;

\node[BrickRed] at ($(D58)+(-1.0,0.0)$) {$\begin{array}{c} L_r^{(0)}\\+\\L_r^{(1)} \end{array}$} ;
\node[BrickRed] at ($(D464)+(-1.0,0.0)$) {$N_r$};
\node[BrickRed] at ($(D783)+(-1.0,0.0)$) {$L_r-1$};

\end{scope}


\node[black, right of=] at ($(B8)+(0,-2.85)$) {\large$\begin{array}{cl}
 \bullet & \mathbf{u}_{ZP} = {\cal{P}}_{ZP}\{ {\cal{P}}_{BC}\{ \mathbf{u} \} \} \\ [5pt]
% \bullet & \mathbf{u}_{ZP} = {\cal{P}}_{ZP}\{ \mathbf{u}_{BC} \} \\ [5pt] 
 \bullet & L_n^{(0)} = \lfloor \frac{L_n - ( L_n (\mbox{mod } 2) )}{2} \rfloor,  n \in \{r,\,c\}   \\ [5pt]
  & L_n^{(1)} = L_n - L_n^{(0)} - ( L_n (\mbox{mod } 2) )   \\ [5pt]
 \bullet & U_F = {\cal{F}}\{ \mathbf{u}_{ZP} \} \\ [5pt]
\end{array}$};


\node[black, right of=] at ($(F8)+(0,-2.25)$) {\large$\begin{array}{cl}
 \bullet & \mathbf{h}_{ZP} = {\cal{P}}_{ZP}\{ \mathbf{h}, \mbox{shape} = \mathbf{u}_{ZP} \} \\ [5pt]
 \bullet & H_F = {\cal{F}}\{ \mathbf{h}_{ZP} \} \\ [5pt]
\end{array}$};


\node[black, right of=] at ($(D8)+(0,-2.25)$) {\large$\begin{array}{cl}
 \bullet & \mathbf{b}_{BC} = {\cal{F}}^{-1}\{ U_F \odot H_F \} \\ [5pt]
 \bullet & \mathbf{b} = {\cal{P}}^{-1}\{ \mathbf{b}_{BC} \} \\ [5pt]
\end{array}$};

``` 

<div style="margin-top:-1.0em;"></div>
<p style="text-align: center;"><a name="fig-convFSymm">&nbsp;</a> Figure 4. Convolution with symmetric B.C. </p>

<!-- ============= -->
<!-- sub-details 1 -->
<details class="dDown tab1">
    <summary><span style="font-size:1.0em;"><u>Code snippet </u></span></summary>
  
````{tab} Code snippet
```python
# Pre-processing  -- (np: numpy)
Lr0 = np.int( (Lr - np.remainder(Lr,2))/2 )
Lr1 = Lr - Lr0 - np.remainder(Lr,2)
Lc0 = np.int( (Lc - np.remainder(Lc,2))/2 )
Lc1 = Lc - Lc0 - np.remainder(Lc,2)

uBC = np.pad(u, ((Lr0,Lc0),(Lr1,Lc1)), mode='symmetric' )

padShp = (uBC.shape[0]+Lr-np.remainder(Lr,2), uBC.shape[1]+Lc-np.remainder(Lc,2))

# Product in the Fourier domain (see additional comments)
UF = rfft2( u, s=padShp, axes=(0,1) )
HF = rfft2( h, s=padShp, axes=(0,1) )

bBC = irfft2( UF*HF, s=padShp, axes=(0,1) )

# Post-processing
b = bBC[Lr-1:u.shape[0]+Lr-1,Lc-1:u.shape[1]+Lc-1] 
```
````

````{tab} Additional comments

* If `pyfftw` is to be used, then
  ```python
  import pyfftw
  
  rfft2 = pyfftw.interfaces.numpy_fft.rfft2
  irfft2 = pyfftw.interfaces.numpy_fft.irfft2
  ```
* if JAX is to be used, then  
  ```python
  import jax
  from jax import numpy as jnp
  
  rfft2 = jnp.fft.rfft2
  irfft2 = jnp.fft.irfft2
  ```
````

</details>

<!-- ============= -->
<!-- sub-details 2 -->
<div style="line-height:25%;"> <br> </div>
<details class="dDown tab1">
        <summary><span style="font-size:1.0em;"><u>Stand-alone simulation </u></span></summary>

```{code-cell} ipython3
        
from PIL import Image 
import numpy as np
from scipy import signal

import matplotlib.pyplot as PLT
%matplotlib inline

import pyfftw
        
# Imports needed to read images from an URL address
import requests
from io import BytesIO

        
# Test images from the SIPI image database
fname = {0: requests.get('http://sipi.usc.edu/database/misc/5.2.10.tiff'),   # bridge (grayscale)
         1: requests.get('http://sipi.usc.edu/database/misc/boat.512.tiff'), # boat (grayscale)
         2: requests.get('http://sipi.usc.edu/database/misc/4.2.03.tiff'),   # mandrill (color)
        }     

# Read input image      
u = np.asarray( Image.open(BytesIO(fname.get(1).content)) ).astype(float) / 256.0
     
# Generate a random filter     
Lr = 7
Lc = 8
h = np.random.randn(Lr,Lc)
      
# Convolution with symmetric BC (sptail domain)
bSpt = signal.convolve2d(u, h, boundary='symm', mode='same')

# --- Convolution with symmetric BC (frequency domain) ---
# Freq. domain: pre-processing
Lr0 = np.int( (Lr - np.remainder(Lr,2))/2 )
Lr1 = Lr - Lr0 - np.remainder(Lr,2)
Lc0 = np.int( (Lc - np.remainder(Lc,2))/2 )
Lc1 = Lc - Lc0 - np.remainder(Lc,2)

uBC = np.pad(u, ((Lr0,Lr0),(Lc1,Lc1)), mode='symmetric' )

padShp = (uBC.shape[0]+Lr-np.remainder(Lr,2), uBC.shape[1]+Lc-np.remainder(Lc,2))

# Freq. domain: product in Fourier
UF = pyfftw.interfaces.numpy_fft.rfft2( uBC, s=padShp, axes=(0,1) )
HF = pyfftw.interfaces.numpy_fft.rfft2( h, s=padShp, axes=(0,1) )

bBC = pyfftw.interfaces.numpy_fft.irfft2( UF*HF, s=padShp, axes=(0,1) )

# Freq. domain: post-processing
bFrq = bBC[Lr-1:u.shape[0]+Lr-1,Lc-1:u.shape[1]+Lc-1]

# ---  ---

# Plot results

diff = np.linalg.norm(bSpt-bFrq) 

figure, f = PLT.subplots(ncols=3, figsize=(12, 18)) 
f[0].imshow( u , cmap='gray') 
f[0].set_title('original') 
f[0].set_axis_off()
f[1].imshow( bSpt, cmap='gray') 
f[1].set_title(r'$\mathbf{b}_{spt} = conv(\mathbf{u},\mathbf{h})$') 
f[1].set_axis_off()
f[2].imshow( bSpt-bFrq , cmap='gray') 
txt = r'$\mathbf{b}_{spt} - \mathbf{b}_{frq}$, ($\|\| \cdot \|\|_2 =$'+'{:01.2g})'.format(diff)
f[2].set_title(txt) 
f[2].set_axis_off()

PLT.show()

```
        
</details>

<!-- ============= -->
<!-- sub-details 3 -->
<div style="line-height:25%;"> <br> </div>
<details class="dDown tab1">
        <summary><span style="font-size:1.0em;"><u>F2O-based simulation </u></span></summary>

</details>



</details>


<div style="line-height:50%;"> <br> </div>
<hr style="border:0.125px solid gray"> </hr> 
