---
jupytext:
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---



# Test

blah

$$ \begin{array}{l}
\; \\ \hline
\mathbf{\mbox{GD (spatial) Algorithm}} \\ \hline 

\; \\ \hline
% ---
\end{array}
$$


Math | Pyhton
-----|-------
$\mathbf{u}_{BC} = {\cal{P}}_{BC}\{ \mathbf{u} \} $ | `np.pad `$(\mathbf{u}, ((L_1^{(0)},L_1^{(1)}),(L_2^{(0)},L_2^{(1)})))$


```python

np.pad(a,( (la0,la1), (lb0,lb1) ), 'symm')

```





```{code-cell} ipython3
---
tags: [remove-input]
---
%load_ext tikzmagic

preamble = '''
    \definecolor{OliveGreen}{RGB}{60,128,49}
    \definecolor{BrickRed}{RGB}{182,50,28}
    \pgfplotsset{compat=1.16}
'''

```


```{code-cell} ipython3
from myst_nb import glue
my_variable = "here is some text!"
glue("cool_text", my_variable)
```



```tikz 
\draw[thick,rounded corners=8pt]
   (0,0)--(0,2)--(1,3.25)--(2,2)--(2,0)--(0,2)--(2,2)--(0,0)--(2,0);
```


````{tab} Python
```python
def main():
    return
```
````
````{tab} C++
```c++
int main(const int argc, const char **argv) {
  return 0;
}
```
````


Correspondance between graphs and code
````{tab} Boundary condition
$\mathbf{u}_{BC} = {\cal{P}}_{BC}\{ \mathbf{u} \} $
{glue:}`cool_text`
````
````{tab} Zero-pad
$\mathbf{u}_{BC} = {\cal{P}}_{BC}\{ \mathbf{u} \} $
````

<div style="margin-top:-1.0em;"></div>

````{tab} Boundary condition
```python
np.pad(a,( (la0,la1), (lb0,lb1) ), 'symm')
```
````
````{tab} Zero-pad
```python
np.pad(a,( (0,l1-1), (0,l2-1) ), 'zero')
```
````

<style type="text/css">
<!--
 .tab { margin-left: 1em; color: blue;}
-->
</style>

<p class="tab">Example of a  tab</p>

<details>
  <summary><span style="font-size:1.0em;"><strong>Caso 1 </strong></span></summary>
    blah
<span style="color: red;" >
<details class="tab">
       <summary><span style="font-size:1.0em;"><strong>Caso 1.1 </strong></span></summary>
        blah
     </details>
<!--  -->
<details>
       <summary><span style="font-size:1.0em;"><strong>Caso 1.2 </strong></span></summary>
        blah
     </details>
</span>
</details>
<!--  -->
<details>
    <summary><span style="font-size:1.0em;"><strong>Caso 2 </strong></span></summary>
</details>
