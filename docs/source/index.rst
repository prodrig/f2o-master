.. F2O documentation master file, created by
   sphinx-quickstart on Mon Nov  1 09:10:58 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to F2O's documentation!
===============================

**F2O** (First order optimization) is a JAX-aware Python package that implements several 
first order optimization methods with applications to inverse problems 
such Tikhonov  regularization, total variation, (convolutional) sparse 
representation, background subtraction/modeling, etc.

Originally, this library was developed as a companion software for a 
graduate-level optimization course at PUCP (Pontificia Universidad 
Catolica del Peru), which, due to the COVID-19 pandemic, was moved to a
100% online format. **F2O**'s main objective was to allow a high level, almost 
*algorithmic description-like*, way to program first order optimization 
methods such (accelerated) proximal gradient methods, ADMM, etc.

Currently, **F2O** support has expanded to other courses (such 
graduaute-lavel DIP) and has shifted from a purely academic companion software
to target inverse problems related to signal/image processing such
Total Variation (TV), Basis Pursuit (BP), convolutional Sparse Representation (CSR),
Video Background Modeling (VBM), etc.



Read the :doc:`notebooks/Phil_n_ex` section for F2O's philosophy and simple illustrative example.

.. note::

   This project is under active development.


.. toctree::
   :maxdepth: 1
   :caption: F2O's Documentation
   
   notebooks/Phil_n_ex.md
   jaxSupp
   
   
.. toctree::
   :maxdepth: 2
   :caption: Examples
   
   paperRep
   f2oDemo
   pucp

   
.. toctree::
   :maxdepth: 1
   :caption: Bibliography
   
   biblio


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


.. Contents
.. --------
.. 
..    
..    
.. .. toctree::
..    :maxdepth: 2
..    :caption: Contents:
   