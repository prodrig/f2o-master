
F2O demo examples
-----------------

Optimization algorithms
=======================

.. toctree::
   :maxdepth: 1
   

ADMM
^^^^

.. toctree::
   :maxdepth: 1

..    notebooks/GD_ex1.ipynb
..    notebooks/GD_ex2.ipynb

   
APG / FISTA
^^^^^^^^^^^

.. toctree::
   :maxdepth: 1

..    notebooks/GD_ex1.ipynb
..    notebooks/GD_ex2.ipynb

Anderson Acceleration
^^^^^^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 1
   

Applications
============

.. toctree::
   :maxdepth: 1

Convolutional Sparse Representations
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 1

   notebooks/f2oDemo/csc_oneImg.md

..    notebooks/GD_ex2.ipynb
   

   
Sparse Coding
^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 1

..    notebooks/GD_ex1.ipynb
..    notebooks/GD_ex2.ipynb
   
Total Variation
^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 1

..    notebooks/GD_ex1.ipynb
..    notebooks/GD_ex2.ipynb
   
   
:math:`\ell_0` gradient
^^^^^^^^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 1

..    notebooks/GD_ex1.ipynb
..    notebooks/GD_ex2.ipynb
   
   
Video Background Modeling
^^^^^^^^^^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 1

..    notebooks/GD_ex1.ipynb
..    notebooks/GD_ex2.ipynb

   