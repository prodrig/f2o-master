
PUCP courses
----------------

MAT787
======

.. toctree::
   :maxdepth: 1
   

Gradient descent
^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 1

   notebooks/pucp/GD_ex1.ipynb
   notebooks/pucp/GD_ex2.ipynb

   
   

ING609
======

.. toctree::
   :maxdepth: 1

   notebooks/pucp/ing609_ConvFqSp.md
   notebooks/pucp/ing609_gdDeconv.md
   
..    notebooks/test01.md
   
..   notebooks/GD_deconv.ipynb
   