# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

autodoc_mock_imports = ['_tkinter']

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))


# -- Project information -----------------------------------------------------

project = 'F2O'
copyright = '2021, Paul Rodriguez'
author = 'Paul Rodriguez'

# The full version, including alpha/beta/rc tags
release = '0.01'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
'sphinx.ext.autodoc',
'sphinx.ext.mathjax',
'sphinx.ext.napoleon',
'sphinx.ext.viewcode',
"sphinxcontrib.proof",
"sphinxcontrib.bibtex",
'matplotlib.sphinxext.plot_directive',
'sphinx.ext.intersphinx',
'sphinx_rtd_theme',
'myst_nb',
'nbsphinx',
'nbsphinx_link',
'sphinx_panels',
'sphinxcontrib.tikz',
"sphinx_inline_tabs",
]

# Potentially usefull extensions
  #"sphinx_proof",
  #"sphinxcontrib.proof",
  # 'matplotlib.sphinxext.plot_directive',               
  # 'matplotlib.sphinxext.only_directives',
  # 'sphinxcontrib.tikz',
  # 'sphinx.ext.graphviz',  
  

mathjax_config = {
    "TeX": {
        "Macros": {
            "argmin": [r"\mathop{\mathrm{argmin}}"],
            "sign": [r"\mathop{\mathrm{sign}}"],
            "prox": [r"\mathop{\mathrm{prox}}"],
        }
    }
}
  
bibtex_bibfiles = ['f2oRefs.bib']

intersphinx_mapping = {
    'python': ('https://docs.python.org/3/', None),
    'numpy': ('https://numpy.org/doc/stable/', None),
    'scipy': ('https://docs.scipy.org/doc/scipy/reference/', None),
}

suppress_warnings = [
    'ref.citation',  # Many duplicated citations in numpy/scipy docstrings.
    'ref.footnote',  # Many unreferenced footnotes in numpy/scipy docstrings
]



source_suffix = ['.rst', '.ipynb', '.md']


# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []

# Panels
panels_add_bootstrap_css = True

panels_css_variables = {
    "tabs-color-label-active": "hsla(231, 99%, 66%, 1)",
    "tabs-color-label-inactive": "rgba(178, 206, 245, 0.62)",
    "tabs-color-overline": "rgb(207, 236, 238)",
    "tabs-color-underline": "rgb(207, 236, 238)",
    "tabs-size-label": "1rem",
}

# Tikz
#tikz_proc_suite = GhostScript
#tikz_tikzlibraries = matrix

# -- Options specific to sphinxcontrib-tikz -------------------------------

latex_elements = {
'preamble': r'''
\usepackage{tikz}
\usetikzlibrary{arrows.meta}
\usepackage{hf-tikz}
\usepackage{tikz-3dplot}
'''
}

# For latex target the tikzlibraries have to be specified explicitly


# Use default suite
tikz_proc_suite = "GhostScript"




# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
#html_theme = 'alabaster'
html_theme = "sphinx_rtd_theme"

pygments_style = 'sphinx'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

#mathjax_path="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"

# Add `code-include` so that the code-include directives used in this documentation work
extensions += [
    "code_include.extension",
]



# -- Jupyter ----------------------------------------------
jupyter_execute_notebooks = "force"
execution_allow_errors = False
execution_fail_on_error = True  

# Notebook timeout
execution_timeout = 100


# -- Myst ----------------------------------------------
myst_enable_extensions = [
    "amsmath",
    "colon_fence",
    "deflist",
    "dollarmath",
    "html_image",
    "substitution",
    "replacements",
]
myst_url_schemes = ("http", "https", "mailto")




# ===============


