
import scipy as sc
import matplotlib.pylab as PLT
import numpy as np

import F2O.constants as f2oDef

import F2O.F2O_utils as F2O
from fwOp.fwOperator import fwOp
from F2O.F2O_sptl import gd

import demo.synthData as sd

__author__ = """Paul Rodriguez <prodrig@pucp.edu.pe>"""


# ======================================================
# ======================================================


#------------------------------------------------

def testGD(N=200, nIter=100, ssCte=5e-7):

  # -------------
  # Generate data
  # -------------
  
  synthData = sd.synthData()
  
  A, b, xori = synthData.genData(N, 1.*N)
  
  
  # --------------------------------------
  # Define parameter optimization variable 
  # --------------------------------------

  args = F2O.argsF2O()

  args.verbose    = False
  args.fCostClass = f2oDef.cost_L2_lin      # F(x) = 0.5|| fAx(x) - b ||_2^2, where fAx is lineal
  args.freqSol    = False                         
  
  # -- specific parameters will be selected below
  
  
  # --------------------------------------------
  # Select forward operator  (see fwOperator.py)
  # --------------------------------------------
  
  Op       = fwOp()
  Op.linOp = f2oDef.fAx_matrixvec
  Op.A     = A
  


  args.bShape    = b.shape
  args.hShape    = A.shape
  
  

  # --------------------------
  # --- Constant step-size ---
  args.ssPoliciy = f2oDef.ss_Cte
  args.ssCte     = ssCte
  x0, gdStats0 = gd(Op, b, nIter, args)


  # ------------------------------
  # --- Barzilai-Borwein step-size
  args.ssPoliciy = f2oDef.ss_BBv1
  x1, gdStats1 = gd(Op, b, nIter, args)
  
  
  # --------------------------
  # --- Cauchy step-size
  args.ssPoliciy = f2oDef.ss_Cauchy
  args.ssCte     = 0.0
  x2, gdStats2 = gd(Op, b, nIter, args)


  # --- Plot results ---
  PLT.figure()
  PLT.plot(gdStats0[:,0], label=r'$\alpha_k$ = {0} -- {1}'.format(ssCte, f2oDef.ss_list[f2oDef.ss_Cte]) )
  PLT.plot(gdStats1[:,0], label=r'$\alpha_k$ = {0}'.format(f2oDef.ss_list[f2oDef.ss_BBv1]))
  PLT.plot(gdStats2[:,0], label=r'$\alpha_k$ = {0}'.format(f2oDef.ss_list[f2oDef.ss_Cauchy]))
  PLT.legend(loc='upper right')
  PLT.ylabel(r'$f(x) = \frac{1}{2}\| A \mathbf{x} - \mathbf{b} \|_2^2$')
  PLT.xlabel('Iteracion')
  
  PLT.show()
