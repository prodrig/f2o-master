

import scipy as sc
import matplotlib.pylab as PLT
import numpy as np

import F2O.constants as CTE

import F2O.F2O_utils as F2O
from fwOp.fwOperator import fwOp
from F2O.F2O_sptl import gd
from F2O.PG_sptl import pg, apg


__author__ = """Paul Rodriguez <prodrig@pucp.edu.pe>"""



def genData(rows=100, cols=100, alpha=50, eta = 0.1):
  

  A = np.random.randn(rows,cols)
  A = A/np.linalg.norm(A,2,axis=0)
  
  xOri = np.random.randn(cols,1)
  indx = np.random.permutation(np.arange(0,cols))
  xOri[indx[0:alpha]] = 0.
  xOri = xOri/np.linalg.norm(xOri)
    
  b = A.dot(xOri) + eta*np.random.randn(rows,1)
  
  
  return(A, b, xOri)


#------------------------------------------------


def testISTA(N=np.array([100,200]), lmbda=0.1, nIter=100, eta=0.1, ssCte=5e-7):

  # -------------
  # Generate data
  # -------------
  
  A, b, xori = genData(N[0],N[1], int(np.floor(0.85*N[1])) )
  
  
  # --------------------------------------
  # Define parameter optimization variable 
  # --------------------------------------

  args = F2O.argsF2O()
  
  args.verbose    = True
  args.fCostClass = CTE.cost_L2L1_lin           # f(x) = 0.5|| fAx(x) - b ||_2^2 + \lambda|| x ||_1, where fAx is lineal
  args.proxOp     = CTE.prox_l1
  args.freqSol    = False                         
  
  # -- specific parameters will be selected below
  
  
  # --------------------------------------------
  # Select forward operator  (see fwOperator.py)
  # --------------------------------------------
  
  Op       = fwOp()
  Op.linOp = CTE.fAx_matrixvec
  Op.A     = A
  
  #fAx = fwOp.sel_Ax(Op)
  
  
  
  # ------------------------------------------------------------------
  # Select cost function (which implicitily defines the grad function)
  # ------------------------------------------------------------------
  
  #fCost = F2O.costGradF2O(fAx, args)
  

  args.bShape    = b.shape
  args.hShape    = A.shape

  # ------------------------------
  # --- Barzilai-Borwein step-size
  args.ssPoliciy = CTE.ss_BBv1
  x1, gdStats1 = pg(Op, b, lmbda, nIter, args)
  
  
  # --------------------------
  # --- Cauchy step-size
  args.ssPoliciy = CTE.ss_Cauchy
  args.ssCte     = 0.0
  x2, gdStats2 = pg(Op, b, lmbda, nIter, args)


  # --- Plot results ---
  PLT.figure()
  #PLT.plot(gdStats0[:,0], label=r'$\alpha_k$ = {0} -- {1}'.format(ssCte, CTE.ss_list[CTE.ss_Cte]) )
  PLT.plot(gdStats1[:,0], label=r'$\alpha_k$ = {0}'.format(CTE.ss_list[CTE.ss_BBv1]))
  PLT.plot(gdStats2[:,0], label=r'$\alpha_k$ = {0}'.format(CTE.ss_list[CTE.ss_Cauchy]))
  PLT.legend(loc='upper right')
  PLT.ylabel(r'$f(x) = \frac{1}{2}\| A \mathbf{x} - \mathbf{b} \|_2^2$')
  PLT.xlabel('Iteracion')
  
  PLT.show()




def testFISTA(N=np.array([100,200]), lmbda=0.1, nIter=100, eta=0.1, sp=0.85):
#
# testFISTA(np.array([400,4000]), lmbda=0.01, nIter=50, eta=0.5,sp=0.95 )  
#

  # -------------
  # Generate data
  # -------------
  
  A, b, xori = genData(N[0],N[1], int(np.floor(sp*N[1])) )
  
  
  # --------------------------------------
  # Define parameter optimization variable 
  # --------------------------------------

  args = F2O.argsF2O()
  
  args.verbose    = True
  args.fCostClass = CTE.cost_L2L1_lin           # f(x) = 0.5|| fAx(x) - b ||_2^2 + \lambda|| x ||_1, where fAx is lineal
  args.proxOp     = CTE.prox_l1
  
  # -- specific parameters will be selected below
  
  
  # --------------------------------------------
  # Select forward operator  (see fwOperator.py)
  # --------------------------------------------
  
  Op       = fwOp()
  Op.linOp = CTE.fAx_matrixvec
  Op.A     = A
  
  #fAx = fwOp.sel_Ax(Op)
  
  
  
  # ------------------------------------------------------------------
  # Select cost function (which implicitily defines the grad function)
  # ------------------------------------------------------------------
  
  #fCost = F2O.costGradF2O(fAx, args)
  


  # ------------------------------
  # --- ISTA + Cauchy Lagged step-size
  args.ssPoliciy = CTE.ss_CauchyLagged
  x1, gdStats1 = pg(Op, b, lmbda, nIter, args)
  
  
  # --------------------------
  # --- FISTA + Cauchy Lagged step-size
  
  #args.ssPoliciy = CTE.ss_CauchyLagged
  args.ssPoliciy = CTE.ss_BBv1
  
  #args.ISeqPolicy = CTE.iseq_ntrv
  args.ISeqPolicy = CTE.iseq_glin
  x2, gdStats2 = apg(Op, b, lmbda, nIter, args)


  # --- Plot results ---
  PLT.figure()
  #PLT.plot(gdStats0[:,0], label=r'$\alpha_k$ = {0} -- {1}'.format(ssCte, CTE.ss_list[CTE.ss_Cte]) )
  PLT.plot(gdStats1[:,0], label=r'ISTA  -- $\alpha_k$ = {0}'.format(CTE.ss_list[CTE.ss_CauchyLagged]))
  PLT.plot(gdStats2[:,0], label=r'FISTA -- $\alpha_k$ = {0}'.format(CTE.ss_list[CTE.ss_CauchyLagged]))
  PLT.legend(loc='upper right')
  PLT.ylabel(r'$f(x) = \frac{1}{2}\| A \mathbf{x} - \mathbf{b} \|_2^2$')
  PLT.xlabel('Iteracion')
  
  PLT.show()
