
import scipy as sc
import matplotlib.pylab as PLT
import numpy as np

from F2O.F2O_utils import f2oJaxUtils

__author__ = """Paul Rodriguez <prodrig@pucp.edu.pe>"""


# ======================================================
# ======================================================

class synthData(object):
    r"""Routine for generating synthetic data to be used by the Demos
    """
  
    def __init__(self):
      pass
  
  
    def genDataMV(self, N=100, alpha=100., eta = 0.05, normalize=True):
  
      #f2oJax = f2oJaxUtils(flagForceJAX=self.enableJAX)
  
      B = np.random.randn(N, N)
  
      A = np.matmul(B.transpose(), B) + np.diag( alpha*np.ones([B.shape[1],],'f') )
  
      if normalize:
         A /= np.linalg.norm(A,axis=0)
  
      xOri = np.random.randn(N,1)
  
      b = A.dot(xOri) + eta*np.random.randn(N,1)
  
      #return(f2oJax.getDataIn(A), f2oJax.getDataIn(b), xOri)
      return(A, b, xOri)


#------------------------------------------------

    def genDataQR(self, rows=100, cols=100, alpha=50, eta = 0.5):
  
  
      [Q, R] = np.linalg.qr(np.random.randn(rows,cols), 'reduced')
  
      xOri = np.random.randn(cols,1)
      xOri[np.random.randint(0, cols, cols-alpha)] = 0.
  
      b = Q.dot(xOri) + eta*np.random.randn(rows,1)
  
  
      return(Q, b, xOri)

  
  
  